package com.digiponic.nitromaxinflator

import android.app.Activity
import android.app.AlertDialog
import android.app.Application
import android.app.ProgressDialog
import android.os.Handler
import android.os.Looper
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.digiponic.nitromaxinflator.external_libraries.bluetooth.Bluetooth
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.EscPosPrinter
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.connection.bluetooth.BluetoothPrintersConnections
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.exceptions.EscPosConnectionException
import com.digiponic.nitromaxinflator.injection.component.DaggerInjectConsumer
import com.digiponic.nitromaxinflator.injection.component.InjectConsumer
import com.digiponic.nitromaxinflator.injection.module.ContextModule
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.response.BTOutputResponse
import com.digiponic.nitromaxinflator.repository.network.parameter.BluetoothStatusLog
import com.digiponic.nitromaxinflator.repository.network.parameter.PayTransactionParams
import com.digiponic.nitromaxinflator.repository.network.request.RemoteRepository
import com.digiponic.nitromaxinflator.repository.network.response.DetailTransaksiResponse
import com.digiponic.nitromaxinflator.repository.network.response.ErrorResponseException
import com.digiponic.nitromaxinflator.repository.preference.PreferenceRepository
import com.digiponic.nitromaxinflator.sqlite.NitromaxDb
import com.digiponic.nitromaxinflator.wrapper.ResultWrapper
import com.mcnmr.utilities.extension.isNotEmptyOrNotNull
import com.mcnmr.utilities.extension.isNotNull
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.*
import org.jetbrains.anko.toast
import retrofit2.HttpException
import java.io.IOException
import java.lang.Runnable
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import javax.inject.Inject

class MainApplication : Application(){
  companion object {
    const val SAVE_NORMAL_TRANSACTION = "SAVE_NORMAL_TRANSACTION"
    const val SAVE_ENGINE_HISTORY = "SAVE_ENGINE_HISTORY"
    const val TRIGGER_QRIS_TRANSACTION = "TRIGGER_QRIS_TRANSACTION"
  }

  lateinit var consumer: InjectConsumer
  val bluetooth: Bluetooth = Bluetooth.getInstance(this)

  private val handler = Handler(Looper.getMainLooper())

  private var startEngineOff = System.currentTimeMillis()
  private var endEngineOff = System.currentTimeMillis()

  private val viewModelScope = CoroutineScope(SupervisorJob() + Dispatchers.Main)

  private var isConnected = true
  private var previousStatusConnected = isConnected
  private var isDisconnected = false
  private var previousStatusDisconnected = isDisconnected
  private var isFirstTime = true
  private val successSaveHistory = SingleEventWrapper<Void>()
  private val successSaveAppHistory = SingleEventWrapper<Void>()

  private lateinit var database: NitromaxDb
  @Inject
  lateinit var remoteRepository: RemoteRepository
  @Inject
  lateinit var preferenceRepository: PreferenceRepository

  override fun onCreate() {
    super.onCreate()
    consumer = DaggerInjectConsumer
      .builder()
      .contextModule(ContextModule(this))
      .build()
    consumer.inject(this)
    database = NitromaxDb.getInstance(this)

    bluetooth.onBluetoothChangeStatus.observeForever {
      when(it) {
        Bluetooth.BluetoothStatus.CONNECTED -> {
          isConnected = true
          isDisconnected = false
        }
        Bluetooth.BluetoothStatus.DISCONNECTED -> {
          isConnected = false
          isDisconnected = true
        }
        else -> {}
      }

      if (preferenceRepository.getUser().isNotNull()) {
        preferenceRepository.getUser().let { session ->
          if (isConnected && previousStatusConnected != isConnected && !isFirstTime) {
            endEngineOff = System.currentTimeMillis()
            if (endEngineOff - startEngineOff >= 30000) {
              saveEngineBluetoothStatusHistory(BluetoothStatusLog(formatDateTime(startEngineOff), formatDateTime(endEngineOff), session!!.idCabang, session.email))
            }
          } else if (isDisconnected && previousStatusDisconnected != isDisconnected && !isFirstTime) {
            startEngineOff = System.currentTimeMillis()
          }
        }
      }

      previousStatusConnected = isConnected
      previousStatusDisconnected = isDisconnected

      isFirstTime = false
    }
  }

  fun getBluetoothName(): String {
    return if (database.bluetoothNameDao().getBluetoothName().isNotNull()) {
      database.bluetoothNameDao().getBluetoothName().bluetooth_name
    } else {
      "NITROMAXBT"
    }
  }

  fun connectBluetooth(delayMillis: Long = 60000){
    bluetooth.onStart()

    val bluetoothName = getBluetoothName()

    handler.post(object : Runnable {
      override fun run() {
        if(bluetooth.onBluetoothChangeStatus.value == Bluetooth.BluetoothStatus.DISCONNECTED){
          if(!bluetooth.isEnabled){
            bluetooth.enable()
          } else {
            try {
              bluetooth.connectToName(bluetoothName)
            } catch (e: Exception) {
              toast(e.message.toString())
            }
          }
        }
        handler.postDelayed(this, delayMillis)
      }
    })
  }

  fun saveNormalTransaction(data: BTOutputResponse, json_string: String, activity: BaseActivity, errorBrokenPipe: SingleEventWrapper<String>, onPrinterNotConnected: SingleEventWrapper<Void>, checkoutSuccess: SingleEventWrapper<Int>, isCekBocor: Boolean = false){
    val harga = data.harga
    preferenceRepository.getUser()?.let { session ->
      val details = mutableListOf<PayTransactionParams.PayTransactionDetail>()
      val it = data.detail
      when (it.modeTransaksi) {
        in 1..2 -> {
          var totalGagal = 0
          val keteranganTekanan = mutableListOf<String>()
          val pressures = data.pressure(it.modeTransaksi, harga, session.skalaGagal)
          pressures.forEach { pressure ->
            totalGagal += if(pressure.isGagal) 1 else 0
            keteranganTekanan.add("${pressure.tekananAwal}/${pressure.tekanan}")
          }
          details.add(
            PayTransactionParams.PayTransactionDetail(
              idItem = it.modeTransaksi.toString(),
              qty = it.jumlahBan,
              gagal = totalGagal,
              keterangan = keteranganTekanan.joinToString()
            )
          )
        }
        in 3..4 -> {
          val pressures = data.pressure(it.modeTransaksi, harga, session.skalaGagal)
          var totalIsiTambah = 0
          var totalIsiBaru = 0
          var totalGagalIsiBaru = 0
          var totalGagalIsiTambah = 0
          val keteranganTekananIsiBaru = mutableListOf<String>()
          val keteranganTekananIsiTambah = mutableListOf<String>()
          pressures.forEach { pressure ->
            val isTekananTerhitungIsiBaru = if (it.modeTransaksi == 3) {
              pressure.tekananAwal <= session.limit_pressure_inflation_motor && !pressure.isTts
            } else {
              pressure.tekananAwal <= session.limit_pressure_inflation_mobil && !pressure.isTts
            }

            if (isTekananTerhitungIsiBaru) {
              keteranganTekananIsiBaru.add("${pressure.tekananAwal}/${pressure.tekanan}")
              totalIsiBaru++
              totalGagalIsiBaru += if (pressure.isGagal) 1 else 0
            } else {
              keteranganTekananIsiTambah.add("${pressure.tekananAwal}/${pressure.tekanan}")
              totalIsiTambah++
              totalGagalIsiTambah += if (pressure.isGagal) 1 else 0
            }
          }
          if (totalIsiBaru > 0) {
            val isiBaruItem = if (it.modeTransaksi == 3) "1" else "2"
            details.add(PayTransactionParams.PayTransactionDetail(idItem = isiBaruItem, qty = totalIsiBaru, gagal = totalGagalIsiBaru, keterangan = keteranganTekananIsiBaru.joinToString()))
          }
          if (totalIsiTambah > 0) {
            val isiTambahItem = if (isCekBocor) {
              if (it.modeTransaksi == 3) "5" else "6"
            } else {
              it.modeTransaksi.toString()
            }
            details.add(PayTransactionParams.PayTransactionDetail(isiTambahItem, qty = totalIsiTambah, gagal = totalGagalIsiTambah, keterangan = keteranganTekananIsiTambah.joinToString()))
          }
        }
        in 7..8 -> {
          details.add(PayTransactionParams.PayTransactionDetail(idItem = it.modeTransaksi.toString(), qty = it.jumlahLubang, gagal = 0, keterangan = ""))

          if (session.has_tambal_ban_khusus == 1) {
            val pressures = data.pressure(it.modeTransaksi, harga, session.skalaGagal)
            pressures.forEach { pressure ->
              val isTekananTerhitungIsiBaru = if (it.modeTransaksi == 7) {
                pressure.tekananAwal <= session.limit_pressure_inflation_motor && !pressure.isTts
              } else {
                pressure.tekananAwal <= session.limit_pressure_inflation_mobil && !pressure.isTts
              }
              if (isTekananTerhitungIsiBaru) details.add(PayTransactionParams.PayTransactionDetail(idItem = if (it.modeTransaksi == 7 ) "1" else "2", qty = 1, gagal = 0, keterangan = "${pressure.tekananAwal}/${pressure.tekanan}"))
            }
          }
        }
      }
      val params = PayTransactionParams(
        idMode = 0,
        user = session.email,
        cabang = session.idCabang,
        jenisKendaraan = data.jenisKendaraan.toString(),
        bayar = harga,
        detail = details,
        tanggal = data.tglTransaksi,
        metodePembayaran = 29,
        json_string = json_string
      )
      viewModelScope.launch {
        activity.shouldShowLoading(SAVE_NORMAL_TRANSACTION)
        val result = safeApiCall(SAVE_NORMAL_TRANSACTION) { remoteRepository.newCheckout(params) }
        activity.shouldHideLoading(SAVE_NORMAL_TRANSACTION)
        result.consume(
          onUnknownError = activity::onUnknownError,
          onTimeoutError = activity::onTimeoutError,
          onNetworkError = activity::onNetworkError,
          onHttpError = activity::onHttpError,
          onErrorResponse = activity::onErrorResponse,
          onSuccess = {
            val transaksi = it.data
            if (session.is_using_qris == 1) {
              triggerQrisTransaction(activity, transaksi, errorBrokenPipe, onPrinterNotConnected, checkoutSuccess)
            } else {
              if (session.is_auto_print_struk == 1) {
                printWithProgressBar(transaksi, activity, errorBrokenPipe, onPrinterNotConnected, checkoutSuccess)
              } else {
                checkoutSuccess.value = transaksi.subtotal
              }
            }
          }
        )
      }
    }
  }

  fun printWithProgressBar(response: DetailTransaksiResponse.Transaksi, activity: Activity, errorBrokenPipe: SingleEventWrapper<String>, onPrinterNotConnected: SingleEventWrapper<Void>, checkoutSuccess: SingleEventWrapper<Int>){
    val progressDialog = ProgressDialog(activity)
    viewModelScope.executeAsyncTask(
      onPreExecute = {
        progressDialog.setCancelable(false)
        progressDialog.setTitle("Harap Tunggu")
        progressDialog.setMessage("Proses pencetakan nota sedang berjalan")
        progressDialog.show()
      },
      doInBackground = { _: suspend(progress: Int) -> Unit ->
        try {
          printTransaksi(response, onPrinterNotConnected)
        } catch (e: EscPosConnectionException){
          if (e.message!!.contains("Broken pipe")) {
            errorBrokenPipe.value = "Koneksi ke printer terputus"
          } else {
            onPrinterNotConnected.postTrigger()
          }
        }
      },
      onPostExecute = {
        if (progressDialog.isShowing) progressDialog.dismiss()
        checkoutSuccess.value = response.subtotal
      },
      onProgressUpdate = {}
    )
  }

  private fun printTransaksi(response: DetailTransaksiResponse.Transaksi, onPrinterNotConnected: SingleEventWrapper<Void>) {
    val firstPairedDevice = BluetoothPrintersConnections.getInstance().selectFirstPaired()
    if(firstPairedDevice == null){
      throw EscPosConnectionException("Printer not connected")
    } else {
      val printer = EscPosPrinter(BluetoothPrintersConnections.getInstance().selectFirstPaired(), 203, 48f, 32)

      var text: String = "[C]<u><font size='big'>${response.header_cabang}</font></u>\n" +
          "[C]${response.nama_cabang}\n" +
          "[L]\n" +
          "[L]Tanggal[R]${response.tanggal}\n" +
          "[L]Transaksi #[R]${response.kode}\n" +
          "[L]Metode Bayar[R]${response.metode_bayar}\n" +
          "[L]Operator [R]${preferenceRepository.getUser()?.name}\n" +
          "[C]================================\n"

      for (item in response.detail) {
        when (item.id_item) {
          1,2,3,4,10 -> {
            text += "[L]<b>${item.nama}</b>\n"
            val keterangan = if (item.keterangan.isNotEmptyOrNotNull()) item.keterangan.split(", ") else listOf()
            val ketSize = keterangan.size
            var index = 0
            for (ket in keterangan) {
              if (ket.isNotEmpty()) {
                var harga = item.harga
                val tekanans = ket.split("/")
                if (tekanans.size > 2 && index == ketSize - 1) {
                  text += "[L]<b>${tekanans[0].toInt()} -> ${tekanans[3].replace(",","").toInt()} (cancel)</b>[R]0\n"
                } else {
                  if (tekanans[1].replace(",","").toInt() >= 17) {
                    if (tekanans[0].toInt() >= tekanans[1].replace(",","").toInt()) {
                      harga = (harga.toDouble() * item.gagal).toInt()
                    }
                    text += "[L]<b>${tekanans[0].toInt()} -> ${tekanans[1].replace(",","").toInt()}</b>[R]${NumberFormat.getInstance().format(harga)}\n"
                  }
                }
                index++
              }
            }
          }
          7,8,11,12 -> {
            text += "[L]<b>${item.nama}</b>\n"
            text += "[L]<b>Jumlah lb/krt : ${item.qty}</b>[R]${NumberFormat.getInstance().format(item.subtotal)}\n"
          }
          else -> {
            text += "[L]<b>${item.nama}</b>\n"
            text += "[L]<b>${item.qty} x ${NumberFormat.getInstance().format(item.harga)}</b>[R]${NumberFormat.getInstance().format(item.subtotal)}\n"
          }
        }
      }
      text += "[C]--------------------------------\n" +
          "[R]TOTAL PRICE : ${NumberFormat.getInstance().format(response.subtotal)}\n" +
          "[C]================================\n"

//      if (response.metode_bayar == "CASHLESS") {
//        text += "[C]Silahkan scan QR code ini untuk melakukan pembayaran cashless dengan QRIS\n" +
//            "[C]<qrcode size='30'>${response.qrcode}</qrcode>\n"
//      }

      if (response.terms.isNotEmptyOrNotNull()) text += "[L]" + response.terms + "\n"

      text += "[L]\n" +
          "[C]by : Nitromax - DIGIPONIC\n"+
          "[L]\n[L]\n"

      try {
        printer.printFormattedText(text, 10f)
      } catch (e: Exception) {
        onPrinterNotConnected.postTrigger()
      }
    }
  }

  fun triggerQrisTransaction(activity: BaseActivity, response: DetailTransaksiResponse.Transaksi, errorBrokenPipe: SingleEventWrapper<String>, onPrinterNotConnected: SingleEventWrapper<Void>, checkoutSuccess: SingleEventWrapper<Int>) {
    preferenceRepository.getUser()?.let { session ->
      AlertDialog.Builder(activity)
        .setCancelable(false)
        .setTitle("Konfirmasi Pembayaran")
        .setMessage("Apakah Anda ingin menggunakan pembayaran cashless (QRIS)?")
        .setPositiveButton("Ya") { _, _ ->
          viewModelScope.launch {
            activity.shouldShowLoading(TRIGGER_QRIS_TRANSACTION)
            val result = safeApiCall(TRIGGER_QRIS_TRANSACTION) { remoteRepository.checkoutCashless(response.kode) }
            activity.shouldHideLoading(TRIGGER_QRIS_TRANSACTION)
            result.consume(
              onUnknownError = activity::onUnknownError,
              onTimeoutError = activity::onTimeoutError,
              onNetworkError = activity::onNetworkError,
              onHttpError = activity::onHttpError,
              onErrorResponse = { _, _ ->
                val alert = AlertDialog.Builder(activity)
                  .setTitle("Kesalahan")
                  .setMessage("Terjadi kesalahan saat melakukan generate QRIS. Anda akan diarahkan ke pembayaran cash dalam 3 detik.")
                  .create()
                alert.show()

                val handler = Handler()
                val runnable = Runnable { if (alert.isShowing) alert.dismiss()}
                alert.setOnDismissListener {
                  printWithProgressBar(response, activity, errorBrokenPipe, onPrinterNotConnected, checkoutSuccess)
                }
                handler.postDelayed(runnable, 3000)
              },
              onSuccess = {
                val transaksi = it.data
                printWithProgressBar(transaksi, activity, errorBrokenPipe, onPrinterNotConnected, checkoutSuccess)
              }
            )
          }
        }
        .setNegativeButton("Tidak") { _, _ ->
          if (session.is_auto_print_struk == 1) {
            printWithProgressBar(response, activity, errorBrokenPipe, onPrinterNotConnected, checkoutSuccess)
          } else {
            checkoutSuccess.value = response.subtotal
          }
        }.create().show()
    }
  }

  fun disconnectBluetooth(){
    handler.removeCallbacksAndMessages(null)
    bluetooth.onStop()
  }

  fun saveAppHistory(keterangan: String) {
    preferenceRepository.getUser().let { session ->
      viewModelScope.launch {
        val result = safeApiCall(SAVE_ENGINE_HISTORY) {
          remoteRepository.saveAppHistory(session!!.idCabang, session.email, keterangan)
        }
        result.consume(
          onUnknownError = { _, _ -> },
          onTimeoutError = { },
          onNetworkError = { _, _ -> },
          onHttpError = { _, _, _ -> },
          onErrorResponse = { _, _ -> },
          onSuccess = {
            successSaveAppHistory.trigger()
          }
        )
      }
    }
  }

  private fun saveEngineBluetoothStatusHistory(data: BluetoothStatusLog) {
    viewModelScope.launch {
      val result = safeApiCall(SAVE_ENGINE_HISTORY) {
        remoteRepository.saveEngineHistory(data)
      }
      result.consume(
        onUnknownError = { _, _ -> },
        onTimeoutError = { },
        onNetworkError = { _, _ -> },
        onHttpError = { _, _, _ -> },
        onErrorResponse = { _, _ -> },
        onSuccess = {
          startEngineOff = System.currentTimeMillis()
          endEngineOff = System.currentTimeMillis()
          successSaveHistory.trigger()
        }
      )
    }
  }

  private fun formatDateTime(timeMillis: Long) : String {
    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
    val resultDate = Date(timeMillis)
    return sdf.format(resultDate)
  }

  fun <P, R> CoroutineScope.executeAsyncTask(
    onPreExecute: () -> Unit,
    doInBackground: suspend (suspend (P) -> Unit) -> R,
    onPostExecute: (R) -> Unit,
    onProgressUpdate: (P) -> Unit
  ) = launch {
    onPreExecute()

    val result = withContext(Dispatchers.IO) {
      doInBackground {
        withContext(Dispatchers.Main) { onProgressUpdate(it) }
      }
    }
    onPostExecute(result)
  }

  protected suspend fun <T: BaseResponse> safeApiCall(tag: Any, dispatcher: CoroutineDispatcher = Dispatchers.IO, apiCall: suspend () -> T): ResultWrapper<T> {
    return withContext(dispatcher){
      try{
        val result = apiCall.invoke()
        if(!result.status()) {
          throw ErrorResponseException(message = result.message())
        }else {
          ResultWrapper.Success(value = result)
        }
      }catch (e: java.lang.Exception){
        e.printStackTrace()
        when(e){
          is IOException -> ResultWrapper.NetworkError(tag = tag, error = e)
          is HttpException -> {
            val code = e.code()
            val errorResponse = e.message()
            ResultWrapper.HttpError(tag, code = code, error = errorResponse)
          }
          is ErrorResponseException -> ResultWrapper.ResponseError(tag = tag, error = e)
          else -> ResultWrapper.UnknownError(tag = tag, message = e.message ?: "No message")
        }
      }
    }
  }
}