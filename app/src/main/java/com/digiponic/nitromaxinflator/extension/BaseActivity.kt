package com.digiponic.nitromaxinflator.extension

import androidx.lifecycle.ViewModelProvider
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.ancestors.BaseViewModel
import com.digiponic.nitromaxinflator.factory.ViewModelFactory
import com.mcnmr.utilities.extension.isDenied

inline fun <reified T: BaseViewModel> BaseActivity.instantiateViewModel(): T =
    ViewModelProvider(this, ViewModelFactory(this)).get(T::class.java)

fun Array<out String>.isPermissionGranted(grantResults: IntArray): Boolean{
    this.forEachIndexed { index, _ ->
        if(grantResults[index].isDenied()){
            return false
        }
    }
    return true
}