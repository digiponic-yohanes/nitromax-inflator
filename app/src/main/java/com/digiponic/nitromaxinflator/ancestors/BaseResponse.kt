package com.digiponic.nitromaxinflator.ancestors

interface BaseResponse{
    fun status(): Boolean
    fun message(): String
}