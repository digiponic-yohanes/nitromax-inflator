package com.digiponic.nitromaxinflator.ancestors

import android.content.Intent
import android.os.Bundle
import android.os.Process
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.callbacks.onDismiss
import com.afollestad.materialdialogs.callbacks.onShow
import com.digiponic.nitromaxinflator.MainApplication
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.custom_dialog.LoadingDialog
import com.digiponic.nitromaxinflator.repository.network.response.ErrorResponseException
import com.digiponic.nitromaxinflator.ui.force_close.ForceCloseActivity
import com.mcnmr.utilities.extension.dismissIfShowing
import com.mcnmr.utilities.extension.showIfNotShowing
import com.mcnmr.utilities.internal_plugin.obtainIntentData
import java.io.IOException
import java.io.PrintWriter
import java.io.StringWriter

open class BaseActivity: AppCompatActivity(), Thread.UncaughtExceptionHandler {
    val app by lazy { (application as MainApplication) }

    companion object{
        const val FROM = "FROM"
    }

    private val loadingDialog by lazy { LoadingDialog(this).apply {
        setCancelable(false)
        setCanceledOnTouchOutside(false)
    } }

    open fun shouldShowLoading(tag: Any) = loadingDialog.showIfNotShowing()
    open fun shouldHideLoading(tag: Any) = loadingDialog.dismissIfShowing()
    open fun onErrorResponse(tag: Any, exception: ErrorResponseException){
        alertDialog(title = getString(R.string.alert_dialog_message_title),
            message = exception.message,
            positiveTitle = getString(R.string.alert_dialog_close))
    }
    open fun onNetworkError(tag: Any, exception: IOException){
        alertDialog(title = getString(R.string.alert_dialog_error_title),
            message = getString(R.string.alert_dialog_no_internet_message).format(exception.message),
            positiveTitle = getString(R.string.alert_dialog_close))
    }
    open fun onTimeoutError(tag: Any){
        alertDialog(title = getString(R.string.alert_dialog_error_title),
            message = getString(R.string.alert_dialog_rto_message),
            positiveTitle = getString(R.string.alert_dialog_close))
    }
    open fun onHttpError(tag: Any, httpCode: Int, message: String){
        alertDialog(title = getString(R.string.alert_dialog_http_error_title),
            message = getString(R.string.alert_dialog_http_error_message).format(httpCode, message),
            positiveTitle = getString(R.string.alert_dialog_close))
    }
    open fun onUnknownError(tag: Any, message: String){
        alertDialog(title = getString(R.string.alert_dialog_error_title),
            message = getString(R.string.alert_dialog_unknown_error).format(message),
            positiveTitle = getString(R.string.alert_dialog_close))
    }
    open fun onOptionsSelected(item: MenuItem){}

    protected open fun shouldCatchForceClose(): Boolean = true

    fun alertDialog(title: String? = null,
                    message: String? = null,
                    negativeTitle: String? = null,
                    negativeAction: (() -> Unit)? = null,
                    positiveTitle: String? = null,
                    positiveAction: (() -> Unit)? = null,
                    onShow: (() -> Unit)? = null,
                    onDismiss: (() -> Unit)? = null,
                    cancelable: Boolean = true){
        MaterialDialog(this).show {
            title?.let{
                title(text = title)
            }
            message?.let{
                message(text = message)
            }
            negativeTitle?.let{
                negativeButton(text = negativeTitle) { dialog ->
                    dialog.dismiss()
                    negativeAction?.invoke()
                }
            }
            positiveTitle?.let {
                positiveButton(text = positiveTitle) { dialog ->
                    dialog.dismiss()
                    positiveAction?.invoke()
                }
            }
        }.onShow { onShow?.invoke() }.onDismiss { onDismiss?.invoke() }.apply {
            cancelable(cancelable)
            cancelOnTouchOutside(cancelable)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(shouldCatchForceClose()) {
            Thread.setDefaultUncaughtExceptionHandler(this)
        }
        obtainIntentData()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> onBackPressed()
            else -> onOptionsSelected(item)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun uncaughtException(thread: Thread, ex: Throwable) {
        val errors = StringWriter()
        ex.printStackTrace(PrintWriter(errors))
        ex.printStackTrace()

        val intent = Intent(this, ForceCloseActivity::class.java).apply {
            putExtra(ForceCloseActivity.ERROR_MESSAGE_ARGUMENT, errors.toString())
            putExtra(FROM, this::class.java.name)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }

        startActivity(intent)
        finishAffinity()
        Process.killProcess(Process.myPid())
    }
}