package com.digiponic.nitromaxinflator.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.ancestors.BaseViewModel
import com.digiponic.nitromaxinflator.ui.absensi.AbsensiVM
import com.digiponic.nitromaxinflator.ui.check_pressure.CheckPressureVM
import com.digiponic.nitromaxinflator.ui.check_tire.CheckTireVM
import com.digiponic.nitromaxinflator.ui.checkout_success.CheckoutSuccessVM
import com.digiponic.nitromaxinflator.ui.dashboard.DashboardVM
import com.digiponic.nitromaxinflator.ui.login.LoginVM
import com.digiponic.nitromaxinflator.ui.logtext.LogVM
import com.digiponic.nitromaxinflator.ui.payment.PaymentVM
import com.digiponic.nitromaxinflator.ui.selfie.SelfieVM
import com.digiponic.nitromaxinflator.ui.splashscreen.SplashScreenVM
import com.digiponic.nitromaxinflator.ui.transaksi.TransaksiVM

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val activity: BaseActivity): ViewModelProvider.NewInstanceFactory() {

  override fun <T : ViewModel> create(modelClass: Class<T>): T = when(modelClass){
    SplashScreenVM::class.java -> SplashScreenVM(activity = activity) as T
    LoginVM::class.java -> LoginVM(activity = activity) as T
    SelfieVM::class.java -> SelfieVM(activity = activity) as T
    DashboardVM::class.java -> DashboardVM(activity = activity) as T
    TransaksiVM::class.java -> TransaksiVM(activity = activity) as T
    PaymentVM::class.java -> PaymentVM(activity = activity) as T
    AbsensiVM::class.java -> AbsensiVM(activity = activity) as T
    CheckTireVM::class.java -> CheckTireVM(activity = activity) as T
    CheckoutSuccessVM::class.java -> CheckoutSuccessVM(activity = activity) as T
    LogVM::class.java -> LogVM(activity = activity) as T
    CheckPressureVM::class.java -> CheckPressureVM(activity = activity) as T
    else -> BaseViewModel(activity = activity) as T
  }
}