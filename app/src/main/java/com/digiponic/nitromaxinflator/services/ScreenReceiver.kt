package com.digiponic.nitromaxinflator.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.digiponic.nitromaxinflator.MainApplication

class ScreenReceiver: BroadcastReceiver() {

  companion object {
    private lateinit var app: MainApplication
  }

  override fun onReceive(context: Context?, intent: Intent?) {
    app = context!!.applicationContext as MainApplication

    if (intent?.action.equals(Intent.ACTION_SCREEN_OFF)) {
      app.saveAppHistory("Layar mati")
    } else if (intent?.action.equals(Intent.ACTION_SCREEN_ON)) {
      app.saveAppHistory("Layar menyala")
    }
  }
}