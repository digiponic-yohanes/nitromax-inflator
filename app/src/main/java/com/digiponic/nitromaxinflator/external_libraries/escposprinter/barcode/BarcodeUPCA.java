package com.digiponic.nitromaxinflator.external_libraries.escposprinter.barcode;

import com.digiponic.nitromaxinflator.external_libraries.escposprinter.EscPosPrinterCommands;
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.EscPosPrinterSize;
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.exceptions.EscPosBarcodeException;

public class BarcodeUPCA extends BarcodeNumber {

    public BarcodeUPCA(EscPosPrinterSize printerSize, String code, float widthMM, float heightMM, int textPosition) throws EscPosBarcodeException {
        super(printerSize, EscPosPrinterCommands.BARCODE_TYPE_UPCA, code, widthMM, heightMM, textPosition);
    }

    @Override
    public int getCodeLength() {
        return 12;
    }
}
