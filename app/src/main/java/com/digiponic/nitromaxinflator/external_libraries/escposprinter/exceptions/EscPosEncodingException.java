package com.digiponic.nitromaxinflator.external_libraries.escposprinter.exceptions;

public class EscPosEncodingException extends Exception {
    public EscPosEncodingException(String errorMessage) {
        super(errorMessage);
    }
}
