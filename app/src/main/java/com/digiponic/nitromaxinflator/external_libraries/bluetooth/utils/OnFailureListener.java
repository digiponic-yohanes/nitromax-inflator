package com.digiponic.nitromaxinflator.external_libraries.bluetooth.utils;

public interface OnFailureListener<T> {
    void onFailure(T t);
}
