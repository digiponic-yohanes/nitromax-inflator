package com.digiponic.nitromaxinflator.external_libraries.escposprinter.exceptions;

public class EscPosBarcodeException extends Exception {
    public EscPosBarcodeException(String errorMessage) {
        super(errorMessage);
    }
}
