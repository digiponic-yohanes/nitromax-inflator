package com.digiponic.nitromaxinflator.external_libraries.escposprinter.exceptions;

public class EscPosConnectionException extends Exception {
    public EscPosConnectionException(String errorMessage) {
        super(errorMessage);
    }
}
