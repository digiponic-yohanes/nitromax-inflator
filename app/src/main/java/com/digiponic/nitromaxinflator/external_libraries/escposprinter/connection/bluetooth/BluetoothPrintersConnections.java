package com.digiponic.nitromaxinflator.external_libraries.escposprinter.connection.bluetooth;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;

import com.digiponic.nitromaxinflator.external_libraries.escposprinter.exceptions.EscPosConnectionException;

public class BluetoothPrintersConnections extends BluetoothConnections {

    private static BluetoothPrintersConnections instance;
    private BluetoothConnection currentConnectedDevice;

    private BluetoothPrintersConnections(){}

    public static BluetoothPrintersConnections getInstance(){
        if(instance == null){
            instance = new BluetoothPrintersConnections();
        }

        return instance;
    }

    /**
     * Easy way to get the first bluetooth printer paired / connected.
     *
     * @return a EscPosPrinterCommands instance
     */
    public  BluetoothConnection selectFirstPaired() {
        if(currentConnectedDevice == null){
            BluetoothPrintersConnections printers = new BluetoothPrintersConnections();
            BluetoothConnection[] bluetoothPrinters = printers.getList();

            if (bluetoothPrinters != null && bluetoothPrinters.length > 0) {
                for (BluetoothConnection printer : bluetoothPrinters) {
                    try {
                        currentConnectedDevice = printer.connect();
                        return currentConnectedDevice;
                    } catch (EscPosConnectionException e) {
                        e.printStackTrace();
                    }
                }
            }
        }else {
            if(!currentConnectedDevice.isConnected()){
                try{
                    currentConnectedDevice.connect();
                }catch (EscPosConnectionException e){
                    e.printStackTrace();
                }
            }
            return currentConnectedDevice;
        }

        return null;
    }


    /**
     * Get a list of bluetooth printers.
     *
     * @return an array of EscPosPrinterCommands
     */
    @SuppressLint("MissingPermission")
    public BluetoothConnection[] getList() {
        BluetoothConnection[] bluetoothDevicesList = super.getList();

        if (bluetoothDevicesList == null) {
            return null;
        }

        int i = 0;
        BluetoothConnection[] printersTmp = new BluetoothConnection[bluetoothDevicesList.length];
        for (BluetoothConnection bluetoothConnection : bluetoothDevicesList) {
            BluetoothDevice device = bluetoothConnection.getDevice();

            int majDeviceCl = device.getBluetoothClass().getMajorDeviceClass(),
                    deviceCl = device.getBluetoothClass().getDeviceClass();

            if (majDeviceCl == BluetoothClass.Device.Major.IMAGING && (deviceCl == 1664 || deviceCl == BluetoothClass.Device.Major.IMAGING)) {
                printersTmp[i++] = new BluetoothConnection(device);
            }
        }
        BluetoothConnection[] bluetoothPrinters = new BluetoothConnection[i];
        System.arraycopy(printersTmp, 0, bluetoothPrinters, 0, i);
        return bluetoothPrinters;
    }

}
