package com.digiponic.nitromaxinflator.external_libraries.bluetooth.utils;

public interface OnSuccessListener<T> {
    void onSuccess(T t);
}
