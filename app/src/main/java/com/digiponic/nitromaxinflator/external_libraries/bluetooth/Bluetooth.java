package com.digiponic.nitromaxinflator.external_libraries.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.digiponic.nitromaxinflator.extension.NonNullLiveData;
import com.digiponic.nitromaxinflator.external_libraries.bluetooth.constants.DeviceError;
import com.digiponic.nitromaxinflator.external_libraries.bluetooth.constants.DiscoveryError;
import com.digiponic.nitromaxinflator.external_libraries.bluetooth.reader.LineReader;
import com.digiponic.nitromaxinflator.external_libraries.bluetooth.reader.SocketReader;
import com.mcnmr.utilities.wrapper.SingleEventWrapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.util.UUID;

/**
 * Created by Omar on 14/07/2015.
 */
public class Bluetooth {
    public enum BluetoothStatus {
        CONNECTING, CONNECTED, DISCONNECTED
    }

    private final static String DEFAULT_UUID = "00001101-0000-1000-8000-00805f9b34fb";

    private static Bluetooth instance;
    private final WeakReference<Context> context;
    private final UUID uuid = UUID.fromString(DEFAULT_UUID);

    private BluetoothAdapter bluetoothAdapter;

    //DeviceCallback
    public NonNullLiveData<BluetoothStatus> onBluetoothChangeStatus = new NonNullLiveData<>(BluetoothStatus.DISCONNECTED);
    public SingleEventWrapper<BluetoothDevice> onDeviceConnected = new SingleEventWrapper<>();
    public SingleEventWrapper<String> onDeviceDisconnected = new SingleEventWrapper<>();
    public SingleEventWrapper<byte[]> onMessage = new SingleEventWrapper<>();
    public SingleEventWrapper<String> onConnectError = new SingleEventWrapper<>();

    //DiscoverCallback
    public SingleEventWrapper<Void> onDiscoveryStarted = new SingleEventWrapper<>();
    public SingleEventWrapper<Void> onDiscoveryFinished = new SingleEventWrapper<>();
    public SingleEventWrapper<BluetoothDevice> onDeviceFound = new SingleEventWrapper<>();
    public SingleEventWrapper<BluetoothDevice> onDevicePaired = new SingleEventWrapper<>();
    public SingleEventWrapper<BluetoothDevice> onDeviceUnpaired = new SingleEventWrapper<>();

    //BluetoothCallback
    public SingleEventWrapper<Void> onBluetoothTurningOn = new SingleEventWrapper<>();
    public SingleEventWrapper<Void> onBluetoothOn = new SingleEventWrapper<>();
    public SingleEventWrapper<Void> onBluetoothTurningOff = new SingleEventWrapper<>();
    public SingleEventWrapper<Void> onBluetoothOff = new SingleEventWrapper<>();

    public SingleEventWrapper<Integer> onError = new SingleEventWrapper<>();

    private ReceiveThread receiveThread;
    private boolean connected = false;

    private final Class<LineReader> readerClass = LineReader.class;

    public static Bluetooth getInstance(Context context){
        if(instance == null){
            instance = new Bluetooth(context);
        }

        return instance;
    }

    /**
     * Init Bluetooth object. Default UUID will be used.
     * @param context Context to be used.
     */
    private Bluetooth(Context context){
        this.context = new WeakReference<>(context);
    }

    /**
     * Start bluetooth service.
     */
    public void onStart(){
        BluetoothManager bluetoothManager = (BluetoothManager) context.get().getSystemService(Context.BLUETOOTH_SERVICE);
        if(bluetoothManager != null) {
            bluetoothAdapter = bluetoothManager.getAdapter();
        }
        context.get().registerReceiver(bluetoothReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }

    /**
     * Stop bluetooth service.
     */
    public void onStop(){
        context.get().unregisterReceiver(bluetoothReceiver);
    }

    /**
     * Enable bluetooth without asking the user.
     */
    public void enable(){
        if(bluetoothAdapter!=null) {
            if (!bluetoothAdapter.isEnabled()) {
                bluetoothAdapter.enable();
            }
        }
    }

    /**
     * Check if the current device is connected to a bluetooth device.
     * @return True if connected, false otherwise.
     */
    public boolean isConnected(){
        return connected;
    }

    /**
     * Check if bluetooth is enabled.
     * @return true if bluetooth is enabled, false otherwise.
     */
    public boolean isEnabled(){
        if(bluetoothAdapter!=null) {
            return bluetoothAdapter.isEnabled();
        }
        return false;
    }

    /**
     * Connect to device already paired, using its name.
     * @param name Device name.
     * @param insecureConnection True if you don't need the data to be encrypted.
     * @param withPortTrick https://stackoverflow.com/a/25647197/5552022.
     */
    public void connectToName(String name, boolean insecureConnection, boolean withPortTrick) {
        for (BluetoothDevice blueDevice : bluetoothAdapter.getBondedDevices()) {
            if (blueDevice.getName().equals(name)) {
                connectToDevice(blueDevice, insecureConnection, withPortTrick);
                return;
            }
        }
    }

    /**
     * Connect to device already paired, using its name.
     * @param name Device name.
     */
    public void connectToName(String name) {
        if(isConnected()){
            return;
        }
        connectToName(name, false, false);
    }

    /**
     * Connect to bluetooth device.
     * @param device Bluetooth device.
     * @param insecureConnection True if you don't need the data to be encrypted.
     * @param withPortTrick https://stackoverflow.com/a/25647197/5552022.
     */
    public void connectToDevice(final BluetoothDevice device, boolean insecureConnection, boolean withPortTrick){
        if(bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }
        connect(device, insecureConnection, withPortTrick);
    }

    /**
     * Send byte array to the connected device.
     * @param data byte array to be sent.
     */
    public void send(byte[] data){
        OutputStream out = receiveThread.getOutputStream();
        try {
            out.write(data);
        } catch (final IOException e) {
            connected = false;
            onDeviceDisconnected.postValue(e.getMessage());
            onBluetoothChangeStatus.postValue(BluetoothStatus.DISCONNECTED);
        }
    }

    /**
     * Send string message to the connected device.
     * @param msg String message.
     * @param charset Charset used to encode the String. Default charset is UTF-8.
     */
    public void send(String msg, Charset charset){
        if(charset==null){
            send(msg.getBytes());
        } else{
            send(msg.getBytes(charset));
        }
    }

    private BluetoothSocket createBluetoothSocketWithPortTrick(BluetoothDevice device){
        BluetoothSocket socket = null;
        try {
            socket = (BluetoothSocket) device.getClass().getMethod("createRfcommSocket", new Class[] {int.class}).invoke(device,1);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            Log.w(getClass().getSimpleName(), e.getMessage());
        }
        return socket;
    }

    private void connect(BluetoothDevice device, boolean insecureConnection, boolean withPortTrick){
        BluetoothSocket socket = null;
        if(withPortTrick){
            socket = createBluetoothSocketWithPortTrick(device);
        }
        if(socket==null){
            try {
                if(insecureConnection){
                    socket = device.createInsecureRfcommSocketToServiceRecord(uuid);
                }
                else{
                    socket = device.createRfcommSocketToServiceRecord(uuid);
                }
            } catch (IOException e) {
                Log.w(getClass().getSimpleName(), e.getMessage());
                onError.postValue(DeviceError.FAILED_WHILE_CREATING_SOCKET);
            }
        }
        connectInThread(socket, device);
    }

    private void connectInThread(final BluetoothSocket socket, final BluetoothDevice device){
        new Thread(() -> {
            try {
                onBluetoothChangeStatus.postValue(BluetoothStatus.CONNECTING);
                socket.connect();
                connected = true;
                receiveThread = new ReceiveThread(readerClass, socket, device);
                onDeviceConnected.postValue(device);
                receiveThread.start();
                onBluetoothChangeStatus.postValue(BluetoothStatus.CONNECTED);
            } catch (final IOException e) {
                connected = false;
                onConnectError.postValue(e.getMessage());
                onBluetoothChangeStatus.postValue(BluetoothStatus.DISCONNECTED);
            }
        }).start();
    }

    private class ReceiveThread extends Thread implements Runnable{
        private SocketReader reader;
        private BluetoothSocket socket;
        private BluetoothDevice device;
        private OutputStream out;

        public ReceiveThread(Class<?> readerClass, BluetoothSocket socket, BluetoothDevice device) {
            this.socket = socket;
            this.device = device;
            try {
                out = socket.getOutputStream();
                InputStream in = socket.getInputStream();
                this.reader = (SocketReader) readerClass.getDeclaredConstructor(InputStream.class).newInstance(in);
            } catch (IOException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                Log.w(getClass().getSimpleName(), e.getMessage());
            }
        }

        public void run(){
            byte[] msg;
            try {
                while((msg = reader.read()) != null) {
                    final byte[] msgCopy = msg;
                    onMessage.postValue(msgCopy);
                }
            } catch (final IOException e) {
                connected = false;
                onDeviceDisconnected.postValue(e.getMessage());
                onBluetoothChangeStatus.postValue(BluetoothStatus.DISCONNECTED);
            }
        }

        public BluetoothSocket getSocket() {
            return socket;
        }

        public BluetoothDevice getDevice() {
            return device;
        }

        public OutputStream getOutputStream() {
            return out;
        }
    }

    private final BroadcastReceiver scanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action!=null) {
                switch (action) {
                    case BluetoothAdapter.ACTION_STATE_CHANGED:
                        final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                        if (state == BluetoothAdapter.STATE_OFF) {
                            onError.postValue(DiscoveryError.BLUETOOTH_DISABLED);
                        }
                        break;
                    case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
                        onDiscoveryStarted.postTrigger();
                        break;
                    case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                        context.unregisterReceiver(scanReceiver);

                        onDiscoveryFinished.postTrigger();
                        break;
                    case BluetoothDevice.ACTION_FOUND:
                        final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                        onDeviceFound.postValue(device);
                        break;
                }
            }
        }
    };

    private final BroadcastReceiver pairReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                final int prevState = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, BluetoothDevice.ERROR);

                if (state == BluetoothDevice.BOND_BONDED && prevState == BluetoothDevice.BOND_BONDING) {
                    context.unregisterReceiver(pairReceiver);
                    onDevicePaired.postValue(device);
                } else if (state == BluetoothDevice.BOND_NONE && prevState == BluetoothDevice.BOND_BONDED) {
                    context.unregisterReceiver(pairReceiver);
                    onDeviceUnpaired.postValue(device);
                }
            }
            else if(BluetoothDevice.ACTION_PAIRING_REQUEST.equals(action)){
                final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                try {
                    device.getClass().getMethod("setPairingConfirmation", boolean.class).invoke(device, true);
                } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    onError.postValue(-1);
                }
            }
        }
    };

    private final BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action!=null && action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        onBluetoothOff.postTrigger();
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        onBluetoothTurningOff.postTrigger();
                        break;
                    case BluetoothAdapter.STATE_ON:
                        onBluetoothOn.postTrigger();
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        onBluetoothTurningOn.postTrigger();
                        break;
                }
            }
        }
    };
}