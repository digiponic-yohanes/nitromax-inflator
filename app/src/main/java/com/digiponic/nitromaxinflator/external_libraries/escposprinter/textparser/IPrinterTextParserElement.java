package com.digiponic.nitromaxinflator.external_libraries.escposprinter.textparser;

import com.digiponic.nitromaxinflator.external_libraries.escposprinter.EscPosPrinterCommands;
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.exceptions.EscPosEncodingException;

public interface IPrinterTextParserElement {
    int length();
    IPrinterTextParserElement print(EscPosPrinterCommands printerSocket) throws EscPosEncodingException;
}
