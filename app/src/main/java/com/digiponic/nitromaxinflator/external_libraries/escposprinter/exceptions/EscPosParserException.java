package com.digiponic.nitromaxinflator.external_libraries.escposprinter.exceptions;

public class EscPosParserException extends Exception {
    public EscPosParserException(String errorMessage) {
        super(errorMessage);
    }
}
