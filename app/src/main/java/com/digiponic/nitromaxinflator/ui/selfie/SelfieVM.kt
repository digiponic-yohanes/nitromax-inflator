package com.digiponic.nitromaxinflator.ui.selfie

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.digiponic.nitromaxinflator.MainApplication
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.ancestors.BaseViewModel
import com.digiponic.nitromaxinflator.repository.network.request.RemoteRepository
import com.digiponic.nitromaxinflator.repository.network.response.CabangResponse
import com.digiponic.nitromaxinflator.repository.network.response.LoginResponse
import com.digiponic.nitromaxinflator.repository.preference.PreferenceRepository
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import javax.inject.Inject

class SelfieVM(activity: BaseActivity): BaseViewModel(activity = activity)  {
  companion object{
    const val CHECK_IN_REQUEST = "CHECK_IN_REQUEST"
    const val GET_CABANG_MESIN = "GET_CABANG_MESIN"
  }

  @Inject
  lateinit var remoteRepository: RemoteRepository

  @Inject
  lateinit var preferenceRepository: PreferenceRepository

  val imageAbsenData = MutableLiveData<File>()
  val successCheckIn = SingleEventWrapper<Void>()

  val imageAbsenUri = MutableLiveData<Uri>()

  val getUserLoginResponse = SingleEventWrapper<LoginResponse>()

  init { (activity.application as MainApplication).consumer.inject(this) }

  fun absen(username: String, userdata: LoginResponse.User, continuePrevious: String){
    viewModelScope.launch {
      val file = MultipartBody.Part.createFormData("foto",
        "${System.currentTimeMillis()}.png",
        imageAbsenData.value!!.asRequestBody("image/jpg".toMediaTypeOrNull()))
      val params = mutableMapOf<String, RequestBody>().apply {
        this["email"] = username.toRequestBody("text/plain".toMediaTypeOrNull())
        this["id_cabang"] = userdata.idCabang.toRequestBody("text/plain".toMediaTypeOrNull())
        this["is_continue_previous"] = continuePrevious.toRequestBody("text/plain".toMediaTypeOrNull())
      }
      activity.shouldShowLoading(tag = CHECK_IN_REQUEST)
      val result = safeApiCall(tag = CHECK_IN_REQUEST){
        remoteRepository.checkInKaryawan(
          params = params,
          foto = file
        )
      }
      activity.shouldHideLoading(tag = CHECK_IN_REQUEST)
      result.consume(
        onUnknownError = activity::onUnknownError,
        onTimeoutError = activity::onTimeoutError,
        onNetworkError = activity::onNetworkError,
        onHttpError = activity::onHttpError,
        onErrorResponse = activity::onErrorResponse,
        onSuccess = {
          preferenceRepository.saveUser(userdata)
          getUserLoginResponse.value?.let{ user ->
            preferenceRepository.saveUser(user.data)
          }
          preferenceRepository.addAbsensiId(it.id_absensi)
          successCheckIn.trigger()
        }
      )
    }
  }
}