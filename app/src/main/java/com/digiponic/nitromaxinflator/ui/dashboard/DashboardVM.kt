package com.digiponic.nitromaxinflator.ui.dashboard

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import com.digiponic.nitromaxinflator.MainApplication
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.ancestors.BaseViewModel
import com.digiponic.nitromaxinflator.extension.NonNullLiveData
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.EscPosPrinter
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.connection.bluetooth.BluetoothPrintersConnections
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.exceptions.EscPosConnectionException
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTICommand
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTIUpdateHargaMikro
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTReply
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.response.BTOutputResponse
import com.digiponic.nitromaxinflator.repository.network.parameter.PayTransactionEmergencyParams
import com.digiponic.nitromaxinflator.repository.network.parameter.PayTransactionParams
import com.digiponic.nitromaxinflator.repository.network.request.RemoteRepository
import com.digiponic.nitromaxinflator.repository.network.response.GroupRekapNitrogenResponse
import com.digiponic.nitromaxinflator.repository.network.response.ItemResponse
import com.digiponic.nitromaxinflator.repository.preference.PreferenceRepository
import com.digiponic.nitromaxinflator.sqlite.DatabaseHelper
import com.digiponic.nitromaxinflator.ui.absensi.AbsensiVM
import com.digiponic.nitromaxinflator.ui.payment.PaymentVM
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.mcnmr.utilities.extension.isNotEmptyOrNotNull
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.*
import org.json.JSONObject
import java.nio.charset.Charset
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DashboardVM(activity: BaseActivity) : BaseViewModel(activity) {
  companion object {
    const val GET_CABANG_REQUEST = "SET_CABANG_REQUEST"
    const val GET_ALL_ITEM_REQUEST = "ALL_ITEM_REQUEST"
    const val CHECK_OUT_KARYAWAN = "CHECK_OUT_KARYAWAN"
    const val SEND_CORRUPT_JSON = "SEND_CORRUPT_JSON"
    const val CHECK_JSON_STRING = "CHECK_JSON_STRING"

    const val KODE_KURAS_NITROGEN_MOTOR = 1
    const val KODE_KURAS_NITROGEN_MOBIL = 2
    const val KODE_TAMBAH_NITROGEN_MOTOR = 3
    const val KODE_TAMBAH_NITROGEN_MOBIL = 4
    const val KODE_TAMBAL_MOTOR = 7
    const val KODE_TAMBAL_MOBIL = 8
  }

  @Inject
  lateinit var remoteRepository: RemoteRepository

  @Inject
  lateinit var preferenceRepository: PreferenceRepository

  private val firebaseEvent = Firebase.analytics

  val selectedItem = MutableLiveData<ItemResponse.Item>()
  val quantityItem = NonNullLiveData<LinkedHashMap<ItemResponse.Item, Int>>(linkedMapOf())

  val getSkalaGagalResponse = SingleEventWrapper<Void>()
  val isLoadingItem = NonNullLiveData(false)
  val getItemResponse = SingleEventWrapper<ItemResponse>()
  val successPrintRekap = SingleEventWrapper<Void>()

  val onPrinterNotConnected = SingleEventWrapper<Void>()

  val userLogout = SingleEventWrapper<Void>()
  val getAbsensiResponse = SingleEventWrapper<MutableMap<String, String>>()

  val decodeResult = SingleEventWrapper<String>()
  val errorBrokenPipe = SingleEventWrapper<String>()
  val normalTransactionSuccess = SingleEventWrapper<Int>()
  val successRecovery = SingleEventWrapper<Void>()
  val listTransaksiRecovered = SingleEventWrapper<HashMap<BTOutputResponse, String>>()

  val isCekBocor = NonNullLiveData(false)

  private var searchItemJob: Job? = null

  private var decodedJson = 0
  private var successDecoded = 0
  private var failDecoded = 0

  var isRecoveryProcess = NonNullLiveData(false)

  var hasRecovery = false

  private var hargaKurasMotor: String? = null
  private var hargaTambahMotor: String? = null
  private var hargaTambalMotor: String? = null

  private var hargaKurasMobil: String? = null
  private var hargaTambahMobil: String? = null
  private var hargaTambalMobil: String? = null

  private var bluetoothOnMessageObserver: Observer<ByteArray>
  private lateinit var progressDialog: ProgressDialog

  val dbHelper = DatabaseHelper(activity)

  private var jsonStringsRecovery: MutableList<String> = mutableListOf()

  init {
    (activity.application as MainApplication).consumer.inject(this)

    bluetoothOnMessageObserver = Observer { message ->
      viewModelScope.launch {
        message?.let {
          val stringMessage = it.toString(Charsets.UTF_8)
          Log.e("Bluetooth Callback", "Message Received : $stringMessage")
          firebaseEvent.logEvent(FirebaseAnalytics.Param.CONTENT) {
            param("data", stringMessage)
          }
          try {
            val jsonObject = JSONObject(stringMessage)
            if (jsonObject.has("note")) {
              dbHelper.writeLogToFile(stringMessage, "Response")
              when (jsonObject.getString("note")) {
                "business" -> {
                  if ((jsonObject.has("status") && jsonObject.getString("status") == "ok")) {
                    Log.e("SUCCESS_UPDATE", "Berhasil update harga")
                  }
                }
                "recovery complete" -> {
                  //Log.e("SIZE_RECOVERY", jsonStringsRecovery.size.toString())
                  if (jsonStringsRecovery.size > 0) {
                    checkJsonStrings(jsonStringsRecovery)
                  } else {
                    successRecovery.trigger()
                  }
                }
              }
            }
            if (jsonObject.has("status") && jsonObject.getString("status") == "no content") {
              dbHelper.writeLogToFile(stringMessage, "Response")
              successRecovery.trigger()
            }
            if (jsonObject.has("mode")) {
              dbHelper.writeLogToFile(stringMessage, "Response")
              when (jsonObject.getString("mode")) {
                "output" ->  {
                  if (jsonObject.has("id_mode") && checkJsonValid(stringMessage)) {
                    when (jsonObject.getInt("id_mode")) {
                      0 -> app.saveNormalTransaction(Gson().fromJson(jsonObject.toString(), BTOutputResponse::class.java), stringMessage, activity, errorBrokenPipe, onPrinterNotConnected, normalTransactionSuccess, isCekBocor.value)
                      in 1..3 -> {
                        if (decodedJson == 0) isRecoveryProcess.value = true
                        decodedJson += 1
                        successDecoded += 1
                        jsonStringsRecovery.add(stringMessage)
                      }
                    }
                  } else {
                    decodedJson += 1
                    failDecoded += 1
                    sendCorruptedJson(stringMessage)
                  }
                }
                "communicationTest" -> {
                  dbHelper.writeLogToFile(Gson().toJson(BTReply()), "Input")
                  app.bluetooth.send(Gson().toJson(BTReply()), charset("utf-8"))
                }
              }
            }
          } catch (e: Exception){
            Log.e("ERROR", e.message!!)
            decodedJson += 1
            failDecoded += 1
            sendCorruptedJson(stringMessage)
          }
        }
      }
    }
  }

  private fun sendCorruptedJson(stringMessage: String){
    failDecoded += 1
    preferenceRepository.getUser()?.let { session ->
      viewModelScope.launch {
        val result = safeApiCall(tag = SEND_CORRUPT_JSON) {
          remoteRepository.sendCorruptJson(
            idCabang = session.idCabang,
            json = stringMessage
          )
        }
        result.consume(
          onUnknownError = activity::onUnknownError,
          onTimeoutError = activity::onTimeoutError,
          onNetworkError = activity::onNetworkError,
          onHttpError = activity::onHttpError,
          onErrorResponse = activity::onErrorResponse,
          onSuccess = { }
        )
      }
    }
    preferenceRepository.saveCorruptedJson(stringMessage)
  }

  fun onResume(){
    app.bluetooth.onMessage.observe(activity, bluetoothOnMessageObserver)
  }

  fun onPause(){
    app.bluetooth.onMessage.removeObserver(bluetoothOnMessageObserver)
  }

  fun updateSkalaGagal(){
    viewModelScope.launch {
      val result = safeApiCall(tag = GET_CABANG_REQUEST) {
        remoteRepository.getUserDataUpdated(
          id = preferenceRepository.getUser()!!.id
        )
      }
      result.consume(
        onUnknownError = activity::onUnknownError,
        onTimeoutError = activity::onTimeoutError,
        onNetworkError = activity::onNetworkError,
        onHttpError = activity::onHttpError,
        onErrorResponse = activity::onErrorResponse,
        onSuccess = {
          preferenceRepository.saveUser(it.data)
          getSkalaGagalResponse.trigger()
        }
      )
    }
  }

  private fun checkJsonStrings(jsonStrings: MutableList<String>) {
    var checkedTransaction = 0
    val penjualans = HashMap<BTOutputResponse, String>()
    val transactionToRecover = mutableListOf<String>()
    viewModelScope.executeAsyncTask(
      onPreExecute = {
        progressDialog = ProgressDialog(activity)
        progressDialog.setCancelable(false)
        progressDialog.setTitle("Harap Tunggu")
        progressDialog.setMessage("Melakukan pengecekan transaksi recovery")
        progressDialog.show()
      },
      doInBackground = { _: suspend (progress: Int) -> Unit ->
        for (list in jsonStrings) {
          val result = safeApiCall(CHECK_JSON_STRING) {
            remoteRepository.checkSingleJson(preferenceRepository.getUser()!!.idCabang, list)
          }
          result.consume(
            onUnknownError = { _, _ -> },
            onTimeoutError = {  },
            onNetworkError = { _, _ -> },
            onHttpError = { _, _, _ -> },
            onErrorResponse = { _, _ -> },
            onSuccess = {
              if (it.status) {
                if (it.data) transactionToRecover.add(list)
                checkedTransaction++
              }
            }
          )
        }
      },
      onPostExecute = {
        progressDialog.dismiss()
        if (transactionToRecover.isEmpty()) {
          app.bluetooth.send(Gson().toJson(BTICommand("command", "clearBackup")), charset("utf-8"))
          dbHelper.writeLogToFile("Input", Gson().toJson(BTICommand("command", "clearBackup")))
          Toast.makeText(activity, "Semua transaksi telah di-recovery. File backup akan dihapus", Toast.LENGTH_LONG).show()
          successRecovery.trigger()
        } else {
          successDecoded = transactionToRecover.size
          for (trans in transactionToRecover) {
            penjualans[Gson().fromJson(trans, BTOutputResponse::class.java)] = trans
          }
          listTransaksiRecovered.value = penjualans
        }
      },
      onProgressUpdate = {}
    )
  }

  fun getAbsensiKaryawan(){
    preferenceRepository.getUser()?.let {
      viewModelScope.launch {
        activity.shouldShowLoading(AbsensiVM.POST_ABSENSI)
        val result = safeApiCall(AbsensiVM.POST_ABSENSI) {
          remoteRepository.getAbsensiKaryawan(
            email = it.email,
            bulan = SimpleDateFormat("M", Locale.getDefault()).format(Date()).toString().toInt(),
            tahun = SimpleDateFormat("yyyy", Locale.getDefault()).format(Date()).toString().toInt()
          )
        }
        activity.shouldHideLoading(AbsensiVM.POST_ABSENSI)
        result.consume(
          onUnknownError = activity::onUnknownError,
          onTimeoutError = activity::onTimeoutError,
          onNetworkError = activity::onNetworkError,
          onHttpError = activity::onHttpError,
          onErrorResponse = activity::onErrorResponse,
          onSuccess = { response ->
            val listTanggalAbsen = mutableMapOf<String, String>()
            response.data.forEach {
              if (it.statusCheckIn != "Absen") {
                listTanggalAbsen[it.dateField] = it.statusCheckIn
              }
            }
            getAbsensiResponse.value = listTanggalAbsen
          }
        )
      }
    }
  }

  @SuppressLint("NullSafeMutableLiveData")
  fun getItemByCategory(id_tipe: String){
    selectedItem.value = null
    selectedItem.removeObservers(activity)
    quantityItem.removeObservers(activity)
    searchItemJob?.cancel()
    searchItemJob = viewModelScope.launch {
      preferenceRepository.getUser()?.let { user ->
        isLoadingItem.value = true
        val result = if (id_tipe == "55") {
          safeApiCall(tag = GET_ALL_ITEM_REQUEST) {
            remoteRepository.getAllProduk(id = user.idCabang)
          }
        } else {
          safeApiCall(tag = GET_ALL_ITEM_REQUEST) {
            remoteRepository.getAllJasa(id = user.idCabang)
          }
        }
        isLoadingItem.value = false
        result.consume(
          onUnknownError = activity::onUnknownError,
          onTimeoutError = {
            Handler(Looper.getMainLooper())
              .postDelayed({ getItemByCategory(id_tipe) }, 5000)
          },
          onNetworkError = { _, _ ->
            Handler(Looper.getMainLooper())
              .postDelayed({ getItemByCategory(id_tipe) }, 5000)
          },
          onHttpError = activity::onHttpError,
          onErrorResponse = activity::onErrorResponse,
          onSuccess = {
            getItemResponse.value = it
          }
        )
      }
    }
  }

  fun testPrint(){
    viewModelScope.launch(context = Dispatchers.IO) {
      try{
        val firstPairedDevice = BluetoothPrintersConnections.getInstance().selectFirstPaired()
        if(firstPairedDevice == null){
          activity.runOnUiThread {
            Toast.makeText(activity, "No connected printer", Toast.LENGTH_SHORT).show()
          }
        } else {
          val printer = EscPosPrinter(firstPairedDevice, 203,
            48f, 32
          )
          printer.printFormattedText("[C]Printer Berfungsi</u>")
        }
      } catch (e: EscPosConnectionException){
        if (e.message!!.contains("Broken pipe")) {
          activity.runOnUiThread {
            Toast.makeText(activity, "Koneksi ke printer terputus", Toast.LENGTH_SHORT).show()
          }
        } else {
          onPrinterNotConnected.postTrigger()
        }
      }
    }
  }

  @SuppressLint("NullSafeMutableLiveData")
  fun getAllItem(){
    selectedItem.value = null
    selectedItem.removeObservers(activity)
    quantityItem.removeObservers(activity)
    searchItemJob?.cancel()
    searchItemJob = viewModelScope.launch {
      preferenceRepository.getUser()?.let { user ->
        isLoadingItem.value = true
        val result = safeApiCall(tag = GET_ALL_ITEM_REQUEST){
          remoteRepository.getAllItem(
            id = user.idCabang
          )
        }
        isLoadingItem.value = false
        result.consume(
          onUnknownError = activity::onUnknownError,
          onTimeoutError = {
            Handler(Looper.getMainLooper())
              .postDelayed(::getAllItem, 5000)
          },
          onNetworkError = { _, _ ->
            Handler(Looper.getMainLooper())
              .postDelayed(::getAllItem, 5000)
          },
          onHttpError = activity::onHttpError,
          onErrorResponse = activity::onErrorResponse,
          onSuccess = {
            updateHargaMikro(it)
            getItemResponse.value = it
          }
        )
      }
    }
  }

  fun saveRecoveryTransaction(penjualans: HashMap<BTOutputResponse, String>){
    preferenceRepository.getUser()?.let { session ->
      val listPenjualan = mutableListOf<PayTransactionParams>()
      penjualans.forEach { item ->
        var qty = item.key.detail.jumlahBan
        val harga = item.key.harga
        val details = mutableListOf<PayTransactionParams.PayTransactionDetail>()
        val it = item.key.detail
        var keterangan = ""
        val pressures = item.key.pressure(it.modeTransaksi, harga, session.skalaGagal)
        var totalGagal = 0
        if (it.modeTransaksi in 1..4) {
          pressures.forEach { pressure ->
            totalGagal += if (pressure.isGagal && pressure.tekanan > 0) 1 else 0
            keterangan += "${pressure.tekananAwal}/${pressure.tekanan}, "
            if (pressure.tekanan < 17) qty -= 1
          }
        }
        val modeTransaksi = it.modeTransaksi
        details.add(PayTransactionParams.PayTransactionDetail(
          idItem = modeTransaksi.toString(),
          qty = if (modeTransaksi in 7..8) it.jumlahLubang else qty,
          gagal = if (modeTransaksi in 7..8) 0 else totalGagal,
          keterangan = keterangan
        ))
        val params = PayTransactionParams(
          idMode = item.key.idMode,
          user = session.email,
          cabang = session.idCabang,
          jenisKendaraan = item.key.jenisKendaraan.toString(),
          bayar = harga,
          detail = details,
          tanggal = item.key.tglTransaksi,
          metodePembayaran = 29,
          json_string = item.value
        )
        listPenjualan.add(params)
      }
      viewModelScope.launch {
        val result = safeApiCall(PaymentVM.POST_CHECKOUT) {
          val param = PayTransactionEmergencyParams(
            penjualan = listPenjualan
          )
          remoteRepository.recoveryTransaksi(param)
        }
        result.consume(
          onUnknownError = activity::onUnknownError,
          onTimeoutError = activity::onTimeoutError,
          onNetworkError = activity::onNetworkError,
          onHttpError = activity::onHttpError,
          onErrorResponse = activity::onErrorResponse,
          onSuccess = {
            if (it.status) {
              if(successDecoded != 0 || failDecoded != 0){
                decodeResult.value = "Sukses = $successDecoded, Gagal = $failDecoded"
              }
              successDecoded = 0
              failDecoded = 0
              successRecovery.trigger()
            }
          }
        )
      }
    }
  }

  private fun alertConnectionNotEstabilished(params: PayTransactionParams) {
    AlertDialog.Builder(activity).setTitle("Koneksi Belum Stabil")
      .setMessage("Pastikan koneksi sudah stabil sebelum melakukan recovery transaksi")
      .setCancelable(false)
      .setPositiveButton("OK") { _, _ ->
        savePendingTransaction(params)
      }
      .show()
  }

  fun getGroupRekapNitrogen(){
    viewModelScope.launch {
      val result = safeApiCall(tag = CHECK_OUT_KARYAWAN) {
        remoteRepository.rekapTransaksiKaryawanTiga(preferenceRepository.getAbsensiId()!!)
      }
      //activity.shouldHideLoading(tag = CHECK_OUT_KARYAWAN)
      result.consume(
        onUnknownError = activity::onUnknownError,
        onTimeoutError = activity::onTimeoutError,
        onNetworkError = activity::onNetworkError,
        onHttpError = activity::onHttpError,
        onErrorResponse = activity::onErrorResponse,
        onSuccess = {
          printGroupRekap(it.data)
        }
      )
    }
  }

  private fun printGroupRekap(data: GroupRekapNitrogenResponse.ModeTransaksi){
    preferenceRepository.getUser()?.let { session ->
      val firstPairedDevice = BluetoothPrintersConnections.getInstance().selectFirstPaired()
      if (firstPairedDevice == null) {
        Toast.makeText(activity, "Printer not connected", Toast.LENGTH_SHORT).show()
      } else {
        val printer = EscPosPrinter(BluetoothPrintersConnections.getInstance().selectFirstPaired(),203,48f,32)

        var text = "[C]<u><font size='big'>${session.header_cabang}</font></u>\n" +
            "[C]${session.namaCabang}\n" +
            "[L]\n" +
            "[L]Tanggal[R]${SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Date())}\n" +
            "[L]Operator[R]${session.name}\n" +
            "[C]================================\n"

        var totalNormal = 0
        var totalEmergency = 0

        text += "[C]==========MODE NORMAL===========\n"
        text += "[C]================================\n"

        for (items in data.normal) {
          if (items.total_cash > 0 || items.total_cashless > 0) {
            text += "[L]${items.keterangan}\n" +
                "[L]Cash[R](${NumberFormat.getInstance().format(items.jumlah_cash)}) ${NumberFormat.getInstance().format(items.total_cash)}\n" +
                "[L]Cashless[R](${NumberFormat.getInstance().format(items.jumlah_cashless)}) ${NumberFormat.getInstance().format(items.total_cashless)}\n" +
                "[L]\n"
          }

          totalNormal += (items.total_cash + items.total_cashless)
        }

        text += "[C]================================\n"
        text += "[C]=====MODE EMERGENCY HARI INI====\n"
        text += "[C]================================\n"

        for (items in data.emergency) {
          if (items.total_cash > 0) {
            text += "[L]${items.keterangan}\n" +
                "[L](${NumberFormat.getInstance().format(items.jumlah_cash)}) [R]${NumberFormat.getInstance().format(items.total_cash)}\n" +
                "[L]\n"
          }

          totalEmergency += (items.total_cash + items.total_cashless)
        }

        text += "[C]--------------------------------\n" +
            "[L]NORMAL[R]${NumberFormat.getInstance().format(totalNormal)}\n" +
            "[L]EMERGENCY[R]${NumberFormat.getInstance().format(totalEmergency)}\n" +
            "[C]================================\n" +
            "[L]\n" +
            "[C]Dicetak pada : ${SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault()).format(Date())}\n" +
            "[C]Powered by : DIGIPONIC\n" +
            "[L]\n"

        try {
          printer.printFormattedText(text)
        } catch (e: Exception) {
          Toast.makeText(activity, e.message, Toast.LENGTH_SHORT).show()
        }
      }
      successPrintRekap.trigger()
    }
  }

  fun total(data: BTOutputResponse): Int {
    var total = 0
    preferenceRepository.getUser()?.let { session ->
      val item = data.detail

      if (item.modeTransaksi in 1..4) {
        var hargaJasa = 0
        when (item.modeTransaksi) {
          1 -> hargaJasa = hargaKurasMotor?.toInt() ?: 0
          2 -> hargaJasa = hargaKurasMobil?.toInt() ?: 0
          3 -> hargaJasa = hargaTambahMotor?.toInt() ?: 0
          4 -> hargaJasa = hargaTambahMobil?.toInt() ?: 0
        }

        val pressures = data.pressure(item.modeTransaksi, hargaJasa, session.skalaGagal)
        pressures.forEach { pressure ->
          total += pressure.harga
        }
      } else if (item.modeTransaksi in 7..8) {
        var hargaJasa = 0
        if (item.modeTransaksi == 7) {
          hargaJasa = hargaTambalMotor?.toInt() ?: 0
        } else if (item.modeTransaksi == 8) {
          hargaJasa = hargaTambahMobil?.toInt() ?: 0
        }
        total += hargaJasa * item.jumlahBan
      }
    }
    return total
  }

  fun logout(){
    viewModelScope.launch {
      activity.shouldShowLoading(tag = CHECK_OUT_KARYAWAN)
      val result = safeApiCall(tag = CHECK_OUT_KARYAWAN) {
        remoteRepository.checkOutKaryawan(
          id_absensi = preferenceRepository.getAbsensiId()!!
        )
      }
      activity.shouldHideLoading(tag = CHECK_OUT_KARYAWAN)
      result.consume(
        onUnknownError = activity::onUnknownError,
        onTimeoutError = activity::onTimeoutError,
        onNetworkError = activity::onNetworkError,
        onHttpError = activity::onHttpError,
        onErrorResponse = activity::onErrorResponse,
        onSuccess = {
          userLogout.trigger()
        }
      )
    }
  }

  private fun updateHargaMikro(it: ItemResponse){
    preferenceRepository.getUser()?.let { session ->
      it.data.forEach { item ->
        when (item.id) {
          KODE_KURAS_NITROGEN_MOTOR -> hargaKurasMotor = item.hargaJual
          KODE_TAMBAH_NITROGEN_MOTOR -> hargaTambahMotor = item.hargaJual
          KODE_TAMBAL_MOTOR -> hargaTambalMotor = item.hargaJual
          KODE_KURAS_NITROGEN_MOBIL -> hargaKurasMobil = item.hargaJual
          KODE_TAMBAH_NITROGEN_MOBIL -> hargaTambahMobil = item.hargaJual
          KODE_TAMBAL_MOBIL -> hargaTambalMobil = item.hargaJual
        }
      }
      val updateMikroParams =
        BTIUpdateHargaMikro(
          motor = BTIUpdateHargaMikro.Produk(
            tambah = if (hargaTambahMotor.isNotEmptyOrNotNull()) hargaTambahMotor!!.toInt() else 0,
            tambal = if (hargaTambalMotor.isNotEmptyOrNotNull()) hargaTambalMotor!!.toInt() else 0,
            kuras = if (hargaKurasMotor.isNotEmptyOrNotNull()) hargaKurasMotor!!.toInt() else 0,
            gagal = session.skalaGagal
          ),
          mobil = BTIUpdateHargaMikro.Produk(
            tambah = if (hargaTambahMobil.isNotEmptyOrNotNull()) hargaTambahMobil!!.toInt() else 0,
            tambal = if (hargaTambalMobil.isNotEmptyOrNotNull()) hargaTambalMobil!!.toInt() else 0,
            kuras = if (hargaKurasMobil.isNotEmptyOrNotNull()) hargaKurasMobil!!.toInt() else 0,
            gagal = session.skalaGagal
          )
        )
      Handler(Looper.getMainLooper())
        .postDelayed({
          try {
            dbHelper.writeLogToFile(Gson().toJson(updateMikroParams), "Input")
            app.bluetooth.send(Gson().toJson(updateMikroParams), Charset.forName("utf-8"))
          } catch (e: Exception) {
            Log.e("Exception", e.localizedMessage ?: "")
          }
        }, 10000)
    }
  }

  fun savePendingTransaction(params: PayTransactionParams) {
    viewModelScope.launch {
      activity.shouldShowLoading(PaymentVM.POST_CHECKOUT)
      val result = safeApiCall(PaymentVM.POST_CHECKOUT) { remoteRepository.newCheckout(params) }
      activity.shouldHideLoading(PaymentVM.POST_CHECKOUT)
      result.consume(
        onUnknownError = activity::onUnknownError,
        onTimeoutError = {
          alertConnectionNotEstabilished(params)
        },
        onNetworkError = { _, _ ->
          alertConnectionNotEstabilished(params)
        },
        onHttpError = activity::onHttpError,
        onErrorResponse = activity::onErrorResponse,
        onSuccess = {
          val transaksi = it.data
          dbHelper.deletePendingTransaction()
          app.printWithProgressBar(transaksi, activity, errorBrokenPipe, onPrinterNotConnected, normalTransactionSuccess)
        }
      )
    }
  }

  private fun <P, R> CoroutineScope.executeAsyncTask(
    onPreExecute: () -> Unit,
    doInBackground: suspend (suspend (P) -> Unit) -> R,
    onPostExecute: (R) -> Unit,
    onProgressUpdate: (P) -> Unit
  ) = launch {
    onPreExecute()

    val result = withContext(Dispatchers.IO) {
      doInBackground {
        withContext(Dispatchers.Main) { onProgressUpdate(it) }
      }
    }
    onPostExecute(result)
  }

  private fun checkJsonValid(jsonString: String): Boolean {
    val jsonObject = JSONObject(jsonString)
    var isValidString = jsonObject.has("mode") && jsonObject.has("status") && jsonObject.has("id_mode") && jsonObject.has("no_transaksi") && jsonObject.has("tgl_transaksi") && jsonObject.has("jenis_kendaraan") && jsonObject.has("harga")
    isValidString = if (jsonObject.has("detail")) {
      val jsonChild = jsonObject.getJSONObject("detail")
      isValidString && (jsonChild.has("mode_transaksi") && jsonChild.has("jumlah_ban") && jsonChild.has("jumlah_error") && jsonChild.has("jumlah_lubang") && jsonChild.has("jumlah_cekbocor") && jsonChild.has("tekanan_akhir") && jsonChild.has("tekanan_awal")) && jsonChild.length() == 7
    } else {
      false
    }
    return isValidString
  }
}