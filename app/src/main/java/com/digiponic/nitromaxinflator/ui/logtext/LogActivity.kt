package com.digiponic.nitromaxinflator.ui.logtext

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.digiponic.nitromaxinflator.sqlite.NitromaxDb
import com.digiponic.nitromaxinflator.ui.logtext.adapter.LogtextAdapter
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.extension.instantiateViewModel
import kotlinx.android.synthetic.main.activity_log.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class LogActivity : BaseActivity() {

  private val vm by lazy { instantiateViewModel<LogVM>() }

  private val database = NitromaxDb.getInstance(this)

  private val logtextDao = database.logtextDao()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_log)

    cabangNama.text = vm.preferenceRepository.getUser()!!.namaCabang

    val listData = logtextDao.getAll()
    main_rv_list_data.layoutManager = LinearLayoutManager(this)

    main_rv_list_data.layoutManager = LinearLayoutManager(this)

    main_rv_list_data.adapter = LogtextAdapter(
      activity = this,
      log = listData
    )
  }

  override fun onUserLeaveHint() {
    super.onUserLeaveHint()
    app.saveAppHistory("Keluar dari aplikasi dengan memencet tombol Home")
  }
}