package com.digiponic.nitromaxinflator.ui.payment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.RecyclerView
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.extension.NonNullLiveData
import com.mcnmr.utilities.extension.getCompatColor
import kotlinx.android.synthetic.main.row_pilih_pressure.view.*

class PilihPressureAdapter(val context: Context,
                           val listPressure: IntArray,
                           val bluetoothLoadingModeTransaction: NonNullLiveData<Boolean>,
                           val bluetoothStatusModeTransaction: NonNullLiveData<Boolean>,
                           val onProsesPressureSelected: (Int) -> Unit, val onUpdatePressureSelected: (Int) -> Unit ): RecyclerView.Adapter<PilihPressureAdapter.PilihPressureVH>() {

  override fun getItemCount(): Int = listPressure.size

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PilihPressureVH = PilihPressureVH(LayoutInflater.from(context).inflate(R.layout.row_pilih_pressure, parent, false))

  override fun onBindViewHolder(holder: PilihPressureVH, position: Int) {
    holder.btnPilihPressure.text = listPressure[position].toString()
    holder.btnPilihPressure.setOnClickListener {

      if (bluetoothStatusModeTransaction.value) {
        onUpdatePressureSelected(listPressure[position])
      } else {
        onProsesPressureSelected(listPressure[position])
      }
    }

    bluetoothLoadingModeTransaction.observeForever {
      holder.btnPilihPressure.isEnabled = it.not()
      holder.btnPilihPressure.setBackgroundColor(if(it.not()) context.getCompatColor(R.color.colorPrimary) else context.getCompatColor(R.color.graydddd))
    }
  }

  inner class PilihPressureVH(v: View): RecyclerView.ViewHolder(v) {
   val btnPilihPressure: AppCompatButton = v.btnPilihPressure
  }
}