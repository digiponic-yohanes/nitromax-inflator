package com.digiponic.nitromaxinflator.ui.force_close

import android.os.Bundle
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.mcnmr.utilities.internal_plugin.StringIntent
import kotlinx.android.synthetic.main.activity_force_close.*

class ForceCloseActivity : BaseActivity() {

    companion object{
        const val ERROR_MESSAGE_ARGUMENT = "ERROR_MESSAGE_ARGUMENT"
    }

    @StringIntent(ERROR_MESSAGE_ARGUMENT)
    lateinit var errorMessageArgument: String

    override fun shouldCatchForceClose(): Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_force_close)

        tvMessage.text = errorMessageArgument
    }
}