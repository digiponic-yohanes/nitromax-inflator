package com.digiponic.nitromaxinflator.ui.dashboard.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.response.BTOutputResponse
import kotlinx.android.synthetic.main.row_transaksi_to_recover.view.*
import java.text.NumberFormat
import java.util.HashMap

class TransEmergencyAdapter(private val activity: BaseActivity,
                            private var transaksi: HashMap<BTOutputResponse, String>): RecyclerView.Adapter<TransEmergencyAdapter.TransaksiVH>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransaksiVH =
    TransaksiVH(LayoutInflater.from(activity).inflate(R.layout.row_transaksi_to_recover, parent, false))

  override fun getItemCount(): Int = transaksi.size

  override fun onBindViewHolder(holder: TransaksiVH, position: Int) {
    val key = transaksi.keys.toList()[position]
    var keterangan = ""
    val mode = when (key.idMode) {
      0 -> "P"
      1 -> "E"
      in 2..3 -> "B"
      else -> "R"
    }
    val namaTransaksi = when (key.detail.modeTransaksi) {
      1 -> "Isi Baru Nitrogen (Motor)"
      2 -> "Isi Baru Nitrogen (Mobil)"
      3 -> "Isi Tambah Nitrogen (Motor)"
      4 -> "Isi Tambah Nitrogen (Mobil)"
      5 -> "Check Ban Bocor (Motor)"
      6 -> "Check Ban Bocor (Mobil)"
      7 -> "Tambal Ban Tubeless (Motor)"
      8 -> "Tambal Ban Tubeless (Mobil)"
      else -> ""
    }
    if (key.detail.modeTransaksi in 1..6) {
      if (!key.detail.tekanan!!.contains(",")) keterangan += "${key.detail.tekananAwal}/${key.detail.tekanan}"

      val tekanan = key.detail.tekanan.split(",")
      val tekananAwal = key.detail.tekananAwal!!.split(",")

      for (i in 0..tekanan.size - 2) {
        keterangan += "${tekananAwal[i]}/${tekanan[i]}"
        if (tekanan[i].toInt() < 17) keterangan += " (cancel)"
        if (i <= tekanan.size - 3) keterangan += ", "
      }
    }
    holder.tvTanggalEmg.text = key.tglTransaksi
    holder.tvNoTransaksi.text = "$mode-${key.noTransaksi}"
    holder.tvNamaItem.text = namaTransaksi
    holder.tvQtyEmg.text = when (key.detail.modeTransaksi) {
      in 1..4 -> "Jumlah Ban : ${key.detail.jumlahBan} ($keterangan)"
      in 5..6 -> "Tekanan : $keterangan"
      in 7..8 -> "Jumlah Lubang/Karet : ${key.detail.jumlahLubang}"
      else -> ""
    }
    holder.tvHargaEmg.text = "Rp ${NumberFormat.getInstance().format(key.harga)}"
  }

  inner class TransaksiVH(v: View): RecyclerView.ViewHolder(v){
    val tvNoTransaksi: TextView = v.tvNoTransaksiEmg
    val tvNamaItem: TextView = v.tvNamaItemEmg
    val tvQtyEmg: TextView = v.tvQtyEmg
    val tvTanggalEmg: TextView = v.tvTanggalEmg
    val tvHargaEmg: TextView = v.tvHargaEmg
  }
}