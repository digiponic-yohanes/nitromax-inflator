package com.digiponic.nitromaxinflator.ui.dashboard.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SwipeLayout
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.repository.network.response.ItemResponse
import kotlinx.android.synthetic.main.row_dashboard_cart.view.*

class DashboardCartAdapter(private val context: Context,
                           private var carts: LinkedHashMap<ItemResponse.Item, Int>,
                           private var onDelete: (ItemResponse.Item) -> Unit) :
    RecyclerView.Adapter<DashboardCartAdapter.DashboardCartVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardCartVH =
        DashboardCartVH(LayoutInflater.from(context).inflate(R.layout.row_dashboard_cart, parent, false))

    override fun getItemCount(): Int = carts.size

    override fun onBindViewHolder(holder: DashboardCartVH, position: Int) {
        val key = carts.keys.toList()[position]

        holder.slContainer.isSwipeEnabled = key.idKategori == "82"

        holder.tvItemName.text = key.keterangan
        holder.tvQuantity.text = carts[key].toString()
        holder.tvItemPrice.text = (key.hargaJual.toInt() * (carts[key] ?: 0)).toString()

        holder.llDelete.setOnClickListener { onDelete(key) }
    }

    fun setNewItem(newItem: LinkedHashMap<ItemResponse.Item, Int>){
        this.carts = newItem
        notifyDataSetChanged()
    }

    inner class DashboardCartVH(v: View) : RecyclerView.ViewHolder(v) {
        val llDelete: LinearLayout = v.llDelete
        val slContainer: SwipeLayout = v.slContainer

        val tvItemName: TextView = v.tvItemName
        val tvQuantity: TextView = v.tvItemQuantity
        val tvItemPrice: TextView = v.tvItemPrice
    }
}