package com.digiponic.nitromaxinflator.ui.transaksi

import android.app.AlertDialog
import android.os.Bundle
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.extension.instantiateViewModel
import com.digiponic.nitromaxinflator.ui.transaksi.adapter.TransaksiAdapter
import com.mcnmr.utilities.extension.gone
import com.mcnmr.utilities.extension.visible
import kotlinx.android.synthetic.main.activity_transaksi.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.toast
import java.text.NumberFormat

class TransaksiActivity : BaseActivity() {
  private val vm by lazy { instantiateViewModel<TransaksiVM>() }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_transaksi)

    cabangNama.text = vm.preferenceRepository.getUser()!!.namaCabang

    setSupportActionBar(toolbar)
    supportActionBar?.apply {
      setDisplayShowTitleEnabled(false)
      setDisplayHomeAsUpEnabled(true)
      setDisplayShowHomeEnabled(true)
    }

    rvTransaksi.layoutManager = LinearLayoutManager(this)

    rvTransaksi.adapter = TransaksiAdapter(
      activity = this,
      transaksi = listOf(),
      onClicked = {
        vm.print(it)
      }
    )

    vm.errorBrokenPipe.observe(this) {
      toast(it)
    }

    vm.dataTransaksiResponse.observe(this) {
      (rvTransaksi.adapter as TransaksiAdapter).setTransaksi(it.data)
    }

    vm.dataCashResponse.observe(this) {
      tvCashYesterdayValue.text = "Rp ${NumberFormat.getInstance().format(it.data.yesterday)}"
      tvCashTodayValue.text = "Rp ${NumberFormat.getInstance().format(it.data.today)}"
    }

    vm.successOpenCash.observe(this) {
      btnLihatTotalCash.gone()
      clCash.visible()
    }

    btnLihatTotalCash.setOnClickListener {
      val textInput = EditText(this)
      textInput.inputType = InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD
      textInput.transformationMethod = PasswordTransformationMethod.getInstance()
      textInput.setPadding(50,30,50,30)
      textInput.hint = "Masukkan Password (Sesuai Password Login Operator)"
      textInput.background = null
      textInput.textAlignment = View.TEXT_ALIGNMENT_CENTER

      val alert = AlertDialog.Builder(this).setTitle("Masukkan Password Untuk Melihat Total Cash")
        .setView(textInput).setPositiveButton("Submit") { _, _ ->
          val passwordEntered = textInput.text.toString()
          if (passwordEntered == "") {
            alertDialog("Kesalahan", "Harap masukkan password")
          } else {
            vm.enterCredentials(passwordEntered)
          }
        }.setNegativeButton("Cancel") { dialog, _ ->
          dialog.cancel()
        }.create()
      alert.show()
    }

    vm.loadAllTransaksi()
    vm.loadCashKaryawan()
  }

  override fun onUserLeaveHint() {
    super.onUserLeaveHint()
    app.saveAppHistory("Keluar dari aplikasi dengan memencet tombol Home")
  }
}