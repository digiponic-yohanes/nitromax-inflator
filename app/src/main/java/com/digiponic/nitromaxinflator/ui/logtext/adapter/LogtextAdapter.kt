package com.digiponic.nitromaxinflator.ui.logtext.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.digiponic.nitromaxinflator.sqlite.data.Logtext
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import kotlinx.android.synthetic.main.row_log.view.*

class LogtextAdapter(private val activity: BaseActivity, private var log: List<Logtext>): RecyclerView.Adapter<LogtextAdapter.LogtextVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LogtextAdapter.LogtextVH = LogtextVH(LayoutInflater.from(activity).inflate(
        R.layout.row_log, parent, false))

    override fun getItemCount(): Int = log.size

    override fun onBindViewHolder(holder: LogtextAdapter.LogtextVH, position: Int) {
        holder.tvTanggal.text = log[position].date
        holder.tvLog.text = log[position].text
    }

    inner class LogtextVH(v: View): RecyclerView.ViewHolder(v){
        val tvTanggal: TextView = v.textTanggal
        val tvLog: TextView = v.textLog
    }
}