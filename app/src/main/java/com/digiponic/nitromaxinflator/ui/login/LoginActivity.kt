package com.digiponic.nitromaxinflator.ui.login

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.extension.instantiateViewModel
import com.digiponic.nitromaxinflator.ui.dashboard.DashboardActivity
import com.digiponic.nitromaxinflator.ui.selfie.SelfieActivity
import com.jakewharton.rxbinding4.widget.textChanges
import com.mcnmr.utilities.extension.getCompatDrawable
import com.mcnmr.utilities.internal_plugin.StringIntent
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity

class LoginActivity : BaseActivity() {

  companion object {
    const val CABANG_ARGUMENT = "USERNAME_ARGUMENT"
  }

  private val vm by lazy { instantiateViewModel<LoginVM>() }

  @StringIntent(CABANG_ARGUMENT)
  lateinit var cabang: String

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_login)

    vm.cabangData.value = cabang

    vm.validation.observe(this) {
      btnLogin.isEnabled = it
      btnLogin.background = getCompatDrawable(if(it) R.drawable.btn_blue_bg else R.drawable.btn_grey_bg)
    }

    vm.alreadyLoggedIn.observe(this) {
      startActivity<DashboardActivity>()
      finishAffinity()
    }

    vm.successLogin.observe(this) {
      var isContinuePrevious = "0"
      if (it.has_previous_login) {
        alertDialog(
          "Konfirmasi",
          "Anda sudah login pada sesi tepat sebelum login ini. Apakah Anda ingin melanjutkan sesi login sebelumnya?",
          "Tidak", {
            isContinuePrevious = "0"
            startActivity<SelfieActivity>(
              SelfieActivity.USERNAME_ARGUMENT to (vm.usernameData.value ?: ""),
              SelfieActivity.LOGGED_USER_ARGUMENT to it,
              SelfieActivity.CONTINUE_PREVIOUS_ARGUMENT to isContinuePrevious
            )
            finishAffinity()
          },
          "Ya", {
            isContinuePrevious = "1"
            startActivity<SelfieActivity>(
              SelfieActivity.USERNAME_ARGUMENT to (vm.usernameData.value ?: ""),
              SelfieActivity.LOGGED_USER_ARGUMENT to it,
              SelfieActivity.CONTINUE_PREVIOUS_ARGUMENT to isContinuePrevious
            )
            finishAffinity()
          }
        )
      } else {
        startActivity<SelfieActivity>(
          SelfieActivity.USERNAME_ARGUMENT to (vm.usernameData.value ?: ""),
          SelfieActivity.LOGGED_USER_ARGUMENT to it,
          SelfieActivity.CONTINUE_PREVIOUS_ARGUMENT to isContinuePrevious
        )
      }
    }

    btnShowPassword.setOnClickListener {
      if (etPassword.transformationMethod.equals(PasswordTransformationMethod.getInstance())) {
        etPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
        btnShowPassword.setBackgroundResource(R.drawable.ic_visibility_off)
      } else {
        etPassword.transformationMethod = PasswordTransformationMethod.getInstance()
        btnShowPassword.setBackgroundResource(R.drawable.ic_visibility)
      }
    }
    etUsername.textChanges().subscribe { vm.usernameData.value = it.toString() }
    etPassword.textChanges().subscribe { vm.passwordData.value = it.toString() }

    btnLogin.setOnClickListener { vm.login() }

    vm.checkSession()
  }
}