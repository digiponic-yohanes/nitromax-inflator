package com.digiponic.nitromaxinflator.ui.absensi

import android.os.Bundle
import android.util.Log
import android.view.View
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.extension.instantiateViewModel
import com.digiponic.nitromaxinflator.ui.absensi.adapter.CalendarDayVH
import com.digiponic.nitromaxinflator.ui.absensi.adapter.CalendarMonthVH
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.mcnmr.utilities.extension.getCompatColor
import com.mcnmr.utilities.extension.getCompatDrawable
import com.mcnmr.utilities.internal_plugin.SerializableIntent
import kotlinx.android.synthetic.main.activity_absensi.*
import kotlinx.android.synthetic.main.activity_transaksi.toolbar
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.time.YearMonth
import java.time.temporal.WeekFields
import java.util.*

class AbsensiActivity : BaseActivity() {

  companion object{
    const val ABSENSI_ARGUMENT = "ABSENSI_ARGUMENT"
  }

  @SerializableIntent(ABSENSI_ARGUMENT)
  lateinit var absensi: MutableMap<String, String>

  private val vm by lazy { instantiateViewModel<AbsensiVM>() }
  private val months by lazy {
    arrayOf("", "Januari", "Februari", "Maret", "April",
      "Mei", "Juni", "Juli", "Agustus", "September",
      "Oktober", "November", "Desember"
    )
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_absensi)

    setSupportActionBar(toolbar)
    supportActionBar?.apply {
      setDisplayShowTitleEnabled(false)
      setDisplayHomeAsUpEnabled(true)
      setDisplayShowHomeEnabled(true)
    }

    cabangNama.text = vm.preferenceRepository.getUser()!!.namaCabang
    textOperatorView.text = "Nama Operator : " + vm.preferenceRepository.getUser()!!.name

    val currentMonth = YearMonth.now()
    val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek

    calendarView.dayBinder = object : DayBinder<CalendarDayVH> {
      override fun create(view: View): CalendarDayVH = CalendarDayVH(view)
      override fun bind(container: CalendarDayVH, day: CalendarDay) {
        container.tvCalendarDay.text = day.date.dayOfMonth.toString()

        val dDay = if(day.date.dayOfMonth < 10) "0${day.date.dayOfMonth}" else day.date.dayOfMonth
        val dMonth = if((day.date.monthValue) < 10) "0${day.date.monthValue}" else day.date.monthValue
        val dYear = day.date.year
        val currentDay = "${dYear}-${dMonth}-${dDay}"
        if(absensi.containsKey(currentDay)){
          Log.e("STATUS", absensi[currentDay]!!)
          if (absensi[currentDay].equals("Absen")) {
            container.tvCalendarDay.setTextColor(getCompatColor(android.R.color.white))
            container.llContainer.background = getCompatDrawable(R.drawable.bg_absence_day)
          } else if (!absensi[currentDay].equals("Belum Absen")) {
            container.tvCalendarDay.setTextColor(getCompatColor(android.R.color.white))
            container.llContainer.background = getCompatDrawable(R.drawable.bg_present_day)
          }
        }
      }
    }
    calendarView.monthHeaderBinder = object : MonthHeaderFooterBinder<CalendarMonthVH> {
      override fun create(view: View): CalendarMonthVH = CalendarMonthVH(view)
      override fun bind(container: CalendarMonthVH, month: CalendarMonth) {
        container.tvCalendarMonth.text = months[month.month]
      }
    }

    calendarView.setup(currentMonth, currentMonth, firstDayOfWeek)
    calendarView.scrollToMonth(currentMonth)
  }

  override fun onUserLeaveHint() {
    super.onUserLeaveHint()
    app.saveAppHistory("Keluar dari aplikasi dengan memencet tombol Home")
  }
}