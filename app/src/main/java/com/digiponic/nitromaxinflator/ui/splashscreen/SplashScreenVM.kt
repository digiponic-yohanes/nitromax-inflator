package com.digiponic.nitromaxinflator.ui.splashscreen

import androidx.lifecycle.viewModelScope
import com.digiponic.nitromaxinflator.MainApplication
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.ancestors.BaseViewModel
import com.digiponic.nitromaxinflator.repository.network.request.RemoteRepository
import com.digiponic.nitromaxinflator.repository.network.response.CabangResponse
import com.digiponic.nitromaxinflator.repository.preference.PreferenceRepository
import com.digiponic.nitromaxinflator.sqlite.DatabaseHelper
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.launch
import javax.inject.Inject

class SplashScreenVM(activity: BaseActivity): BaseViewModel(activity = activity) {
  companion object {
    const val GET_CABANG_MESIN = "GET_CABANG_MESIN"
  }

  @Inject
  lateinit var remoteRepository: RemoteRepository

  @Inject
  lateinit var preferenceRepository: PreferenceRepository

  val getCabangMesinResponse = SingleEventWrapper<CabangResponse>()

  val database = DatabaseHelper(activity)

  init {
    (activity.application as MainApplication).consumer.inject(this)
  }

  fun checkBluetoothName(): Boolean {
    return database.checkBluetoothNameExist()
  }

  fun getCabangByMesin(macMesin: String) {
    viewModelScope.launch {
      activity.shouldShowLoading(GET_CABANG_MESIN)
      val result = safeApiCall(GET_CABANG_MESIN) {
        remoteRepository.getCabangMac(macMesin)
      }
      activity.shouldHideLoading(GET_CABANG_MESIN)
      result.consume(
        onUnknownError = activity::onUnknownError,
        onTimeoutError = activity::onTimeoutError,
        onNetworkError = activity::onNetworkError,
        onHttpError = activity::onHttpError,
        onErrorResponse = activity::onErrorResponse,
        onSuccess = {
          getCabangMesinResponse.value = it
        }
      )
    }
  }
}