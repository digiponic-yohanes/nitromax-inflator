package com.digiponic.nitromaxinflator.ui.absensi

import androidx.lifecycle.viewModelScope
import com.digiponic.nitromaxinflator.MainApplication
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.ancestors.BaseViewModel
import com.digiponic.nitromaxinflator.repository.network.request.RemoteRepository
import com.digiponic.nitromaxinflator.repository.network.response.AbsensiResponse
import com.digiponic.nitromaxinflator.repository.preference.PreferenceRepository
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.launch
import javax.inject.Inject

class AbsensiVM(activity: BaseActivity) : BaseViewModel(activity) {
    companion object {
        const val POST_ABSENSI = "POST_ABSENSI"
    }
    @Inject
    lateinit var remoteRepository: RemoteRepository
    @Inject
    lateinit var preferenceRepository: PreferenceRepository

    init {
        (activity.application as MainApplication).consumer.inject(this)
    }

    val getAbsensiResponse = SingleEventWrapper<AbsensiResponse>()

    fun getAbsensi(bulan: Int, tahun: Int){
        preferenceRepository.getUser()?.let {
            viewModelScope.launch {
                activity.shouldShowLoading(POST_ABSENSI)
                val result = safeApiCall(POST_ABSENSI) {
                    remoteRepository.getAbsensiKaryawan(
                        email = it.email,
                        bulan = bulan,
                        tahun = tahun
                    )
                }
                activity.shouldHideLoading(POST_ABSENSI)
                result.consume(
                    onUnknownError = activity::onUnknownError,
                    onTimeoutError = activity::onTimeoutError,
                    onNetworkError = activity::onNetworkError,
                    onHttpError = activity::onHttpError,
                    onErrorResponse = activity::onErrorResponse,
                    onSuccess = {
                        getAbsensiResponse.value = it
                    }
                )
            }
        }
    }

    fun getAbsensiYear(tahun: Int){
        preferenceRepository.getUser()?.let {
            viewModelScope.launch {
                activity.shouldShowLoading(POST_ABSENSI)
                val result = safeApiCall(POST_ABSENSI) {
                    remoteRepository.getAbsensiKaryawanYear(
                        email = it.email,
                        tahun = tahun
                    )
                }
                activity.shouldHideLoading(POST_ABSENSI)
                result.consume(
                    onUnknownError = activity::onUnknownError,
                    onTimeoutError = activity::onTimeoutError,
                    onNetworkError = activity::onNetworkError,
                    onHttpError = activity::onHttpError,
                    onErrorResponse = activity::onErrorResponse,
                    onSuccess = {
                        getAbsensiResponse.value = it
                    }
                )
            }
        }
    }
}