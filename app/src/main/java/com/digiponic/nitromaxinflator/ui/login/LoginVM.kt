package com.digiponic.nitromaxinflator.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.digiponic.nitromaxinflator.MainApplication
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.ancestors.BaseViewModel
import com.digiponic.nitromaxinflator.repository.network.request.RemoteRepository
import com.digiponic.nitromaxinflator.repository.network.response.LoginResponse
import com.digiponic.nitromaxinflator.repository.preference.PreferenceRepository
import com.mcnmr.utilities.extension.isNotEmptyOrNotNull
import com.mcnmr.utilities.internal_plugin.combineLiveData
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginVM(activity: BaseActivity): BaseViewModel(activity = activity) {
  companion object{
    const val LOGIN_REQUEST = "LOGIN_REQUEST"
  }

  @Inject
  lateinit var remoteRepository: RemoteRepository

  @Inject
  lateinit var preferenceRepository: PreferenceRepository

  val usernameData = MutableLiveData<String>()
  val passwordData = MutableLiveData<String>()
  val cabangData = MutableLiveData<String>()
  val successLogin = SingleEventWrapper<LoginResponse.User>()
  val alreadyLoggedIn = SingleEventWrapper<Void>()

  val validation = combineLiveData(usernameData, passwordData){
      a, b -> a.isNotEmptyOrNotNull() && b.isNotEmptyOrNotNull()
  }

  init {
    (activity.application as MainApplication).consumer.inject(this)
  }

  fun checkSession(){
    preferenceRepository.getUser()?.let {
      alreadyLoggedIn.trigger()
    }
  }

  fun login(){
    viewModelScope.launch {
      activity.shouldShowLoading(tag = LOGIN_REQUEST)
      val result = safeApiCall(tag = LOGIN_REQUEST){
        remoteRepository.login(usernameData.value.toString(), passwordData.value.toString(), cabangData.value.toString())
      }
      activity.shouldHideLoading(tag = LOGIN_REQUEST)
      result.consume(
        onUnknownError = activity::onUnknownError,
        onTimeoutError = activity::onTimeoutError,
        onNetworkError = activity::onNetworkError,
        onHttpError = activity::onHttpError,
        onErrorResponse = activity::onErrorResponse,
        onSuccess = {
          successLogin.value = it.data
        }
      )
    }
  }
}