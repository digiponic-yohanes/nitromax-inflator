package com.digiponic.nitromaxinflator.ui.check_pressure

import androidx.lifecycle.Observer
import com.digiponic.nitromaxinflator.sqlite.NitromaxDb
import com.digiponic.nitromaxinflator.sqlite.data.Logtext
import com.digiponic.nitromaxinflator.MainApplication
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.ancestors.BaseViewModel
import com.digiponic.nitromaxinflator.extension.NonNullLiveData
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.response.BTOutputTekanan
import com.digiponic.nitromaxinflator.repository.network.request.RemoteRepository
import com.digiponic.nitromaxinflator.repository.preference.PreferenceRepository
import com.digiponic.nitromaxinflator.sqlite.DatabaseHelper
import com.google.gson.Gson
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class CheckPressureVM(activity: BaseActivity): BaseViewModel(activity) {

  @Inject
  lateinit var preferenceRepository: PreferenceRepository

  @Inject
  lateinit var remoteRepository: RemoteRepository

  val bluetoothLoadingModeTransaction = NonNullLiveData(false)
  val bluetoothStatusModeTransaction = NonNullLiveData(false)
  val bluetoothOutputResponse = SingleEventWrapper<BTOutputTekanan>()

  val database = DatabaseHelper(activity)

  init {
    (activity.application as MainApplication).consumer.inject(this)
    app.bluetooth.onMessage.observe(activity) { message ->
      bluetoothLoadingModeTransaction.value = false
      message?.let {
        val stringMessage = String(it)
        try {
          val jsonObject = JSONObject(stringMessage)
          if ((jsonObject.has("mode") && jsonObject.getString("mode") == "input")){
            if(jsonObject.has("status")){
              bluetoothStatusModeTransaction.value = jsonObject.getString("status") == "ok"
            }
            database.writeLogToFile(stringMessage, "Response")
          } else if(jsonObject.has("tekanan")){
            bluetoothOutputResponse.value = Gson().fromJson(jsonObject.toString(), BTOutputTekanan::class.java)
          }
        } catch (e: Exception){
          e.printStackTrace()
        }
      }
    }
  }
}