package com.digiponic.nitromaxinflator.ui.check_pressure

import android.os.Bundle
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.extension.instantiateViewModel
import com.digiponic.nitromaxinflator.external_libraries.bluetooth.Bluetooth
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTICommand
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTIRequestCheckTekanan
import com.google.gson.Gson
import com.mcnmr.utilities.extension.getCompatColor
import kotlinx.android.synthetic.main.activity_check_pressure.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.toast

class CheckPressureActivity : BaseActivity() {

  private val vm by lazy { instantiateViewModel<CheckPressureVM>() }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_check_pressure)

    cabangNama.text = vm.preferenceRepository.getUser()!!.namaCabang

    app.bluetooth.onBluetoothChangeStatus.observe(this) {
      when(it){
        Bluetooth.BluetoothStatus.CONNECTING -> vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_connecting))
        Bluetooth.BluetoothStatus.CONNECTED -> vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_connected))
        Bluetooth.BluetoothStatus.DISCONNECTED -> vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_disconnected))
      }
    }

    vm.bluetoothStatusModeTransaction.observe(this) {
      btnCheckTekanan.isEnabled = it.not()
      btnCheckTekanan.setBackgroundColor(if (btnCheckTekanan.isEnabled) getCompatColor(R.color.colorPrimary) else getCompatColor(R.color.graydddd))
    }

    btnCheckTekanan.setOnClickListener {
      if(app.bluetooth.isConnected){
        vm.bluetoothLoadingModeTransaction.value = true
        vm.database.writeLogToFile(Gson().toJson(BTIRequestCheckTekanan()), "Input")
        app.bluetooth.send(Gson().toJson(BTIRequestCheckTekanan()), charset("utf-8"))
        btnCheckTekanan.isEnabled = false
        btnCheckTekanan.setBackgroundColor(if (btnCheckTekanan.isEnabled) getCompatColor(R.color.colorPrimary) else getCompatColor(R.color.graydddd))
      } else {
        toast("Bluetooth tidak terhubung")
      }
    }

    vm.bluetoothOutputResponse.observe(this) {
      tvTekananValue.text = it.tekanan.toString()
    }
  }

  override fun onUserLeaveHint() {
    super.onUserLeaveHint()
    app.saveAppHistory("Keluar dari aplikasi dengan memencet tombol Home")
  }

  override fun onBackPressed() {
    alertDialog(
      title = "Kembali",
      message = "Apakah anda yakin ingin keluar?",
      positiveTitle = "Ya",
      negativeTitle = "Tidak",
      positiveAction = {
        if(app.bluetooth.isConnected){
          vm.database.writeLogToFile(Gson().toJson(BTICommand(value = "cancel")), "Input")
          app.bluetooth.send(Gson().toJson(BTICommand(value = "cancel")), charset("utf-8"))
          finish()
        }else {
          toast("Bluetooth tidak terhubung")
        }
      }
    )
  }
}