package com.digiponic.nitromaxinflator.ui.dashboard.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.extension.NonNullLiveData
import com.digiponic.nitromaxinflator.repository.network.response.ItemResponse
import com.mcnmr.utilities.extension.circularHide
import com.mcnmr.utilities.extension.circularReveal
import com.mcnmr.utilities.extension.doWhenInvisible
import com.mcnmr.utilities.extension.doWhenVisible
import kotlinx.android.synthetic.main.row_item.view.*

class ItemAdapter(private val activity: AppCompatActivity,
                  private var items: List<ItemResponse.Item>,
                  private val selectedItemLiveData: MutableLiveData<ItemResponse.Item>,
                  private val quantityItemLiveData: NonNullLiveData<LinkedHashMap<ItemResponse.Item, Int>>,
                  private val onCheckTireSelected: (ItemResponse.Item) -> Unit,
                  private val onTireRepairSelected: (ItemResponse.Item) -> Unit,
                  private val onCheckPressureSelected: (ItemResponse.Item) -> Unit) : RecyclerView.Adapter<ItemAdapter.ItemVH>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemVH =
    ItemVH(LayoutInflater.from(activity).inflate(R.layout.row_item, parent, false))

  override fun getItemCount(): Int = items.size
  override fun getItemViewType(position: Int): Int = position

  override fun onBindViewHolder(holder: ItemVH, position: Int) {
    selectedItemLiveData.observe(activity) {
      if (position < itemCount) {
        if (items[position] == it) {
          holder.clQuantityContainer.doWhenInvisible { view -> view.circularReveal() }
        } else {
          holder.clQuantityContainer.doWhenVisible { view -> view.circularHide() }
        }
      }
    }

    quantityItemLiveData.observe(activity) {
      if (position < itemCount) {
        if (items[position].qty == 0) {
          holder.llPlus.visibility = View.INVISIBLE
          holder.llMinus.visibility = View.INVISIBLE
          holder.tvQuantity.text = "Stock Habis"
        } else {
          val quantity = if (it.containsKey(items[position])) it[items[position]] else 0
          holder.llPlus.visibility = if (quantity == items[position].qty) View.INVISIBLE else View.VISIBLE
          holder.llMinus.visibility = if (quantity == 0) View.INVISIBLE else View.VISIBLE
          holder.tvQuantity.text = quantity.toString()
        }
      }
    }

    Glide.with(activity).load(items[position].gambar).into(holder.ivItemPic)
    holder.tvItemName.text = items[position].keterangan
    holder.tvItemNameQuantity.text = items[position].keterangan

    holder.llPlus.setOnClickListener {
      quantityItemLiveData.value[items[position]] = (quantityItemLiveData.value[items[position]] ?: 0) + 1
      quantityItemLiveData.value = quantityItemLiveData.value
    }

    holder.llMinus.setOnClickListener {
      val currentQuantity = quantityItemLiveData.value[items[position]] ?: 0

      if(currentQuantity <= 1){
        quantityItemLiveData.value.remove(items[position])
      } else {
        quantityItemLiveData.value[items[position]] = currentQuantity - 1
      }
      quantityItemLiveData.value = quantityItemLiveData.value
    }

    holder.clItemContainer.setOnClickListener {
      when (items[position].id) {
        in 5..6 -> onCheckTireSelected(items[position])
        in 7..8 -> onTireRepairSelected(items[position])
        9 -> onCheckPressureSelected(items[position])
        in 11..12 -> onTireRepairSelected(items[position])
        else -> selectedItemLiveData.value = items[position]
      }
    }

    holder.clQuantityContainer.setOnClickListener { selectedItemLiveData.value = null }
    holder.isJasaMotor.visibility = if (items[position].keterangan.contains("Motor")) View.VISIBLE else View.GONE
  }

  fun setNewItem(items: List<ItemResponse.Item>){
    this.items = items
    notifyDataSetChanged()
  }

  inner class ItemVH(v: View): RecyclerView.ViewHolder(v){
    val clItemContainer: ConstraintLayout = v.clItemContainer
    val clQuantityContainer: ConstraintLayout = v.clQuantityContainer
    val llPlus: LinearLayout = v.llPlus
    val llMinus: LinearLayout = v.llMinus
    val ivItemPic: AppCompatImageView = v.ivItemPic
    val tvItemName: TextView = v.tvNamaItem
    val tvQuantity: TextView = v.tvQuantity
    val tvItemNameQuantity: TextView = v.tvItemNameQuantity
    val isJasaMotor: ImageView = v.isJasaMotor
  }
}

