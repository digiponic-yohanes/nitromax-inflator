package com.digiponic.nitromaxinflator.ui.dashboard

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.extension.instantiateViewModel
import com.digiponic.nitromaxinflator.external_libraries.bluetooth.Bluetooth
import com.digiponic.nitromaxinflator.model.Menu
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTICommand
import com.digiponic.nitromaxinflator.repository.network.response.ItemResponse
import com.digiponic.nitromaxinflator.ui.absensi.AbsensiActivity
import com.digiponic.nitromaxinflator.ui.check_pressure.CheckPressureActivity
import com.digiponic.nitromaxinflator.ui.check_tire.CheckTireActivity
import com.digiponic.nitromaxinflator.ui.checkout_success.CheckoutSuccessActivity
import com.digiponic.nitromaxinflator.ui.dashboard.adapter.DashboardCartAdapter
import com.digiponic.nitromaxinflator.ui.dashboard.adapter.ItemAdapter
import com.digiponic.nitromaxinflator.ui.dashboard.adapter.MenuAdapter
import com.digiponic.nitromaxinflator.ui.dashboard.adapter.TransEmergencyAdapter
import com.digiponic.nitromaxinflator.ui.logtext.LogActivity
import com.digiponic.nitromaxinflator.ui.payment.PaymentActivity
import com.digiponic.nitromaxinflator.ui.splashscreen.SplashScreenActivity
import com.digiponic.nitromaxinflator.ui.transaksi.TransaksiActivity
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.google.gson.Gson
import com.jakewharton.rxbinding4.widget.textChanges
import com.mcnmr.utilities.extension.getCompatColor
import com.mcnmr.utilities.extension.visibleOrGoneIf
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.layout_list_transaksi_recovery.view.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.startActivityForResult
import org.jetbrains.anko.toast

class DashboardActivity : BaseActivity() {

  companion object {
    const val CHECK_BAN_BOCOR = 0x01
  }

  private val vm by lazy { instantiateViewModel<DashboardVM>() }
  private val cartObserver by lazy { Observer<LinkedHashMap<ItemResponse.Item, Int>> {
    var total = 0
    for((item, quantity) in it) total += item.hargaJual.toInt() * quantity

    tvTotalValue.text = "Rp $total"
    (rvCart.adapter as DashboardCartAdapter).setNewItem(it)
  } }

  private lateinit var progress: ProgressDialog

  private var currentItemList: List<ItemResponse.Item> = listOf()

  private lateinit var kurasMotor: ItemResponse.Item
  private lateinit var kurasMobil: ItemResponse.Item
  private lateinit var kurasMobilBesar: ItemResponse.Item
  private lateinit var isiMotor: ItemResponse.Item
  private lateinit var isiMobil: ItemResponse.Item
  private lateinit var tambalMotor: ItemResponse.Item
  private lateinit var tambalMobil: ItemResponse.Item
  private lateinit var tiptopMotor: ItemResponse.Item
  private lateinit var tiptopMobil: ItemResponse.Item

  private var isLogout = false

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_dashboard)

    app.connectBluetooth(10000)
    progress = ProgressDialog(this)

    cabangNama.text = vm.preferenceRepository.getUser()!!.namaCabang

    rvMenu.layoutManager = LinearLayoutManager(this)
    rvItem.layoutManager = FlexboxLayoutManager(this).apply {
        justifyContent = JustifyContent.SPACE_EVENLY
    }
    rvCart.layoutManager = LinearLayoutManager(this)

    rvMenu.adapter = MenuAdapter(this) { menu ->
      when(menu){
        is Menu.Semua -> vm.getAllItem()
        is Menu.Produk -> vm.getItemByCategory("55")
        is Menu.Jasa -> vm.getItemByCategory("56")
        is Menu.Absensi -> vm.getAbsensiKaryawan()
        is Menu.ConnectBluetoothMikro -> app.bluetooth.connectToName(app.getBluetoothName())
        is Menu.ConnectBluetoothPrinter -> vm.testPrint()
        is Menu.Recovery -> doRecoveryTransaction()
        is Menu.Logtext -> startActivity<LogActivity>()
        is Menu.Logout -> if (rvCart.adapter!!.itemCount > 0) {
          Toast.makeText(this, "Anda tidak dapat keluar dari aplikasi karena masih ada transksi yang belum di-checkout", Toast.LENGTH_SHORT).show()
        } else {
          alertDialog(
            "Konfirmasi Logout",
            "Apakah Anda yakin ingin logout? Anda akan otomatis melakukan absensi check out jika logout",
            "Tidak", {},
            "Ya", {
              isLogout = true
              doRecoveryTransaction()
              //vm.get_group_rekap_nitrogen()
            }
          )
        }
      }
    }

    rvItem.adapter = ItemAdapter(
      activity = this,
      items = listOf(),
      selectedItemLiveData = vm.selectedItem,
      quantityItemLiveData = vm.quantityItem,
      onCheckTireSelected = {
        startActivityForResult<CheckTireActivity>(CHECK_BAN_BOCOR,
            CheckTireActivity.SERVICE_ARGUMENT to it,
            CheckTireActivity.TAMBAL_ARGUMENT to if (it.id == 5) tambalMotor else tambalMobil,
            CheckTireActivity.TAMBAH_ARGUMENT to if (it.id == 5) isiMotor else isiMobil)
      },
      onTireRepairSelected =  {
        val cart = java.util.LinkedHashMap<ItemResponse.Item, Int>()
        when (it.id) {
          7 -> cart[tambalMotor] = 1
          8 -> cart[tambalMobil] = 1
          11 -> cart[tiptopMotor] = 1
          12 -> cart[tiptopMobil] = 1
        }
        startActivityForResult<PaymentActivity>(0, PaymentActivity.CART_ARGUMENT to cart, PaymentActivity.ISI_BARU_ARGUMENT to (if (it.id == 7 || it.id == 11) kurasMotor else kurasMobil))
      },
      onCheckPressureSelected = {
        startActivity<CheckPressureActivity>()
      }
    )

    rvCart.adapter = DashboardCartAdapter(
      context = this,
      carts = linkedMapOf(),
      onDelete = { item ->
        vm.quantityItem.value.remove(item)
        vm.quantityItem.value = vm.quantityItem.value
      }
    )

    app.bluetooth.onBluetoothChangeStatus.observe(this) {
      when(it){
        Bluetooth.BluetoothStatus.CONNECTING -> vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_connecting))
        Bluetooth.BluetoothStatus.CONNECTED -> vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_connected))
        Bluetooth.BluetoothStatus.DISCONNECTED -> vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_disconnected))
      }
    }

    vm.successRecovery.observe(this) {
      if (progress.isShowing) progress.dismiss()
      if (isLogout) {
        showProgressRekap()
        vm.getGroupRekapNitrogen()
      }
    }

    vm.listTransaksiRecovered.observe(this) {
      val mDialogView = LayoutInflater.from(this).inflate(R.layout.layout_list_transaksi_recovery, null)
      mDialogView.rvListTransRecovery.adapter = TransEmergencyAdapter(this, it)
      mDialogView.rvListTransRecovery.layoutManager = LinearLayoutManager(applicationContext)
      val alertDialog = AlertDialog.Builder(this).setTitle("${it.size} Transaksi yang Bisa Di-Recovery")
        .setView(mDialogView)
        .setCancelable(false)
        .setPositiveButton("Recovery Sekarang") { _, _ ->
          vm.saveRecoveryTransaction(it)
        }
        .create()

      alertDialog.show()
    }

    vm.successPrintRekap.observe(this) {
      if (progress.isShowing) progress.dismiss()
      vm.logout()
    }

    vm.onPrinterNotConnected.observe(this) {
      toast("Printer tidak terhubung")
    }

    vm.errorBrokenPipe.observe(this) {
      toast(it)
    }

    vm.isLoadingItem.observe(this) {
      shimmerItem.visibleOrGoneIf(it)
      rvItem.visibleOrGoneIf(!it)
    }

    vm.getAbsensiResponse.observe(this) {
      startActivity<AbsensiActivity>(AbsensiActivity.ABSENSI_ARGUMENT to it)
    }

    vm.getItemResponse.observe(this) {
      vm.quantityItem.observe(this, cartObserver)
      (rvItem.adapter as ItemAdapter).setNewItem(it.data)
      currentItemList = it.data

      for (items in it.data) {
        when (items.id) {
          1 -> kurasMotor = items
          2 -> kurasMobil = items
          3 -> isiMotor = items
          4 -> isiMobil = items
          7 -> tambalMotor = items
          8 -> tambalMobil = items
          10 -> kurasMobilBesar = items
          11 -> tiptopMotor = items
          12 -> tiptopMobil = items
        }
      }
    }

    vm.userLogout.observe(this) {
      vm.preferenceRepository.removeUser()
      startActivity<SplashScreenActivity>()
      setResult(Activity.RESULT_CANCELED)
      finishAffinity()
    }

    vm.decodeResult.observe(this) {
      toast("Hasil Recovery : $it")
    }

    vm.normalTransactionSuccess.observe(this) {
      Handler(Looper.getMainLooper()).postDelayed({
        setResult(Activity.RESULT_OK)
        startActivity<CheckoutSuccessActivity>(CheckoutSuccessActivity.ARGS_HARGA to it)
      }, 2000)
    }

    searchItem.textChanges().subscribe{
      val filteredList = mutableListOf<ItemResponse.Item>()

      for (item in currentItemList) {
        if (item.keterangan.lowercase().contains(it.toString().lowercase())) filteredList.add(item)
      }
      (rvItem.adapter as ItemAdapter).setNewItem(filteredList)
    }

    btnTransaction.setOnClickListener{ startActivity<TransaksiActivity>() }

    btnCheckout.setOnClickListener {
      if(vm.quantityItem.value.isEmpty()){
        toast("Belum ada produk/jasa yang dipilih")
      } else {
        var uniqueService = -1
        var doubleUniqueService = false
        lateinit var isiBaruArgument: ItemResponse.Item
        vm.quantityItem.value.forEach {
          if (uniqueService == -1){
            if(it.key.id in 1..6) uniqueService = it.key.id
          } else {
            if(it.key.id in 1..6){
              if(it.key.id != uniqueService) doubleUniqueService = true
            }
          }
          isiBaruArgument = when (it.key.id) {
            1, 3 -> kurasMotor
            2, 4, 10 -> kurasMobil
            else -> kurasMobil
          }
        }

        if(doubleUniqueService){
          alertDialog(
            title = "Terjadi kesalahan",
            message = "Hanya bisa memilih 1 transaksi jasa mesin",
            negativeTitle = "Tutup"
          )
        } else {
          startActivityForResult<PaymentActivity>(0, PaymentActivity.CART_ARGUMENT to vm.quantityItem.value, PaymentActivity.ISI_BARU_ARGUMENT to isiBaruArgument)
        }
      }
    }
    vm.getAllItem()

    if (!vm.dbHelper.checkPendingTransaction()) {
      alertDialog("Peringatan", "Ada satu transaksi pending yang belum tersimpan. Harap menyimpan transaksi terlebih dahulu", null, null, "OK",
        {
          val payParams = vm.dbHelper.assignPendingTransaction()
          vm.savePendingTransaction(payParams)
        },
        null, null, false)
    }
  }

  private fun doRecoveryTransaction(){
    if (app.bluetooth.isConnected) {
      showProgressRecovery()
      vm.hasRecovery = true
      vm.dbHelper.writeLogToFile(Gson().toJson(BTICommand(value = "readBackup")), "Input")
      app.bluetooth.send(Gson().toJson(BTICommand(value = "readBackup")), charset("utf-8"))
    } else {
      toast("Bluetooth tidak terhubung")
    }
  }

  override fun onUserLeaveHint() {
    super.onUserLeaveHint()
    app.saveAppHistory("Keluar dari aplikasi dengan memencet tombol Home")
  }

  override fun onResume() {
    super.onResume()
    vm.onResume()
  }

  override fun onPause() {
    super.onPause()
    vm.onPause()
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    if (requestCode == 0 && resultCode == Activity.RESULT_OK){
      vm.selectedItem.value = null
      vm.quantityItem.value = linkedMapOf()
    } else if (requestCode == CHECK_BAN_BOCOR && resultCode == Activity.RESULT_OK) {
      if (data!!.hasExtra(CheckTireActivity.RESULT)) {
        val cartExtras = data.getSerializableExtra(CheckTireActivity.RESULT) as HashMap<ItemResponse.Item, Int>
        val cart = LinkedHashMap<ItemResponse.Item, Int>(cartExtras)
        vm.quantityItem.value = cart
        lateinit var isiBaruArgument: ItemResponse.Item
        vm.quantityItem.value.forEach { isiBaruArgument = if (it.key.id == 7) kurasMotor else kurasMobil }
        startActivityForResult<PaymentActivity>(0, PaymentActivity.CART_ARGUMENT to vm.quantityItem.value,PaymentActivity.ISI_BARU_ARGUMENT to isiBaruArgument)
      } else if (data.hasExtra(CheckTireActivity.IS_TIDAK_BOCOR)) {
        vm.isCekBocor.value = true
      }
    } else {
      super.onActivityResult(requestCode, resultCode, data)
    }
  }

  override fun onDestroy() {
    app.disconnectBluetooth()
    super.onDestroy()
  }

  override fun onBackPressed() {
    if (rvCart.adapter!!.itemCount > 0) {
      toast("Anda tidak dapat keluar dari aplikasi karena masih ada transksi yang belum di-checkout")
    } else {
      super.onBackPressed()
    }
  }

  private fun showProgressRekap(){
    progress.setTitle("Harap Tunggu")
    progress.setMessage("Mendapatkan rekapitulasi transaksi hari ini")
    progress.setCancelable(false) // disable dismiss by tapping outside of the dialog
    progress.show()
  }

  private fun showProgressRecovery(){
    progress.setTitle("Harap Tunggu")
    progress.setMessage("Proses recovery transaksi mode emergency sedang berjalan")
    progress.setCancelable(false) // disable dismiss by tapping outside of the dialog
    progress.show()
  }
}