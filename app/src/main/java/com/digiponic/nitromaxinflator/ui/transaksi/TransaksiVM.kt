package com.digiponic.nitromaxinflator.ui.transaksi

import android.os.Handler
import android.widget.Toast
import androidx.lifecycle.viewModelScope
import com.digiponic.nitromaxinflator.MainApplication
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.ancestors.BaseViewModel
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.EscPosPrinter
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.connection.bluetooth.BluetoothPrintersConnections
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.exceptions.EscPosConnectionException
import com.digiponic.nitromaxinflator.repository.network.request.RemoteRepository
import com.digiponic.nitromaxinflator.repository.network.response.CashKaryawanResponse
import com.digiponic.nitromaxinflator.repository.network.response.DetailTransaksiResponse
import com.digiponic.nitromaxinflator.repository.network.response.TransaksiResponse
import com.digiponic.nitromaxinflator.repository.preference.PreferenceRepository
import com.mcnmr.utilities.extension.isNotNull
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.launch
import java.text.NumberFormat
import javax.inject.Inject

class TransaksiVM(activity: BaseActivity): BaseViewModel(activity = activity){
  companion object {
    const val TRANSAKSI_REQUEST = "TRANSAKSI_REQUEST"
    const val CASH_REQUEST = "CASH_REQUEST"
    const val ENTER_PASSWORD = "ENTER_PASSWORD"
  }

  @Inject
  lateinit var remoteRepository: RemoteRepository

  @Inject
  lateinit var preferenceRepository: PreferenceRepository

  val dataTransaksiResponse = SingleEventWrapper<TransaksiResponse>()
  val dataCashResponse = SingleEventWrapper<CashKaryawanResponse>()
  val successOpenCash = SingleEventWrapper<Void>()
  val errorBrokenPipe = SingleEventWrapper<String>()

  init { (activity.application as MainApplication).consumer.inject(this) }

  fun loadCashKaryawan(){
    viewModelScope.launch {
      activity.shouldShowLoading(tag = CASH_REQUEST)
      val result = safeApiCall(tag = CASH_REQUEST) {
        remoteRepository.cashKaryawan(
          id_cabang = preferenceRepository.getUser()!!.idCabang,
          karyawan = preferenceRepository.getUser()!!.email
        )
      }
      activity.shouldHideLoading(CASH_REQUEST)
      result.consume(
        onUnknownError = activity::onUnknownError,
        onTimeoutError = {
          Handler().postDelayed(::loadAllTransaksi, 5000)
        },
        onNetworkError = { _, _ ->
          Handler().postDelayed(::loadAllTransaksi, 5000)
        },
        onHttpError = activity::onHttpError,
        onErrorResponse = activity::onErrorResponse,
        onSuccess = {
          dataCashResponse.value = it
        }
      )
    }
  }

  fun print(detailTransaksi: DetailTransaksiResponse.Transaksi){
    val firstPairedDevice = BluetoothPrintersConnections.getInstance().selectFirstPaired()
    if(firstPairedDevice == null){
      Toast.makeText(activity, "Printer not connected", Toast.LENGTH_SHORT).show()
    }else {
      val printer = EscPosPrinter(
        BluetoothPrintersConnections.getInstance().selectFirstPaired(),
        203,
        48f,
        32
      )

      var text: String = "[C]<u><font size='big'>${detailTransaksi.header_cabang}</font></u>\n" +
          "[C]${detailTransaksi.nama_cabang}\n" +
          "[L]\n" +
          "[L]Tanggal[R]${detailTransaksi.tanggal}\n" +
          "[L]Transaksi #[R]${detailTransaksi.kode}\n" +
          "[L]Metode Bayar[R]${detailTransaksi.metode_bayar}\n" +
          "[C]================================\n"

      for (item in detailTransaksi.detail) {
        when (item.id_item) {
          in 1..4 -> {
            text += "[L]<b>${item.nama}</b>\n"
            if (item.keterangan.isNotNull()) {
              val keterangan = item.keterangan.replace(", ", ",").split(",")
              for (ket in keterangan) {
                if (ket.isNotEmpty()) {
                  var harga = item.harga
                  val tekanans = ket.split("/")
                  if (tekanans[1].trim().toInt() < 17) {
                    text += "[L]<b>${tekanans[0].toInt()} -> ${tekanans[1].replace(",","").toInt()} (cancel)</b>[R]0\n"
                  } else {
                    if (tekanans[0].trim().toInt() >= tekanans[1].trim().toInt()) {
                      harga = (harga.toDouble() * item.gagal).toInt()
                    }
                    text += "[L]<b>${tekanans[0].toInt()} -> ${tekanans[1].trim().toInt()}</b>[R]${NumberFormat.getInstance().format(harga)}\n"
                  }
                }
              }
            }
          }
          in 7..8 -> {
            text += "[L]<b>${item.nama}</b>\n"
            text += "[L]<b>Jumlah lb/krt : ${item.qty}</b>[R]${NumberFormat.getInstance().format(item.subtotal)}\n"
          }
          else -> {
            text += "[L]<b>${item.nama}</b>[R]${NumberFormat.getInstance().format(item.subtotal)}\n"
          }
        }
      }
      text += "[C]--------------------------------\n" +
          "[R]TOTAL PRICE : ${NumberFormat.getInstance().format(detailTransaksi.subtotal)}\n" +
          "[C]================================\n"

      text += "[L]" + detailTransaksi.terms + "\n"

      text += "[L]\n" +
          "[C]by : Nitromax - DIGIPONIC\n"+
          "[L]\n"

      try {
        printer.printFormattedText(text)
      } catch (e: EscPosConnectionException) {
        if (e.message!!.contains("Broken pipe")) {
          errorBrokenPipe.value = "Koneksi ke printer terputus"
        } else {
          Toast.makeText(activity, e.message, Toast.LENGTH_SHORT).show()
        }
      }
    }
  }

  fun enterCredentials(password: String){
    preferenceRepository.getUser()?.let {
      viewModelScope.launch {
        activity.shouldShowLoading(ENTER_PASSWORD)
        val result = safeApiCall(ENTER_PASSWORD){
          remoteRepository.login(it.email, password, it.idCabang)
        }
        activity.shouldHideLoading(ENTER_PASSWORD)
        result.consume(
          onUnknownError = activity::onUnknownError,
          onTimeoutError = activity::onTimeoutError,
          onNetworkError = activity::onNetworkError,
          onHttpError = activity::onHttpError,
          onErrorResponse = activity::onErrorResponse,
          onSuccess = {
            successOpenCash.trigger()
          }
        )
      }
    }
  }

  fun loadAllTransaksi(){
    viewModelScope.launch {
      activity.shouldShowLoading(tag = TRANSAKSI_REQUEST)
      val result = safeApiCall(tag = TRANSAKSI_REQUEST){
        remoteRepository.getTransaksi(
          id = preferenceRepository.getUser()!!.idCabang,
          karyawan = preferenceRepository.getUser()!!.id
        )
      }
      activity.shouldHideLoading(tag = TRANSAKSI_REQUEST)
      result.consume(
        onUnknownError = activity::onUnknownError,
        onTimeoutError = {
          Handler().postDelayed(::loadAllTransaksi, 5000)
        },
        onNetworkError = { _, _ ->
          Handler().postDelayed(::loadAllTransaksi, 5000)
        },
        onHttpError = activity::onHttpError,
        onErrorResponse = activity::onErrorResponse,
        onSuccess = {
          dataTransaksiResponse.value = it
        }
      )
    }
  }
}