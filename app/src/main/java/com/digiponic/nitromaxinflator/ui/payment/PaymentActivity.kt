package com.digiponic.nitromaxinflator.ui.payment

import android.app.Activity
import android.app.AlertDialog
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.extension.instantiateViewModel
import com.digiponic.nitromaxinflator.external_libraries.bluetooth.Bluetooth
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTICommand
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTIHole
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTIRequestService
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTIUpdatePressure
import com.digiponic.nitromaxinflator.repository.network.response.ItemResponse
import com.digiponic.nitromaxinflator.ui.checkout_success.CheckoutSuccessActivity
import com.digiponic.nitromaxinflator.ui.payment.adapter.PaymentCartAdapter
import com.digiponic.nitromaxinflator.ui.payment.adapter.PilihPressureAdapter
import com.google.gson.Gson
import com.jakewharton.rxbinding4.widget.textChanges
import com.mcnmr.utilities.extension.*
import com.mcnmr.utilities.internal_plugin.SerializableIntent
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.indikator_nitrogen.*
import kotlinx.android.synthetic.main.toolbar_with_reconnect_layout.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import kotlin.collections.HashMap

class PaymentActivity : BaseActivity() {
  companion object {
    const val CART_ARGUMENT = "CART_ARGUMENT"
    const val ISI_BARU_ARGUMENT = "ISI_BARU_ARGUMENT"

    const val CASH = 29
    const val CASHLESS = 86
  }

  private val idJasaMotor = intArrayOf(1, 3, 7, 11)
  private val idJasaMobil = intArrayOf(2, 4, 8, 10, 12)

  private val pilihanPressureMotor = intArrayOf(29, 30, 32, 33, 35, 36, 38, 40)
  private val pilihanPressureMobil = intArrayOf(30, 32, 33, 34, 35, 36, 38, 40, 42, 45, 48, 50)

  private var isBack: Boolean = true

  private val vm by lazy { instantiateViewModel<PaymentVM>() }

  @SerializableIntent(CART_ARGUMENT)
  lateinit var cart: HashMap<ItemResponse.Item, Int>

  @SerializableIntent(ISI_BARU_ARGUMENT)
  lateinit var isiBaru: ItemResponse.Item

  private var btRequestService: BTIRequestService? = null

  private lateinit var animationDrawable: AnimationDrawable

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_payment)

    animationDrawable = arrowIndicator.background as AnimationDrawable

    cabangNama.text = vm.preferenceRepository.getUser()!!.namaCabang
    if (isiBaru.isNotNull()) vm.isiBaruItem.value = isiBaru

    for((item, _) in cart) {
      if (item.id in 1..12) {
        if (item.id in idJasaMotor) {
          ivCarMotor.setImageResource(R.drawable.motorcycle_big)
          rvPressures.adapter = PilihPressureAdapter(this, pilihanPressureMotor, vm.bluetoothLoadingModeTransaction, vm.bluetoothStatusModeTransaction, {
            prosesChooseTekanan(it)
          }, {
            updateChooseTekanan(it)
          })
          rvPressures.layoutManager = GridLayoutManager(this, 4)
        } else if (item.id in idJasaMobil) {
          ivCarMotor.setImageResource(R.drawable.car_big)
          rvPressures.adapter = PilihPressureAdapter(this, pilihanPressureMobil, vm.bluetoothLoadingModeTransaction, vm.bluetoothStatusModeTransaction, {
            prosesChooseTekanan(it)
          }, {
            updateChooseTekanan(it)
          })
          rvPressures.layoutManager = GridLayoutManager(this, 6)
        }
      }
    }

    vm.metodePembayaranData.observe(this) {
      btnCash.background = getCompatDrawable(if (it == CASH) R.drawable.btn_blue_bg else R.drawable.btn_bordered_blue_bg)
      btnCash.setTextColor(getCompatColor(if (it == CASH) android.R.color.white else R.color.colorPrimary))

      btnCashless.background = getCompatDrawable(if (it == CASHLESS) R.drawable.btn_blue_bg else R.drawable.btn_bordered_blue_bg)
      btnCashless.setTextColor(getCompatColor(if (it == CASHLESS) android.R.color.white else R.color.colorPrimary))
    }

    btnCash.setOnClickListener { vm.metodePembayaranData.value = CASH }
    btnCashless.setOnClickListener { vm.metodePembayaranData.value = CASHLESS }

    vm.isAlreadyCabut.observe(this) {
      if (it) {
        if (vm.transactionTypeData.value == TransactionType.TIRE_REPAIR_TRANSACTION) {
          vDivider3.gone()
          tvPilihPressure.gone()
          rvPressures.gone()
          etTekanan.gone()
          btnProses.gone()
          btnForceContinue.gone()
          etHole.isEnabled = true
          btnForceFinish.isEnabled = true
          btnForceFinish.setBackgroundColor(if(btnForceFinish.isEnabled) getCompatColor(R.color.colorPrimary) else getCompatColor(R.color.graydddd))
        }
      }
    }

    vm.transactionTypeData.observe(this){
      when(it){
        TransactionType.TIRE_TRANSACTION -> {
          btnPayAndPrint.gone()
          etTekanan.visible()
          btnProses.visible()
          btnForceContinue.visible()
          btnForceFinish.visible()
          clIndikatorNitrogen.visible()
          etHole.gone()
          tvPilihPressure.visible()
          rvPressures.visible()
        }
        TransactionType.TIRE_REPAIR_TRANSACTION -> {
          btnPayAndPrint.gone()
          etTekanan.visible()
          btnProses.visible()
          btnForceContinue.visible()
          clIndikatorNitrogen.visible()
          tvPilihPressure.visible()
          rvPressures.visible()
          etHole.isEnabled = false
          btnForceFinish.isEnabled = false
          btnForceFinish.setBackgroundColor(if(btnForceFinish.isEnabled) getCompatColor(R.color.colorPrimary) else getCompatColor(R.color.graydddd))
        } else -> {
          etHole.gone()
          btnPayAndPrint.visible()
          clIndikatorNitrogen.gone()
          tvPilihPressure.gone()
          rvPressures.gone()
        }
      }
    }

    app.bluetooth.onBluetoothChangeStatus.observe(this) {
      when(it){
        Bluetooth.BluetoothStatus.CONNECTING -> vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_connecting))
        Bluetooth.BluetoothStatus.CONNECTED -> vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_connected))
        Bluetooth.BluetoothStatus.DISCONNECTED -> vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_disconnected))
      }
    }

    vm.currentPressureRequestData.observe(this) {
      tvTekananValue.text = it.toString()
    }

    vm.transactionCodeData.observe(this) {
      if(vm.transactionTypeData.value != TransactionType.REGULAR_TRANSACTION){
        btnForceFinish.visibleOrGoneIf(it == "")
      } else {
        btnPayAndPrint.visibleOrGoneIf(it == "")
      }
      btnPrintAndClose.visibleOrGoneIf(it != "")
    }

    vm.bluetoothLoadingModeTransaction.observe(this) {
      toggleEnableDisableButton(btnProses, it.not())
    }

    vm.btnAnginTidakKeluarPressed.observe(this) {
      toggleEnableDisableButton(btnForceContinue, it.not())
    }

    vm.bluetoothPressureResponse.observe(this) {
      val tekananDisplay = it.tekanan
      tvTekananCurValue.text = tekananDisplay.toString()
      vm.currentPressureRequestData.value = it.ref
    }

    vm.isStartProcessFilling.observe(this) {
      if (it) {
        arrowIndicator.visible()
        clIndikatorCurrent.visible()
      } else {
        arrowIndicator.gone()
        clIndikatorCurrent.gone()
      }
    }

    vm.isAlreadyCabut.observe(this) {
      tvTekananCurValue.text = "0"
    }

    vm.bluetoothStatusModeTransaction.observe(this) {
      if (it){
        isBack = false
        btnProses.text = getString(R.string.btnUpdateTekanan)
        btnProses.setOnClickListener {
          if(app.bluetooth.isConnected){
            if (etTekanan.text.toString().isEmpty()) {
              alertDialog("Kesalahan", "Tekanan harus diisi")
            } else {
              if (vm.pressureData.value in 17..99) {
                updateChooseTekanan(vm.pressureData.value)
                etTekanan.setText("")
              } else {
                alertDialog("Kesalahan", "Nilai tekanan yang diizinkan berkisar antara 17-99 Psi")
              }
            }
          } else {
            toast("Bluetooth tidak terhubung")
          }
        }
      } else {
        btnProses.text = getString(R.string.btnProses)
        btnProses.setOnClickListener {
          if(app.bluetooth.isConnected){
            if (etTekanan.text.toString().isEmpty()) {
              alertDialog("Kesalahan", "Tekanan harus diisi")
            } else {
              if (vm.pressureData.value in 17..99) {
                prosesChooseTekanan(vm.pressureData.value)
                etTekanan.setText("")
              } else {
                alertDialog("Kesalahan", "Nilai tekanan yang diizinkan berkisar antara 17-99 Psi")
              }
            }
          } else {
            toast("Bluetooth tidak terhubung")
          }
        }
      }
    }

    vm.bluetoothOutputResponse.observe(this) {
      if (vm.transactionTypeData.value == TransactionType.TIRE_REPAIR_TRANSACTION) {
        var cartTambalBan: ItemResponse.Item? = null
        vm.holeData.value = it.detail.jumlahLubang
        cart.forEach { item ->
          if (item.key.id in 7..8 || item.key.id in 11..12) {
            cartTambalBan = item.key
          }
        }
        cartTambalBan?.let { item ->
          cart[item] = vm.holeData.value
        }
      }
      vm.payTransaction(items = cart)
    }

    vm.bluetoothCancelTransaction.observe(this) {
      vm.payTransaction(items = cart)
    }

    vm.onPrinterNotConnected.observe(this) {
      toast("Printer tidak terhubung, pastikan printer telah terhubung dengan bluetooth, " +
          "kemudian tekan \"Print dan Keluar\"")
    }

    vm.errorBrokenPipe.observe(this) {
      toast(it)
    }

    vm.checkoutSuccess.observe(this) {
      Handler(Looper.getMainLooper()).postDelayed({
        setResult(Activity.RESULT_OK)
        finish()
        startActivity<CheckoutSuccessActivity>(CheckoutSuccessActivity.ARGS_HARGA to it)
      }, 2000)
    }

    rvCart.layoutManager = LinearLayoutManager(this)
    rvCart.adapter = PaymentCartAdapter(
      context = this,
      carts = cart
    )

    vm.isLoadingSentTransaction.observe(this) {
      toggleEnableDisableButton(btnPayAndPrint, it.not())
    }

    var total = 0
    for((item, quantity) in cart){
      total += item.hargaJual.toInt() * quantity
    }
    tvTotalValue.text = total.toString()

    etTekanan.textChanges().subscribe {
      vm.pressureData.value = try {
        it.toString().toInt()
      } catch (e: java.lang.NumberFormatException) {
        0
      }
    }

    etHole.textChanges().subscribe {
      vm.holeData.value = try {
        it.toString().toInt()
      } catch (e: java.lang.NumberFormatException) {
        0
      }
    }

    btnForceContinue.setOnClickListener {
      if(app.bluetooth.isConnected){
        vm.dbHelper.writeLogToFile(Gson().toJson(BTICommand(value = "open")),"Input")
        app.bluetooth.send(Gson().toJson(BTICommand(value = "open")), charset("utf-8"))
        vm.btnAnginTidakKeluarPressed.value = true
      } else {
        toast("Bluetooth tidak terhubung")
      }
    }

    btnForceFinish.setOnClickListener {
      //if contains tire repair transaction change the quantity with tire hole
      val alertDialogBuilder = AlertDialog.Builder(this)
      alertDialogBuilder.setTitle("Apakah Anda yakin?")
      alertDialogBuilder.setMessage("Transaksi akan selesai dan Anda tidak akan dapat mengembalikan data Anda!")
      alertDialogBuilder.setPositiveButton("Ya") { _, _ ->
        // Check apakah ada JSON transaksi yang tersimpan di database lokal
        if (vm.dbHelper.checkPendingTransaction()) {
          if(vm.transactionTypeData.value == TransactionType.TIRE_REPAIR_TRANSACTION){
            if(vm.holeData.value <= 0){
              toast("Jumlah lubang belum diisi")
            } else if (vm.holeData.value in 1..9) {
              if(app.bluetooth.isConnected){
                vm.dbHelper.writeLogToFile(Gson().toJson(BTIHole(jumlah_lubang = vm.holeData.value)), "Input")
                app.bluetooth.send(Gson().toJson(BTIHole(jumlah_lubang = vm.holeData.value)), charset("utf-8"))
              }else {
                toast("Bluetooth tidak terhubung")
              }
            } else {
              alertDialog("Kesalahan", "Jumlah lubang/karet yang diperbolehkan hanya antara 1-9")
            }
          } else {
            if(app.bluetooth.isConnected){
              vm.dbHelper.writeLogToFile(Gson().toJson(BTICommand(value = "cancel")), "Input")
              app.bluetooth.send(Gson().toJson(BTICommand(value = "cancel")), charset("utf-8"))
            } else {
              toast("Bluetooth tidak terhubung")
            }
          }
        } else {
          val payParams = vm.dbHelper.assignPendingTransaction()
          vm.savePendingTransaction(payParams)
        }
      }
      alertDialogBuilder.setNegativeButton("Tidak") { _, _ ->

      }
      alertDialogBuilder.show()
    }

    btnPayAndPrint.setOnClickListener {
      // regular transaction
      isBack = false
      vm.payTransaction(items = cart)
    }

    tvReconnectMikro.setOnClickListener {
      try {
        app.bluetooth.connectToName(app.getBluetoothName())
      } catch (e: Exception) {
        toast(e.message.toString())
      }
    }

    vm.checkTransaction(cart)
  }

  private fun toggleEnableDisableButton(button: AppCompatButton, it: Boolean){
    button.isEnabled = it
    button.setBackgroundColor(if(it) getCompatColor(R.color.colorPrimary) else getCompatColor(R.color.graydddd))
  }

  private fun updateChooseTekanan(tekanan: Int) {
    if(app.bluetooth.isConnected){
      vm.bluetoothLoadingModeTransaction.value = true
      vm.pressureData.value = tekanan
      vm.currentPressureRequestData.value = tekanan
      vm.dbHelper.writeLogToFile(Gson().toJson(BTIUpdatePressure(tekanan = vm.pressureData.value)), "Input")
      app.bluetooth.send(Gson().toJson(BTIUpdatePressure(tekanan = vm.pressureData.value)), charset("utf-8"))
    } else {
      toast("Bluetooth tidak terhubung")
    }
  }

  private fun prosesChooseTekanan(tekanan: Int) {
    if(app.bluetooth.isConnected){
      vm.pressureData.value = tekanan
      vm.bluetoothLoadingModeTransaction.value = true
      cart.forEach { item ->
        if(item.key.id in 1..4 || item.key.id in 7..8 || item.key.id in 10..12){
          val modeTransaksi = if (item.key.id in 10..12) {
            when (item.key.id) {
              10 -> 2
              11 -> 7
              12 -> 8
              else -> item.key.id
            }
          } else {
            item.key.id
          }
          btRequestService = BTIRequestService(modeTransaksi = modeTransaksi, jumlahBan = item.value, tekanan = vm.pressureData.value)
        }
      }
      vm.currentPressureRequestData.value = vm.pressureData.value
      btRequestService?.let { request ->
        vm.dbHelper.writeLogToFile(Gson().toJson(request), "Input")
        app.bluetooth.send(Gson().toJson(request), charset("utf-8"))
      }
    } else {
      toast("Bluetooth tidak terhubung")
    }
  }

  override fun onWindowFocusChanged(hasFocus: Boolean) {
    super.onWindowFocusChanged(hasFocus)
    if (hasFocus) animationDrawable.start()
  }

  override fun onUserLeaveHint() {
    super.onUserLeaveHint()
    app.saveAppHistory("Keluar dari aplikasi dengan memencet tombol Home")
  }

  override fun onBackPressed() {
    if (isBack) {
      alertDialog(
        title = "Kembali",
        message = "Apakah anda yakin ingin membatalkan transaksi?",
        positiveTitle = "Ya",
        negativeTitle = "Tidak",
        positiveAction = {
          if(vm.transactionTypeData.value != TransactionType.REGULAR_TRANSACTION){
            if(app.bluetooth.isConnected){
              vm.dbHelper.writeLogToFile(Gson().toJson(BTICommand(value = "cancel")), "Input")
              app.bluetooth.send(Gson().toJson(BTICommand(value = "cancel")), charset("utf-8"))
              finish()
            }else {
              toast("Bluetooth tidak terhubung")
            }
          }else {
            finish()
          }
        }
      )
    }
  }
}