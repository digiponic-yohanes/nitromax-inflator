package com.digiponic.nitromaxinflator.ui.check_tire

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.viewModelScope
import com.digiponic.nitromaxinflator.MainApplication
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.ancestors.BaseViewModel
import com.digiponic.nitromaxinflator.extension.NonNullLiveData
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.exceptions.EscPosConnectionException
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTReply
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.response.BTOutputResponse
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.response.BTOutputTekanan
import com.digiponic.nitromaxinflator.repository.network.parameter.PayTransactionParams
import com.digiponic.nitromaxinflator.repository.network.request.RemoteRepository
import com.digiponic.nitromaxinflator.repository.network.response.ItemResponse
import com.digiponic.nitromaxinflator.repository.preference.PreferenceRepository
import com.digiponic.nitromaxinflator.sqlite.DatabaseHelper
import com.digiponic.nitromaxinflator.ui.payment.PaymentVM
import com.google.gson.Gson
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class CheckTireVM(activity: BaseActivity) : BaseViewModel(activity){

  @Inject
  lateinit var preferenceRepository: PreferenceRepository

  @Inject
  lateinit var remoteRepository: RemoteRepository

  val checkoutSuccess = SingleEventWrapper<Int>()

  val pressureData = NonNullLiveData(0)
  val transactionCodeData = NonNullLiveData("")

  val onPrinterNotConnected = SingleEventWrapper<Void>()

  val isAnswerBocor = NonNullLiveData(false)
  val currentPressureData = NonNullLiveData(0)
  val currentPressureRequestData = NonNullLiveData(0)
  val bluetoothLoadingModeTransaction = NonNullLiveData(false)
  val bluetoothStatusModeTransaction = NonNullLiveData(false)
  val bluetoothQuestionBocorStatus = NonNullLiveData(false)
  val bluetoothQuestionLanjutStatus = NonNullLiveData(false)
  val bluetoothOutputResponse = SingleEventWrapper<BTOutputResponse>()
  val bluetoothPressureResponse = SingleEventWrapper<BTOutputTekanan>()
  val btnAnginTidakKeluarPressed = NonNullLiveData(false)
  val onPengisianSelesai = NonNullLiveData(false)
  val transactionCancelled = NonNullLiveData(false)
  val errorBrokenPipe = SingleEventWrapper<String>()
  val isCekBocorMasuk = SingleEventWrapper<Void>()

  val isBanBocor = NonNullLiveData(false)
  val isLanjutTambal = NonNullLiveData(false)

  var jsonStrings = mutableListOf<String>()
  val isStartProcessFilling = NonNullLiveData(false)
  private val isGettingInitialPressure = NonNullLiveData(false)

  val databaseHelper = DatabaseHelper(activity)

  init {
    (activity.application as MainApplication).consumer.inject(this)
    app.bluetooth.onMessage.observe(activity) { message ->
      bluetoothLoadingModeTransaction.value = false
      message?.let {
        val stringMessage = String(it)
        Log.e("STRING_MESSAGE", stringMessage)
        jsonStrings.add(stringMessage)
        try {
          val jsonObject = JSONObject(stringMessage)
          if (jsonObject.has("note")) {
            when (jsonObject.getString("note")) {
              "transaction" -> {
                bluetoothStatusModeTransaction.value = jsonObject.has("status") && jsonObject.getString("status") == "ok"
                bluetoothLoadingModeTransaction.value = !(jsonObject.has("status") && jsonObject.getString("status") == "ok")
              }
              "open" -> btnAnginTidakKeluarPressed.value = !(jsonObject.has("status") && jsonObject.getString("status") == "ok")
              "tekanan" -> bluetoothStatusModeTransaction.value = jsonObject.has("status") && jsonObject.getString("status") == "ok"
              "askBocor" ->  bluetoothQuestionBocorStatus.value = jsonObject.has("status") && jsonObject.getString("status") == "ok"
              "askIsiSekarang" -> bluetoothQuestionLanjutStatus.value = jsonObject.has("status") && jsonObject.getString("status") == "ok"
              "cancel" -> transactionCancelled.value = jsonObject.has("status") && jsonObject.getString("status") == "ok"
            }
            databaseHelper.writeLogToFile(stringMessage, "Response")
          } else if (jsonObject.has("tekanan")) {
            if (!isStartProcessFilling.value) isStartProcessFilling.value = true
            if (!isGettingInitialPressure.value) {
              bluetoothPressureResponse.value = Gson().fromJson(jsonObject.toString(), BTOutputTekanan::class.java)
              isGettingInitialPressure.value = true
            }
          } else if (jsonObject.has("status") && jsonObject.getString("status") == "proses ban berhasil") {
            onPengisianSelesai.value = true
            isStartProcessFilling.value = false
            isGettingInitialPressure.value = false
            databaseHelper.writeLogToFile(stringMessage, "Response")
          } else if (jsonObject.has("mode")) {
            when (jsonObject.getString("mode")) {
              "output" -> if (jsonObject.has("status") && jsonObject.getString("status") == "ok") {
                bluetoothOutputResponse.value = Gson().fromJson(jsonObject.toString(), BTOutputResponse::class.java)
                Gson().fromJson(jsonObject.toString(), BTOutputResponse::class.java)
              }
              "info" -> if (jsonObject.has("kind") && jsonObject.getString("kind") == "askBocor") {
                onPengisianSelesai.value = true
              }
              "communicationTest" -> {
                databaseHelper.writeLogToFile(Gson().toJson(BTReply()), "Input")
                app.bluetooth.send(Gson().toJson(BTReply()), charset("utf-8"))
              }
            }
            databaseHelper.writeLogToFile(stringMessage, "Response")
          }
        }catch (e: Exception){
          e.printStackTrace()
        }
      }
    }
  }

  fun payTransaction(service: ItemResponse.Item) {
    preferenceRepository.getUser()?.let { session ->
      val response = bluetoothOutputResponse.value!!
      val keterangan = Gson().toJson(response, BTOutputResponse::class.java)
      val tanggal = bluetoothOutputResponse.value!!.tglTransaksi
      val detail = mutableListOf<PayTransactionParams.PayTransactionDetail>()
      val pressures = response.pressure(service.id, service.hargaJual.toInt(), session.skalaGagal)
      var totalIsiTambah = 0
      var totalIsiBaru = 0
      var totalGagalIsiBaru = 0
      var totalGagalIsiTambah = 0
      val keteranganTekananIsiBaru = mutableListOf<String>()
      val keteranganTekananIsiTambah = mutableListOf<String>()
      pressures.forEach { pressure ->
        val isTekananTerhitungIsiBaru = if (service.id == 3) {
          pressure.tekananAwal <= session.limit_pressure_inflation_motor && !pressure.isTts
        } else {
          pressure.tekananAwal <= session.limit_pressure_inflation_mobil && !pressure.isTts
        }

        if (isTekananTerhitungIsiBaru) {
          keteranganTekananIsiBaru.add(if(pressure.tekanan < 17) {
            "${pressure.tekananAwal}/${pressure.tekanan}/${currentPressureRequestData.value}/${getLastPressureBeforeCabut()}"
          } else {
            "${pressure.tekananAwal}/${pressure.tekanan}"
          })
          totalIsiBaru++
          totalGagalIsiBaru += if (pressure.isGagal) 1 else 0
        } else {
          keteranganTekananIsiTambah.add(if(pressure.tekanan < 17) {
            "${pressure.tekananAwal}/${pressure.tekanan}/${currentPressureRequestData.value}/${getLastPressureBeforeCabut()}"
          } else {
            "${pressure.tekananAwal}/${pressure.tekanan}"
          })
          totalIsiTambah++
          totalGagalIsiTambah += if (pressure.isGagal) 1 else 0
        }
      }
      if (totalIsiBaru > 0) {
        val isiBaruItem = if (service.id == 5 || service.id == 3) "1" else "2"
        detail.add(PayTransactionParams.PayTransactionDetail(idItem = isiBaruItem, qty = totalIsiBaru, gagal = totalGagalIsiBaru, keterangan = keteranganTekananIsiBaru.joinToString()))
      }
      if (totalIsiTambah > 0) {
        val isiTambahItem = if (service.id == 5 || service.id == 3) "3" else "4"
        detail.add(PayTransactionParams.PayTransactionDetail(idItem = isiTambahItem, qty = totalIsiTambah, gagal = totalGagalIsiTambah, keterangan = keteranganTekananIsiTambah.joinToString()))
      }

      preferenceRepository.getUser()?.let {
        val params = PayTransactionParams(
          idMode = 0,
          user = it.email,
          cabang = it.idCabang,
          jenisKendaraan = "0",
          bayar = service.hargaJual.toInt(),
          detail = detail,
          metodePembayaran = 29,
          json_string = keterangan,
          tanggal = tanggal
        )
        viewModelScope.launch {
          activity.shouldShowLoading(PaymentVM.POST_CHECKOUT)
          val result = safeApiCall(PaymentVM.POST_CHECKOUT) { remoteRepository.newCheckout(params) }
          activity.shouldHideLoading(PaymentVM.POST_CHECKOUT)
          result.consume(
            onUnknownError = activity::onUnknownError,
            onTimeoutError = activity::onTimeoutError,
            onNetworkError = { _, _ -> writePendingTransaction(params) },
            onHttpError = activity::onHttpError,
            onErrorResponse = activity::onErrorResponse,
            onSuccess = { transaksi ->
              app.printWithProgressBar(transaksi.data, activity, errorBrokenPipe, onPrinterNotConnected, checkoutSuccess)
            }
          )
        }
      }
    }
  }

  private fun writePendingTransaction(params: PayTransactionParams) {
    preferenceRepository.getUser()?.let {
      if (databaseHelper.writePendingTransaction(it.skalaGagal, params)) {
        Toast.makeText(activity, "Koneksi internet tidak stabil. Data disimpan sementara dalam database lokal", Toast.LENGTH_SHORT).show()
      }
    }
  }

  fun payTransactionTwo(service: ItemResponse.Item) {
    preferenceRepository.getUser()?.let { session ->
      val detail = mutableListOf<PayTransactionParams.PayTransactionDetail>()
      detail.add(
        PayTransactionParams.PayTransactionDetail(
          idItem = service.id.toString(),
          qty = 0,
          gagal = 0,
          keterangan = ""
        )
      )

      preferenceRepository.getUser()?.let {
        val params = PayTransactionParams(
          idMode = 0,
          user = session.email,
          cabang = session.idCabang,
          jenisKendaraan = "0",
          bayar = 0,
          detail = detail,
          metodePembayaran = 29,
          tanggal = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Date()),
        )
        viewModelScope.launch {
          try {
            activity.shouldShowLoading(PaymentVM.POST_CHECKOUT)
            val result = safeApiCall(PaymentVM.POST_CHECKOUT) { remoteRepository.newCheckout(params) }
            activity.shouldHideLoading(PaymentVM.POST_CHECKOUT)
            result.consume(
              onUnknownError = activity::onUnknownError,
              onTimeoutError = activity::onTimeoutError,
              onNetworkError = activity::onNetworkError,
              onHttpError = activity::onHttpError,
              onErrorResponse = activity::onErrorResponse,
              onSuccess = {
                Toast.makeText(activity, "Transaksi cek bocor berhasil disimpan", Toast.LENGTH_SHORT).show()
                isCekBocorMasuk.trigger()
              }
            )
          } catch (e: EscPosConnectionException){
            onPrinterNotConnected.postTrigger()
            checkoutSuccess.value = service.hargaJual.toInt()
          }
        }
      }
    }
  }

  private fun getLastPressureBeforeCabut(): Int{
    var lastPressure = 0
    val size = jsonStrings.size
    for (i in size - 1 downTo 0) {
      val jsonObject = JSONObject(jsonStrings[i])
      if (jsonObject.has("tekanan")) {
        if (jsonObject.getInt("tekanan") < 5 || jsonObject.getInt("tekanan") > currentPressureData.value) {
          continue
        } else {
          lastPressure = jsonObject.getInt("tekanan")
          break
        }
      } else {
        continue
      }
    }
    return lastPressure
  }
}