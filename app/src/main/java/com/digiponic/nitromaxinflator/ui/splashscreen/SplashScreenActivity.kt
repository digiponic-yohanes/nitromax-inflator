package com.digiponic.nitromaxinflator.ui.splashscreen

import android.Manifest
import android.app.AlertDialog
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.extension.instantiateViewModel
import com.digiponic.nitromaxinflator.extension.isPermissionGranted
import com.digiponic.nitromaxinflator.external_libraries.bluetooth.Bluetooth
import com.digiponic.nitromaxinflator.services.ScreenReceiver
import com.digiponic.nitromaxinflator.ui.login.LoginActivity
import com.mcnmr.utilities.extension.getCompatColor
import com.mcnmr.utilities.extension.isNotNull
import kotlinx.android.synthetic.main.toolbar_layout_splash.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.lang.reflect.Method

class SplashScreenActivity : BaseActivity() {

  companion object {
    const val REQUEST_PERMISSION = 0
  }

  private val permissions by lazy {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
      arrayOf(
        Manifest.permission.BLUETOOTH,
        Manifest.permission.BLUETOOTH_CONNECT,
        Manifest.permission.BLUETOOTH_SCAN,
        Manifest.permission.BLUETOOTH_ADMIN,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.READ_MEDIA_IMAGES,
        Manifest.permission.CAMERA
      )
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
      arrayOf(
        Manifest.permission.BLUETOOTH,
        Manifest.permission.BLUETOOTH_CONNECT,
        Manifest.permission.BLUETOOTH_SCAN,
        Manifest.permission.BLUETOOTH_ADMIN,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
      )
    } else {
      arrayOf(
        Manifest.permission.BLUETOOTH,
        Manifest.permission.BLUETOOTH_ADMIN,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
      )
    }
  }

  private lateinit var alertDialog: AlertDialog
  private lateinit var progress: ProgressDialog
  private var currentMac = ""
  private var isAlreadyConnected = false

  private val vm by lazy { instantiateViewModel<SplashScreenVM>() }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_splash_screen)

    alertDialog = AlertDialog.Builder(this).create()
    progress = ProgressDialog(this)

    app.bluetooth.onBluetoothChangeStatus.observe(this) {

      if (vm.checkBluetoothName() && !isAlreadyConnected) {
        when(it){
          Bluetooth.BluetoothStatus.CONNECTING -> {
            vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_connecting))
            if (alertDialog.isShowing) alertDialog.dismiss()
            progress.setTitle("Menghubungkan Bluetooth")
            progress.setMessage("Harap tunggu...")
            progress.setCancelable(false) // disable dismiss by tapping outside of the dialog
            progress.show()
          }
          Bluetooth.BluetoothStatus.CONNECTED -> {
            vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_connected))
            if (progress.isShowing) progress.dismiss()
            if (alertDialog.isShowing) alertDialog.dismiss()
            isAlreadyConnected = true
            checkOutletEngine()
          }
          Bluetooth.BluetoothStatus.DISCONNECTED -> {
            vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_disconnected))
            if (progress.isShowing) progress.dismiss()
            if (alertDialog.isShowing) alertDialog.dismiss()

            alertDialog.setTitle("Mesin Tidak Terhubung")
            alertDialog.setMessage("Harap hubungkan terlebih dahulu tablet dengan mesin NitroMax.")
            alertDialog.setCancelable(false)
            alertDialog.show()
          }
        }
      }
    }

    val filter = IntentFilter(Intent.ACTION_SCREEN_ON)
    filter.addAction(Intent.ACTION_SCREEN_OFF)
    val mReceiver = ScreenReceiver()
    registerReceiver(mReceiver, filter)

    if (!isPermissionGranted(permissions)) {
      ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSION)
    } else {
//      vm.getCabangByMesin("D4:D4:DA:E4:9E:DE")
      checkBluetoothNameExist()
    }

    vm.getCabangMesinResponse.observe(this) {
      val result = it.data
      if (alertDialog.isShowing) alertDialog.dismiss()
      if (result.isNotNull()) {
        val alertDialog = AlertDialog.Builder(this).setTitle("Selamat Datang").setMessage("Selamat datang di ${result.nama_cabang}").setCancelable(false).show()

        val handler = Handler()
        val runnable = Runnable {
          if (alertDialog.isShowing) {
            alertDialog.dismiss()
          }
        }

        alertDialog.setOnDismissListener {
          Handler(Looper.getMainLooper()).postDelayed({
            startActivity<LoginActivity>(LoginActivity.CABANG_ARGUMENT to result.id_cabang)
            finishAffinity()
          }, 5000)

          handler.removeCallbacks(runnable)
        }

        handler.postDelayed(runnable, 5000)
      } else {
        alertDialog.setTitle("Mesin Tidak Teridentifikasi")
        alertDialog.setMessage("Pastikan mesin NitroMax outlet Anda telah di-registrasi ke pihak kami. MAC Address mesin : $currentMac")
        alertDialog.setCancelable(false)
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Hubungkan Ulang") { _, _ ->
          if (app.bluetooth.isConnected) {
            checkOutletEngine()
          } else {
            app.connectBluetooth(15000)
          }
        }
        alertDialog.show()
      }
    }
  }

  fun isConnected(device: BluetoothDevice): Boolean {
    var connected = false
    try {
      val m: Method = device.javaClass.getMethod("isConnected")
      connected = m.invoke(device) as Boolean
    } catch (e: java.lang.Exception) {
      toast(e.message.toString())
    }
    return connected
  }

  private fun checkOutletEngine(){
    val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
    val pairedDevices: Set<BluetoothDevice>? = bluetoothAdapter?.bondedDevices
    var isConnectedToNitromax = false
    pairedDevices?.forEach { device ->
      val deviceName = device.name
      val deviceHardwareAddress = device.address // MAC address
      if (deviceName == app.getBluetoothName() && isConnected(device)) {
        currentMac = deviceHardwareAddress
        vm.getCabangByMesin(currentMac)
        isConnectedToNitromax = true
      }
    }
    if (!isConnectedToNitromax) {
      alertDialog.setTitle("Mesin Tidak Terhubung")
      alertDialog.setMessage("Pastikan nama mesin yang diinputkan sudah sesuai.")
      alertDialog.setCancelable(false)
      alertDialog.show()
    }
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    if(permissions.isPermissionGranted(grantResults)){
      checkBluetoothNameExist()
    } else {
      for (perm in permissions) {
        Log.e("IS_GRANTED", "$perm ${PermissionChecker.checkSelfPermission(this, perm)}")
      }
      toast("Izin diperlukan untuk mengakses aplikasi")
    }
  }

  private fun checkBluetoothNameExist(){
    if (vm.checkBluetoothName()) {
      app.connectBluetooth(15000)
    } else {
      val textInput = EditText(this)
      textInput.inputType = InputType.TYPE_CLASS_TEXT
      textInput.setPadding(50,30,50,30)
      textInput.background = null
      textInput.hint = "Contoh : NITROMAXBT"
      textInput.textAlignment = View.TEXT_ALIGNMENT_CENTER

      val alert = AlertDialog.Builder(this).setTitle("Masukkan Nama Bluetooth")
        .setMessage("Nama Bluetooth yang tersambung ke mesin Inflator dapat dilihat di pengaturan Bluetooth.")
        .setCancelable(false)
        .setView(textInput).setPositiveButton("Submit", null).create()

      alert.setOnShowListener {
        val button: Button = (alert as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE)
        button.setOnClickListener { // TODO Do something
          val passwordEntered = textInput.text.toString()
          if (passwordEntered == "") {
            toast("Harap masukkan nama Bluetooth")
          } else {
            vm.database.insertNewBluetoothName(passwordEntered)
            alert.dismiss()
            app.connectBluetooth(15000)
          }
        }
      }

      alert.show()
    }
  }
}