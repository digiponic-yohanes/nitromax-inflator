package com.digiponic.nitromaxinflator.ui.transaksi.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.repository.network.response.DetailTransaksiResponse
import com.mcnmr.utilities.extension.gone
import com.mcnmr.utilities.extension.visible
import kotlinx.android.synthetic.main.row_transaksi.view.*
import org.jetbrains.anko.textColor
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class TransaksiAdapter(private val activity: BaseActivity,
                       private var transaksi: List<DetailTransaksiResponse.Transaksi>,
                       private var onClicked: (DetailTransaksiResponse.Transaksi) -> Unit): RecyclerView.Adapter<TransaksiAdapter.TransaksiVH>() {

  private var transactionToReprint = 0
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransaksiVH =
    TransaksiVH(LayoutInflater.from(activity).inflate(R.layout.row_transaksi, parent, false))

  override fun getItemCount(): Int = transaksi.size

  override fun onBindViewHolder(holder: TransaksiVH, position: Int) {
    var stringDetail = ""
    for (item in transaksi[position].detail) {
      stringDetail += "- ${item.nama} (${NumberFormat.getIntegerInstance().format(item.harga)} x ${NumberFormat.getIntegerInstance().format(item.qty)})\n"
    }

    if (getTimestampDifferenceFromNow(transaksi[position].tanggal) <= 10 && transactionToReprint < 5) {
      holder.btnPrint.visible()
      transactionToReprint++
    } else {
      holder.btnPrint.gone()
    }

    holder.tvKodePenjualan.text = transaksi[position].kode
    holder.tvTanggal.text = transaksi[position].tanggal.substring(0, 16)
    holder.tvProdukJasa.text = stringDetail
    holder.tvNominal.text = NumberFormat.getIntegerInstance().format(transaksi[position].subtotal)
    holder.tvMetodeBayar.text = transaksi[position].metode_bayar
    holder.btnPrint.setOnClickListener {
      onClicked(transaksi[position])
    }

    setTextColorByPaymentMethod(holder.tvKodePenjualan, transaksi[position])
    setTextColorByPaymentMethod(holder.tvTanggal, transaksi[position])
    setTextColorByPaymentMethod(holder.tvProdukJasa, transaksi[position])
    setTextColorByPaymentMethod(holder.tvNominal, transaksi[position])
    setTextColorByPaymentMethod(holder.tvMetodeBayar, transaksi[position])
  }

  fun setTransaksi(transaksi: List<DetailTransaksiResponse.Transaksi>){
    this.transaksi = transaksi
    notifyDataSetChanged()
  }

  private fun setTextColorByPaymentMethod(tv: TextView, transaksi: DetailTransaksiResponse.Transaksi){
    tv.textColor = if (transaksi.metode_bayar == "TUNAI") {
      Color.BLACK
    } else {
      if (transaksi.status_pembayaran == 26) Color.BLUE else Color.RED
    }
  }

  private fun getTimestampDifferenceFromNow(dateTime: String): Long {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
    val transactionDate = dateFormat.parse(dateTime)
    val currentDate = Date()

    val diff: Long = currentDate.time - transactionDate!!.time
    val seconds = diff / 1000
    return seconds / 60
  }

  inner class TransaksiVH(v: View): RecyclerView.ViewHolder(v){
    val tvKodePenjualan: TextView = v.tvKodePenjualan
    val tvTanggal: TextView = v.tvTanggalTransaksi
    val tvNominal: TextView = v.tvNominal
    val tvMetodeBayar: TextView = v.tvMetodeBayar
    val tvProdukJasa: TextView = v.tvProdukJasa
    val btnPrint: ImageButton = v.btnPrintTransaction
  }
}