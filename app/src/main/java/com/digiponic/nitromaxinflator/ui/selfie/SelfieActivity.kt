package com.digiponic.nitromaxinflator.ui.selfie

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.extension.instantiateViewModel
import com.digiponic.nitromaxinflator.repository.network.response.LoginResponse
import com.digiponic.nitromaxinflator.ui.dashboard.DashboardActivity
import com.mcnmr.utilities.extension.doIf
import com.mcnmr.utilities.internal_plugin.SerializableIntent
import com.mcnmr.utilities.internal_plugin.StringIntent
import kotlinx.android.synthetic.main.activity_selfie.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

class SelfieActivity : BaseActivity() {

  companion object {
    const val USERNAME_ARGUMENT = "USERNAME_ARGUMENT"
    const val LOGGED_USER_ARGUMENT = "LOGGED_USER_ARGUMENT"
    const val CONTINUE_PREVIOUS_ARGUMENT = "CONTINUE_PREVIOUS_ARGUMENT"

    const val REQUEST_CODE = 200
  }

  lateinit var alertDialog: AlertDialog
  lateinit var progress: ProgressDialog

  private val vm by lazy { instantiateViewModel<SelfieVM>() }

  @StringIntent(USERNAME_ARGUMENT)
  lateinit var username: String
  @SerializableIntent(LOGGED_USER_ARGUMENT)
  lateinit var user: LoginResponse.User
  @StringIntent(CONTINUE_PREVIOUS_ARGUMENT)
  lateinit var continuePrevious: String

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_selfie)

    alertDialog = AlertDialog.Builder(this).create()
    progress = ProgressDialog(this)

    vm.successCheckIn.observe(this) {
      startActivity<DashboardActivity>()
      finishAffinity()
    }

    imageAbsensi.setOnClickListener{
      capturePhoto()
    }

    btnAbsensi.setOnClickListener{
      vm.imageAbsenData.value.doIf(
        ifNull = {
          toast("Pemotretan absensi belum dilakukan")
        },
        ifNotNull = {
          vm.absen(username, user, continuePrevious)
        }
      )
    }
  }

  private fun capturePhoto() {
    val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    val photoFile = createImageFile()
    vm.imageAbsenUri.value = FileProvider.getUriForFile(this, "com.digiponic.nitromaxinflator.provider", photoFile!!)
    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, vm.imageAbsenUri.value)
    startActivityForResult(cameraIntent, REQUEST_CODE)
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE){
      this.contentResolver.notifyChange(vm.imageAbsenUri.value!!, null)
      val cr = this.contentResolver
      try {
        val photo = MediaStore.Images.Media.getBitmap(cr, vm.imageAbsenUri.value)
        val pickedImg = bitmapToFile(photo, "DC_${System.currentTimeMillis()}.jpg")
        vm.imageAbsenData.value = pickedImg
        Glide.with(this).load(pickedImg).into(imageAbsensi)
      } catch (e: Exception) {
        toast("Failed to load")
      }
    }
  }

  private fun createImageFile(): File? {
    val imageFileName = "DC_" + System.currentTimeMillis()
    val storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
    return File.createTempFile(imageFileName,".jpg", storageDir)
  }

  private fun bitmapToFile(bitmap: Bitmap, fileNameToSave: String): File? { // File name like "image.png"
    //create a file to write bitmap data
    var file: File? = null
    return try {
      file = File(cacheDir, fileNameToSave)
      file.createNewFile()

      //Convert bitmap to byte array
      val bos = ByteArrayOutputStream()
      bitmap.compress(Bitmap.CompressFormat.JPEG, 85, bos) // YOU can also save it in JPEG
      val bitmapdata = bos.toByteArray()

      //write the bytes in file
      val fos = FileOutputStream(file)
      fos.write(bitmapdata)
      fos.flush()
      fos.close()
      file
    } catch (e: Exception) {
      e.printStackTrace()
      file // it will return null
    }
  }

  private fun deleteFile(uri: Uri) {
    val file = File(uri.path!!)
    file.delete()
    if (file.exists()) {
      file.canonicalFile.delete()
      if (file.exists()) {
        applicationContext.deleteFile(file.name)
      }
    }
  }
}