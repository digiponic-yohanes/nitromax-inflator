package com.digiponic.nitromaxinflator.ui.check_tire

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.TypedValue
import android.view.View
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.extension.instantiateViewModel
import com.digiponic.nitromaxinflator.external_libraries.bluetooth.Bluetooth
import com.digiponic.nitromaxinflator.external_libraries.escposprinter.exceptions.EscPosConnectionException
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTICommand
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTIRequestService
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTIUpdatePressure
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTIYesNo
import com.digiponic.nitromaxinflator.repository.network.response.ItemResponse
import com.digiponic.nitromaxinflator.ui.checkout_success.CheckoutSuccessActivity
import com.google.gson.Gson
import com.jakewharton.rxbinding4.widget.textChanges
import com.mcnmr.utilities.extension.*
import com.mcnmr.utilities.internal_plugin.SerializableIntent
import kotlinx.android.synthetic.main.activity_check_tire.*
import kotlinx.android.synthetic.main.activity_check_tire.btnForceContinue
import kotlinx.android.synthetic.main.activity_check_tire.btnForceFinish
import kotlinx.android.synthetic.main.activity_check_tire.btnPrintAndClose
import kotlinx.android.synthetic.main.activity_check_tire.btnProses
import kotlinx.android.synthetic.main.activity_check_tire.clIndikatorNitrogen
import kotlinx.android.synthetic.main.activity_check_tire.etTekanan
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.indikator_nitrogen.*
import kotlinx.android.synthetic.main.toolbar_with_reconnect_layout.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import kotlin.text.isEmpty

class CheckTireActivity : BaseActivity() {
  companion object {
    const val SERVICE_ARGUMENT = "SERVICE_ARGUMENT"
    const val TAMBAL_ARGUMENT = "TAMBAL_ARGUMENT"
    const val TAMBAH_ARGUMENT = "TAMBAH_ARGUMENT"
    const val RESULT = "RESULT"
    const val IS_TIDAK_BOCOR = "IS_TIDAK_BOCOR"
  }

  private val vm by lazy { instantiateViewModel<CheckTireVM>() }

  private var isBack: Boolean = true

  @SerializableIntent(SERVICE_ARGUMENT)
  lateinit var service: ItemResponse.Item

  @SerializableIntent(TAMBAH_ARGUMENT)
  lateinit var tambah: ItemResponse.Item

  @SerializableIntent(TAMBAL_ARGUMENT)
  lateinit var tambal: ItemResponse.Item

  private lateinit var animationDrawable: AnimationDrawable

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_check_tire)

    animationDrawable = arrowIndicator.background as AnimationDrawable

    ivCarMotor.setImageResource(if (service.id == 5) R.drawable.motorcycle_big else R.drawable.car_big)
    cabangNama.text = vm.preferenceRepository.getUser()!!.namaCabang

    clQuestionBocor.visibility = View.GONE
    clQuestionLanjut.visibility = View.GONE

    app.bluetooth.onBluetoothChangeStatus.observe(this) {
      when(it){
        Bluetooth.BluetoothStatus.CONNECTING -> vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_connecting))
        Bluetooth.BluetoothStatus.CONNECTED -> vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_connected))
        Bluetooth.BluetoothStatus.DISCONNECTED -> vBluetoothStatus.setBackgroundColor(getCompatColor(R.color.bluetooth_disconnected))
      }
    }

    vm.transactionCodeData.observe(this) {
      etTekanan.visibleOrGoneIf(it == "")
      btnProses.visibleOrGoneIf(it == "")
      btnForceContinue.visibleOrGoneIf(it == "")
      btnPrintAndClose.visibleOrGoneIf(it != "")
    }

    vm.bluetoothLoadingModeTransaction.observe(this) {
      btnProses.isEnabled = it.not()
      btnProses.setBackgroundColor(if(it.not()) getCompatColor(R.color.colorPrimary) else getCompatColor(R.color.graydddd))
    }

    vm.currentPressureRequestData.observe(this) { tvTekananValue.text = it.toString() }

    vm.bluetoothPressureResponse.observe(this) {
      val tekananDisplay = it.tekanan
      tvTekananCurValue.text = tekananDisplay.toString()
      vm.currentPressureRequestData.value = it.ref
    }

    vm.isStartProcessFilling.observe(this) {
      arrowIndicator.visibleOrGoneIf(it)
      clIndikatorCurrent.visibleOrGoneIf(it)
    }

    vm.transactionCancelled.observe(this) { if (it) finish() }
    vm.errorBrokenPipe.observe(this) { toast(it) }

    vm.onPengisianSelesai.observe(this) {
      if (it) {
        etTekanan.gone()
        btnProses.gone()
        btnForceContinue.gone()
        clIndikatorNitrogen.gone()
        btnForceFinish.gone()
        clQuestionBocor.visible()
      }
    }

    vm.bluetoothQuestionBocorStatus.observe(this) {
      if (it) {
        if (vm.isBanBocor.value) {
          clQuestionBocor.gone()
          clQuestionLanjut.visible()
        }
      }
    }

    vm.bluetoothOutputResponse.observe(this) {
      if (it.detail.modeTransaksi in 3..4 && vm.isAnswerBocor.value) {
        vm.payTransaction(tambah)
      } else {
        app.saveNormalTransaction(it, Gson().toJson(it), this, vm.errorBrokenPipe, vm.onPrinterNotConnected, vm.checkoutSuccess, false)
      }
    }

    vm.bluetoothQuestionLanjutStatus.observe(this) {
      if (it) vm.payTransactionTwo(service)
    }

    vm.bluetoothStatusModeTransaction.observe(this) {
      if(it){
        isBack = false
        btnProses.text = getString(R.string.btnUpdateTekanan)
        btnProses.setOnClickListener {
          if(app.bluetooth.isConnected){
            if (etTekanan.text.toString().isEmpty()) {
              alertDialog("Kesalahan", "Tekanan harus diisi")
            } else {
              if (vm.pressureData.value in 17..99) {
                vm.bluetoothLoadingModeTransaction.value = true
                vm.currentPressureRequestData.value = vm.pressureData.value
                vm.databaseHelper.writeLogToFile(Gson().toJson(BTIUpdatePressure(tekanan = vm.pressureData.value)), "Input")
                app.bluetooth.send(Gson().toJson(BTIUpdatePressure(tekanan = vm.pressureData.value)), charset("utf-8"))
                etTekanan.setText("")
              } else {
                alertDialog("Kesalahan", "Nilai tekanan yang diizinkan berkisar antara 17-99 Psi")
              }
            }
          } else {
            toast("Bluetooth tidak terhubung")
          }
        }
      } else {
        btnProses.text = getString(R.string.btnProses)
        btnProses.setOnClickListener {
          if(app.bluetooth.isConnected){
            if (etTekanan.text.toString().isEmpty()) {
              alertDialog("Kesalahan", "Tekanan harus diisi")
            } else {
              if (vm.pressureData.value in 17..99) {
                vm.bluetoothLoadingModeTransaction.value = true
                vm.currentPressureRequestData.value = vm.pressureData.value
                vm.databaseHelper.writeLogToFile(Gson().toJson(BTIRequestService(modeTransaksi = service.id, jumlahBan = 1, tekanan = vm.pressureData.value)), "Input")
                app.bluetooth.send(Gson().toJson(BTIRequestService(modeTransaksi = service.id, jumlahBan = 1, tekanan = vm.pressureData.value)), charset("utf-8"))
                etTekanan.setText("")
              } else {
                alertDialog("Kesalahan", "Nilai tekanan yang diizinkan berkisar antara 17-99 Psi")
              }
            }
          } else {
            toast("Bluetooth tidak terhubung")
          }
        }
      }
    }

    vm.isCekBocorMasuk.observe(this) {
      if (vm.isLanjutTambal.value) {
        val cart: LinkedHashMap<ItemResponse.Item, Int> = linkedMapOf()
        cart[tambal] = 1
        setResult(RESULT_OK, Intent().apply { putExtra(RESULT, cart) })
        finish()
      } else {
        finish()
      }
    }

    vm.checkoutSuccess.observe(this) {
      Handler(Looper.getMainLooper()).postDelayed({
        finish()
        startActivity<CheckoutSuccessActivity>(CheckoutSuccessActivity.ARGS_HARGA to it)
      }, 2000)
    }

    vm.onPrinterNotConnected.observe(this) {
      toast("Printer tidak terhubung, pastikan printer telah terhubung dengan bluetooth")
    }

    etTekanan.textChanges().subscribe {
      vm.pressureData.value = try {
        it.toString().toInt()
      }catch (e: NumberFormatException){
        0
      }
    }

    vm.btnAnginTidakKeluarPressed.observe(this) {
      btnForceContinue.isEnabled = it.not()
      btnForceContinue.setBackgroundColor(if(it.not()) getCompatColor(R.color.colorPrimary) else getCompatColor(R.color.graydddd))
    }

    btnForceContinue.setOnClickListener {
      if(app.bluetooth.isConnected){
        vm.databaseHelper.writeLogToFile(Gson().toJson(BTICommand(value = "open")), "Input")
        app.bluetooth.send(Gson().toJson(BTICommand(value = "open")), charset("utf-8"))
        vm.btnAnginTidakKeluarPressed.value = true
      }else {
        toast("Bluetooth tidak terhubung")
      }
    }

    btnForceFinish.setOnClickListener {
      if(app.bluetooth.isConnected){
        vm.databaseHelper.writeLogToFile(Gson().toJson(BTICommand(value = "cancel")), "Input")
        app.bluetooth.send(Gson().toJson(BTICommand(value = "cancel")), charset("utf-8"))
      }else {
        toast("Bluetooth tidak terhubung")
      }
    }

    btnNoLanjut.setOnClickListener {
      if(app.bluetooth.isConnected){
        vm.isLanjutTambal.value = false
        vm.databaseHelper.writeLogToFile(Gson().toJson(BTIYesNo(value = false)), "Input")
        app.bluetooth.send(Gson().toJson(BTIYesNo(value = false)), charset("utf-8"))
        // Disable setelah ditekan sebelum ada response lagi
        btnNoLanjut.isEnabled = false
        btnNoLanjut.setBackgroundColor(if (btnNoBocor.isEnabled) getCompatColor(R.color.colorPrimary) else getCompatColor(R.color.graydddd))
      } else {
        toast("Bluetooth tidak terhubung")
      }
    }

    btnYesLanjut.setOnClickListener {
      if(app.bluetooth.isConnected){
        vm.isLanjutTambal.value = true
        vm.databaseHelper.writeLogToFile(Gson().toJson(BTIYesNo(value = true)), "Input")
        app.bluetooth.send(Gson().toJson(BTIYesNo(value = true)), charset("utf-8"))
        // Disable setelah ditekan sebelum ada response lagi
        btnYesLanjut.isEnabled = false
        btnYesLanjut.setBackgroundColor(if (btnYesBocor.isEnabled) getCompatColor(R.color.colorPrimary) else getCompatColor(R.color.graydddd))
      }else {
        toast("Bluetooth tidak terhubung")
      }
    }

    btnNoBocor.setOnClickListener {
      if(app.bluetooth.isConnected){
        vm.isAnswerBocor.value = true
        vm.isBanBocor.value = false
        vm.databaseHelper.writeLogToFile(Gson().toJson(BTIYesNo(value = false)), "Input")
        app.bluetooth.send(Gson().toJson(BTIYesNo(value = false)), charset("utf-8"))
        // Disable setelah ditekan sebelum ada response lagi
        btnNoBocor.isEnabled = false
        btnNoBocor.setBackgroundColor(if (btnNoBocor.isEnabled) getCompatColor(R.color.colorPrimary) else getCompatColor(R.color.graydddd))
      } else {
        toast("Bluetooth tidak terhubung")
      }
    }

    btnYesBocor.setOnClickListener {
      if(app.bluetooth.isConnected){
        vm.isAnswerBocor.value = true
        vm.isBanBocor.value = true
        vm.databaseHelper.writeLogToFile(Gson().toJson(BTIYesNo(value = true)), "Input")
        app.bluetooth.send(Gson().toJson(BTIYesNo(value = true)), charset("utf-8"))
        // Disable setelah ditekan sebelum ada response lagi
        btnYesBocor.isEnabled = false
        btnYesBocor.setBackgroundColor(if (btnYesBocor.isEnabled) getCompatColor(R.color.colorPrimary) else getCompatColor(R.color.graydddd))
      }else {
        toast("Bluetooth tidak terhubung")
      }
    }

    btnPrintAndClose.setOnClickListener {
      try{
        //vm.print(service, true)
      }catch(e: EscPosConnectionException){
        vm.onPrinterNotConnected.trigger()
      }
    }

    tvReconnectMikro.setOnClickListener {
      app.bluetooth.connectToName(app.getBluetoothName())
    }
  }

  override fun onWindowFocusChanged(hasFocus: Boolean) {
    super.onWindowFocusChanged(hasFocus)
    if (hasFocus) animationDrawable.start()
  }

  override fun onUserLeaveHint() {
    super.onUserLeaveHint()
    app.saveAppHistory("Keluar dari aplikasi dengan memencet tombol Home")
  }

  override fun onBackPressed() {
    if (isBack) {
      alertDialog(
        title = "Kembali",
        message = "Apakah anda yakin ingin membatalkan transaksi?",
        positiveTitle = "Ya",
        negativeTitle = "Tidak",
        positiveAction = {
          if (app.bluetooth.isConnected) {
            vm.databaseHelper.writeLogToFile(Gson().toJson(BTICommand(value = "cancel")), "Input")
            app.bluetooth.send(Gson().toJson(BTICommand(value = "cancel")), charset("utf-8"))
            finish()
          } else {
            toast("Bluetooth tidak terhubung")
          }
        }
      )
    }
  }
}