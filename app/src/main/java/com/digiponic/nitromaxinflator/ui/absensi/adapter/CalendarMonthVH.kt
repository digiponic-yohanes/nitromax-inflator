package com.digiponic.nitromaxinflator.ui.absensi.adapter

import android.view.View
import android.widget.TextView
import com.kizitonwose.calendarview.ui.ViewContainer
import kotlinx.android.synthetic.main.row_calendar_month.view.*

class CalendarMonthVH(v: View) : ViewContainer(v) {
    val tvCalendarMonth: TextView = v.tvCalendarMonth
}