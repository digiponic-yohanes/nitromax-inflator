package com.digiponic.nitromaxinflator.ui.payment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.repository.network.response.ItemResponse
import kotlinx.android.synthetic.main.row_payment_cart.view.*

class PaymentCartAdapter(val context: Context,
                         val carts: HashMap<ItemResponse.Item, Int>)
    : RecyclerView.Adapter<PaymentCartAdapter.PaymentCartVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentCartVH =
        PaymentCartVH(LayoutInflater.from(context).inflate(R.layout.row_payment_cart, parent, false))

    override fun getItemCount(): Int = carts.size

    override fun onBindViewHolder(holder: PaymentCartVH, position: Int) {
        val key = carts.keys.toList()[position]

        holder.tvItemName.text = key.keterangan
        holder.tvQuantity.text = carts[key].toString()
        holder.tvItemPrice.text = (key.hargaJual.toInt() * (carts[key] ?: 0)).toString()
    }

    inner class PaymentCartVH(v: View) : RecyclerView.ViewHolder(v){
        val tvItemName: TextView = v.tvItemName
        val tvQuantity: TextView = v.tvItemQuantity
        val tvItemPrice: TextView = v.tvItemPrice
    }
}