package com.digiponic.nitromaxinflator.ui.payment

import android.app.AlertDialog
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.digiponic.nitromaxinflator.MainApplication
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.ancestors.BaseViewModel
import com.digiponic.nitromaxinflator.extension.NonNullLiveData
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param.BTReply
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.response.BTOutputResponse
import com.digiponic.nitromaxinflator.repository.bluetooth_arduino.response.BTOutputTekanan
import com.digiponic.nitromaxinflator.repository.network.parameter.PayTransactionParams
import com.digiponic.nitromaxinflator.repository.network.request.RemoteRepository
import com.digiponic.nitromaxinflator.repository.network.response.ItemResponse
import com.digiponic.nitromaxinflator.repository.preference.PreferenceRepository
import com.digiponic.nitromaxinflator.sqlite.DatabaseHelper
import com.google.gson.Gson
import com.mcnmr.utilities.extension.isNotNull
import com.mcnmr.utilities.wrapper.SingleEventWrapper
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

enum class TransactionType {
  REGULAR_TRANSACTION, TIRE_TRANSACTION, TIRE_REPAIR_TRANSACTION
}

class PaymentVM(activity: BaseActivity) : BaseViewModel(activity) {
  companion object {
    const val POST_CHECKOUT = "POST_CHECKOUT"
  }

  @Inject
  lateinit var remoteRepository: RemoteRepository

  @Inject
  lateinit var preferenceRepository: PreferenceRepository

  val btnAnginTidakKeluarPressed = NonNullLiveData(false)
  val metodePembayaranData = NonNullLiveData(29)

  val holeData = NonNullLiveData(0)
  val transactionTypeData = NonNullLiveData(TransactionType.REGULAR_TRANSACTION)
  val pressureData = NonNullLiveData(0)
  private val currentPressureData = NonNullLiveData(0)
  val currentPressureRequestData = NonNullLiveData(0)
  val transactionCodeData = NonNullLiveData("")
  val isAlreadyCabut = NonNullLiveData(false)
  val isiBaruItem = MutableLiveData<ItemResponse.Item>()
  val isLoadingSentTransaction = NonNullLiveData(false)

  val onPrinterNotConnected = SingleEventWrapper<Void>()
  val errorBrokenPipe = SingleEventWrapper<String>()

  val bluetoothOutputResponse = SingleEventWrapper<BTOutputResponse>()
  val bluetoothLoadingModeTransaction = NonNullLiveData(false)
  val bluetoothStatusModeTransaction = NonNullLiveData(false)
  val bluetoothPressureResponse = SingleEventWrapper<BTOutputTekanan>()
  val bluetoothCancelTransaction = SingleEventWrapper<Void>()

  val checkoutSuccess = SingleEventWrapper<Int>()
  var jsonStrings = mutableListOf<String>()
  val isStartProcessFilling = NonNullLiveData(false)
  private val isGettingInitialPressure = NonNullLiveData(false)

  val dbHelper = DatabaseHelper(activity)

  init {
    (activity.application as MainApplication).consumer.inject(this)

    app.bluetooth.onMessage.observe(activity) { message ->
      bluetoothLoadingModeTransaction.value = false
      message?.let {
        val stringMessage = String(it)
        Log.e("STRING_MESSAGE", stringMessage)
        dbHelper.writeLogToFile(stringMessage, "Response")
        jsonStrings.add(stringMessage)
        try {
          val jsonObject = JSONObject(stringMessage)
          if (jsonObject.has("note")) {
            // dbHelper.writeLogToFile(stringMessage, "Response")
            when (jsonObject.getString("note")) {
              "transaction" -> {
                bluetoothStatusModeTransaction.value = jsonObject.has("status") && jsonObject.getString("status") == "ok"
                bluetoothLoadingModeTransaction.value = !(jsonObject.has("status") && jsonObject.getString("status") == "ok")
              }
              "open" -> btnAnginTidakKeluarPressed.value = !(jsonObject.has("status") && jsonObject.getString("status") == "ok")
              "tekanan" -> {
                bluetoothStatusModeTransaction.value = jsonObject.has("status") && jsonObject.getString("status") == "ok"
              }
              "cancel" -> if (!isAlreadyCabut.value) bluetoothCancelTransaction.trigger()
            }
          } else if (jsonObject.has("tekanan")) {
            if (!isStartProcessFilling.value) isStartProcessFilling.value = true
            if (!isGettingInitialPressure.value) {
              bluetoothPressureResponse.value = Gson().fromJson(jsonObject.toString(), BTOutputTekanan::class.java)
              isGettingInitialPressure.value = true
            }
          } else if (jsonObject.has("mode")) {
            // dbHelper.writeLogToFile(stringMessage, "Response")
            when (jsonObject.getString("mode")) {
              "output" -> if (jsonObject.has("status") && jsonObject.getString("status") == "ok") {
                bluetoothOutputResponse.value = Gson().fromJson(jsonObject.toString(), BTOutputResponse::class.java)
                Gson().fromJson(jsonObject.toString(), BTOutputResponse::class.java)
              }
              "info" -> if (jsonObject.has("kind") && jsonObject.getString("kind") == "askJumlahLubang") {
                isAlreadyCabut.value = true
                currentPressureData.value = 0
              }
              "communicationTest" -> app.bluetooth.send(Gson().toJson(BTReply()), charset("utf-8"))
            }
          } else if (jsonObject.has("status") && jsonObject.getString("status") == "proses ban berhasil") {
            // dbHelper.writeLogToFile(stringMessage, "Response")
            currentPressureData.value = 0
            isAlreadyCabut.value = true
            isStartProcessFilling.value = false
            isGettingInitialPressure.value = false
          }
        } catch (e: Exception){
          e.printStackTrace()
        }
      }
    }
  }

  fun checkTransaction(items: HashMap<ItemResponse.Item, Int>){
    for((item, _) in items){
      if(item.id in 1..4 || item.id == 10){
        transactionTypeData.value = TransactionType.TIRE_TRANSACTION
        break
      }else if(item.id in 7..8 || item.id in 11..12){
        transactionTypeData.value = TransactionType.TIRE_REPAIR_TRANSACTION
        break
      }else {
        transactionTypeData.value = TransactionType.REGULAR_TRANSACTION
      }
    }
  }

  fun payTransaction(items: HashMap<ItemResponse.Item, Int>) {
    preferenceRepository.getUser()?.let { session ->
      var total = 0
      val detail = mutableListOf<PayTransactionParams.PayTransactionDetail>()

      var keterangan = ""
      var lastJsonString = ""
      if (jsonStrings.size > 0) {
        lastJsonString = jsonStrings[jsonStrings.size - 1]
      }
      var hasNitrogenTransaction = false
      var tanggal = SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS", Locale.getDefault()).format(Date())

      for ((item, quantity) in items) {
        when (item.id) {
          1, 2, 10 -> {
            var totalGagal = 0
            val keteranganTekanan = mutableListOf<String>()
            if (bluetoothOutputResponse.value.isNotNull()) {
              val pressures = bluetoothOutputResponse.value!!.pressure(if (item.id == 10) 2 else item.id, item.hargaJual.toInt(), session.skalaGagal)
              total += item.hargaJual.toInt() * quantity
              pressures.forEach { pressure ->
                keteranganTekanan.add(if (pressure.tekanan == 0) {
                  "${pressure.tekananAwal}/${pressure.tekanan}/${currentPressureRequestData.value}/${getLastPressureBeforeCabut()}"
                } else {
                  "${pressure.tekananAwal}/${pressure.tekanan}"
                })
                total += pressure.harga
                totalGagal += if (pressure.isGagal) 1 else 0
              }
              tanggal = bluetoothOutputResponse.value!!.tglTransaksi
            } else {
              if (isAlreadyTancap()) {
                keteranganTekanan.add("${getFirstPressure()}/0/${currentPressureRequestData.value}/${getLastPressureBeforeCabut()}")
              }
            }
            detail.add(
              PayTransactionParams.PayTransactionDetail(
                idItem = item.id.toString(),
                namaItem = item.keterangan,
                harga = item.hargaJual.toInt(),
                qty = quantity,
                gagal = totalGagal,
                keterangan = keteranganTekanan.joinToString()
              )
            )
            hasNitrogenTransaction = true
          }
          3, 4 -> {
            var totalIsiTambah = 0
            var totalIsiBaru = 0
            var totalGagalIsiBaru = 0
            var totalGagalIsiTambah = 0
            val keteranganTekananIsiBaru = mutableListOf<String>()
            val keteranganTekananIsiTambah = mutableListOf<String>()
            if (bluetoothOutputResponse.value.isNotNull()) {
              val pressures = bluetoothOutputResponse.value!!.pressure(item.id, item.hargaJual.toInt(), session.skalaGagal)
              pressures.forEach { pressure ->
                val isTekananTerhitungIsiBaru = if (item.id == 3) {
                  pressure.tekananAwal <= session.limit_pressure_inflation_motor && !pressure.isTts
                } else {
                  pressure.tekananAwal <= session.limit_pressure_inflation_mobil && !pressure.isTts
                }

                if (isTekananTerhitungIsiBaru) {
                  keteranganTekananIsiBaru.add(if (pressure.tekanan == 0) {
                    "${pressure.tekananAwal}/${pressure.tekanan}/${currentPressureRequestData.value}/${getLastPressureBeforeCabut()}"
                  } else {
                    "${pressure.tekananAwal}/${pressure.tekanan}"
                  })
                  totalIsiBaru++
                  totalGagalIsiBaru += if (pressure.isGagal) 1 else 0
                } else {
                  keteranganTekananIsiTambah.add(if (pressure.tekanan == 0) {
                    "${pressure.tekananAwal}/${pressure.tekanan}/${currentPressureRequestData.value}/${getLastPressureBeforeCabut()}"
                  } else {
                    "${pressure.tekananAwal}/${pressure.tekanan}"
                  })
                  totalIsiTambah++
                  totalGagalIsiTambah += if (pressure.isGagal) 1 else 0
                }
              }
              tanggal = bluetoothOutputResponse.value!!.tglTransaksi
            } else {
              if (isAlreadyTancap()) keteranganTekananIsiTambah.add("${getFirstPressure()}/0/${currentPressureData.value}/${getLastPressureBeforeCabut()}")
              detail.add(PayTransactionParams.PayTransactionDetail(item.id.toString(), item.keterangan, item.hargaJual.toInt(), 0, 0, keteranganTekananIsiTambah.joinToString()))
            }
            if (totalIsiBaru > 0) {
              total += (item.hargaJual.toInt() * totalIsiBaru)
              detail.add(PayTransactionParams.PayTransactionDetail(isiBaruItem.value!!.id.toString(), isiBaruItem.value!!.keterangan, isiBaruItem.value!!.hargaJual.toInt(), totalIsiBaru, totalGagalIsiBaru, keteranganTekananIsiBaru.joinToString()))
            }
            if (totalIsiTambah > 0) {
              total += ((item.hargaJual.toInt() * totalIsiTambah - totalGagalIsiTambah) + (item.hargaJual.toDouble() * totalGagalIsiTambah.toDouble() * session.skalaGagal).toInt())
              detail.add(PayTransactionParams.PayTransactionDetail(item.id.toString(), item.keterangan, item.hargaJual.toInt(), totalIsiTambah, totalGagalIsiTambah, keteranganTekananIsiTambah.joinToString()))
            }
            hasNitrogenTransaction = true
          }
          else -> {
            if (item.id in 7..8 || item.id in 11..12) {
              if (bluetoothOutputResponse.value.isNotNull()) tanggal = bluetoothOutputResponse.value!!.tglTransaksi
              hasNitrogenTransaction = true
              if (session.has_tambal_ban_khusus == 1) {
                if (bluetoothOutputResponse.value.isNotNull()) {
                  val pressures = bluetoothOutputResponse.value!!.pressure(item.id, item.hargaJual.toInt(), session.skalaGagal)
                  for (i in pressures.indices) {
                    val isTekananTerhitungIsiBaru = if (item.id == 7 || item.id == 11) {
                      pressures[i].tekananAwal <= session.limit_pressure_inflation_motor && !pressures[i].isTts
                    } else {
                      pressures[i].tekananAwal <= session.limit_pressure_inflation_mobil && !pressures[i].isTts
                    }

                    if (isTekananTerhitungIsiBaru) {
                      val keteranganTekanan = "${pressures[i].tekananAwal}/${pressures[i].tekanan}"
                      val totalGagalIsiBaru = if (pressures[i].isGagal) 1 else 0
                      detail.add(PayTransactionParams.PayTransactionDetail(isiBaruItem.value!!.id.toString(), isiBaruItem.value!!.keterangan, isiBaruItem.value!!.hargaJual.toInt(), 1, totalGagalIsiBaru, keteranganTekanan))
                    }
                  }
                }
              }
            }
            total += item.hargaJual.toInt() * quantity
            detail.add(
              PayTransactionParams.PayTransactionDetail(
                idItem = item.id.toString(),
                namaItem = item.keterangan,
                harga = item.hargaJual.toInt(),
                qty = quantity,
                gagal = 0,
                keterangan = ""
              )
            )
          }
        }
      }

      if (hasNitrogenTransaction && bluetoothOutputResponse.value.isNotNull()) keterangan = lastJsonString

      val params = PayTransactionParams(
        idMode = 0,
        user = session.email,
        tanggal = tanggal,
        cabang = session.idCabang,
        jenisKendaraan = "0",
        bayar = total,
        detail = detail,
        metodePembayaran = metodePembayaranData.value,
        json_string = keterangan
      )
      viewModelScope.launch {
        isLoadingSentTransaction.value = true
        activity.shouldShowLoading(POST_CHECKOUT)
        val result = safeApiCall(POST_CHECKOUT) { remoteRepository.newCheckout(params) }
        activity.shouldHideLoading(POST_CHECKOUT)
        result.consume(
          onUnknownError = activity::onUnknownError,
          onTimeoutError = activity::onTimeoutError,
          onNetworkError = { _, _ -> writePendingTransaction(params) },
          onHttpError = activity::onHttpError,
          onErrorResponse = activity::onErrorResponse,
          onSuccess = { dtr ->
            if (session.is_using_qris == 1 && dtr.data.subtotal >= 1500) {
              app.triggerQrisTransaction(activity, dtr.data, errorBrokenPipe, onPrinterNotConnected, checkoutSuccess)
            } else {
              if (session.is_auto_print_struk == 1) {
                app.printWithProgressBar(dtr.data, activity, errorBrokenPipe, onPrinterNotConnected, checkoutSuccess)
              } else {
                checkoutSuccess.value = dtr.data.subtotal
              }
            }
          }
        )
      }
    }
  }

  fun savePendingTransaction(params: PayTransactionParams) {
    viewModelScope.launch {
      activity.shouldShowLoading(POST_CHECKOUT)
      val result = safeApiCall(POST_CHECKOUT) { remoteRepository.newCheckout(params) }
      activity.shouldHideLoading(POST_CHECKOUT)
      result.consume(
        onUnknownError = activity::onUnknownError,
        onTimeoutError = {
          alertConnectionNotEstabilished(params)
        },
        onNetworkError = { _, _ ->
          alertConnectionNotEstabilished(params)
        },
        onHttpError = activity::onHttpError,
        onErrorResponse = activity::onErrorResponse,
        onSuccess = { dtr ->
          dbHelper.deletePendingTransaction()
          app.printWithProgressBar(dtr.data, activity, errorBrokenPipe, onPrinterNotConnected, checkoutSuccess)
        }
      )
    }
  }

  private fun writePendingTransaction(params: PayTransactionParams) {
    preferenceRepository.getUser()?.let {
      if (dbHelper.writePendingTransaction(it.skalaGagal, params)) {
        Toast.makeText(activity, "Koneksi internet tidak stabil. Data disimpan sementara dalam database lokal", Toast.LENGTH_SHORT).show()
      } else {
        alertConnectionNotEstabilished(params)
      }
    }
  }

  private fun isAlreadyTancap(): Boolean {
    for (i in jsonStrings.indices) {
      val jsonObject = JSONObject(jsonStrings[i])
      if (jsonObject.has("tekanan")) {
        return true
      }
    }
    return false
  }

  private fun getFirstPressure(): Int {
    for (i in jsonStrings.indices) {
      val jsonObject = JSONObject(jsonStrings[i])
      if (jsonObject.has("tekanan")) {
        return jsonObject.getInt("tekanan")
      }
    }
    return 0
  }

  private fun getLastPressureBeforeCabut(): Int{
    var lastPressure = 0
    val size = jsonStrings.size
    for (i in size - 1 downTo 0) {
      val jsonObject = JSONObject(jsonStrings[i])
      if (jsonObject.has("tekanan")) {
        if (jsonObject.getInt("tekanan") < 5 || jsonObject.getInt("tekanan") > currentPressureData.value) {
          continue
        } else {
          lastPressure = jsonObject.getInt("tekanan")
          break
        }
      } else {
        continue
      }
    }
    return lastPressure
  }

  private fun alertConnectionNotEstabilished(params: PayTransactionParams) {
    AlertDialog.Builder(activity).setTitle("Koneksi Belum Stabil")
      .setMessage("Pastikan koneksi sudah stabil sebelum melakukan recovery transaksi")
      .setCancelable(false)
      .setPositiveButton("OK") { _, _ ->
        savePendingTransaction(params)
      }.show()
  }
}