package com.digiponic.nitromaxinflator.ui.checkout_success

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.extension.instantiateViewModel
import com.mcnmr.utilities.internal_plugin.IntIntent
import kotlinx.android.synthetic.main.activity_checkout_success.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.text.NumberFormat

class CheckoutSuccessActivity : BaseActivity() {
  companion object {
    const val ARGS_HARGA = "ARGS_HARGA"
  }

  @IntIntent(ARGS_HARGA, default = 0)
  @JvmField
  var harga: Int = 0

  private val vm by lazy { instantiateViewModel<CheckoutSuccessVM>() }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_checkout_success)

    tvCostValue.text = "Rp ${NumberFormat.getInstance().format(harga)}"

    Handler(Looper.getMainLooper()).postDelayed({ finish() }, 5000)

    cabangNama.setText(vm.preferenceRepository.getUser()!!.namaCabang)
  }

  override fun onUserLeaveHint() {
    super.onUserLeaveHint()
    app.saveAppHistory("Keluar dari aplikasi dengan memencet tombol Home")
  }
}