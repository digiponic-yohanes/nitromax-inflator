package com.digiponic.nitromaxinflator.ui.checkout_success

import com.digiponic.nitromaxinflator.MainApplication
import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.ancestors.BaseViewModel
import com.digiponic.nitromaxinflator.repository.preference.PreferenceRepository
import javax.inject.Inject

class CheckoutSuccessVM(activity: BaseActivity): BaseViewModel(activity) {

    @Inject
    lateinit var preferenceRepository: PreferenceRepository

    init {
        (activity.application as MainApplication).consumer.inject(this)
    }
}