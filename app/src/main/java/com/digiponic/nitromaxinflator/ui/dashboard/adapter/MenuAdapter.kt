package com.digiponic.nitromaxinflator.ui.dashboard.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.digiponic.nitromaxinflator.R
import com.digiponic.nitromaxinflator.model.Menu
import kotlinx.android.synthetic.main.row_menu.view.*

class MenuAdapter(private val context: Context,
                  private val onMenuClicked: (Menu) -> Unit): RecyclerView.Adapter<MenuAdapter.MenuVH>() {
    private val menus: List<Menu> = Menu.generate()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuVH =
        MenuVH(LayoutInflater.from(context).inflate(R.layout.row_menu, parent, false))

    override fun getItemCount(): Int = menus.size

    override fun onBindViewHolder(holder: MenuVH, position: Int) {
        holder.tvMenu.text = menus[position].name

        holder.clContainer.setOnClickListener { onMenuClicked(menus[position]) }
    }

    inner class MenuVH(v: View): RecyclerView.ViewHolder(v) {
        val clContainer: ConstraintLayout = v.clContainer
        val tvMenu: TextView = v.tvMenu
    }
}