package com.digiponic.nitromaxinflator.ui.absensi.adapter

import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.kizitonwose.calendarview.ui.ViewContainer
import kotlinx.android.synthetic.main.row_calendar_day.view.*

class CalendarDayVH(v: View) : ViewContainer(v) {
    val tvCalendarDay: TextView = v.tvCalendarDay
    val llContainer: LinearLayout = v.llContainer
}