package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CabangResponse(
    @SerializedName("data")
    val data: Cabang,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
) : BaseResponse, Serializable {
    override fun status(): Boolean = status
    override fun message(): String = message

    data class Cabang(
        @SerializedName("id")
        val id_cabang: String,
        @SerializedName("nama_cabang")
        val nama_cabang: String,
        @SerializedName("header_cabang")
        val header_cabang: String,
        @SerializedName("skala_gagal")
        val skalaGagal: Double
    )
}
