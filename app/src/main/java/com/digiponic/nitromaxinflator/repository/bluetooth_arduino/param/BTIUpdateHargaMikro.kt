package com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param

import com.google.gson.annotations.SerializedName

data class BTIUpdateHargaMikro(
    @SerializedName("mode")
    val mode: String = "business",
    @SerializedName("motor")
    val motor: Produk,
    @SerializedName("mobil")
    val mobil: Produk
) {
    data class Produk(
        @SerializedName("tambah")
        val tambah: Int,
        @SerializedName("kuras")
        val kuras: Int,
        @SerializedName("tambal")
        val tambal: Int,
        @SerializedName("gagal")
        val gagal: Double
    )
}
