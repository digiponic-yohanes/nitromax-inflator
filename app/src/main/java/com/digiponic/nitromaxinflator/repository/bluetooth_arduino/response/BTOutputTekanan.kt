package com.digiponic.nitromaxinflator.repository.bluetooth_arduino.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class BTOutputTekanan(
    @SerializedName("tekanan")
    val tekanan: Int,
    @SerializedName("ref")
    val ref: Int
)
