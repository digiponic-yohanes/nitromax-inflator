package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class GroupRekapNitrogenResponse(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("data")
    val data: ModeTransaksi
): BaseResponse, Serializable {
    override fun status(): Boolean = status
    override fun message(): String = message

    data class ModeTransaksi(
        @SerializedName("normal")
        val normal: List<RekapNitrogenResponse.RekapNitrogen>,
        @SerializedName("emergency")
        val emergency: List<RekapNitrogenResponse.RekapNitrogen>,
    )
}
