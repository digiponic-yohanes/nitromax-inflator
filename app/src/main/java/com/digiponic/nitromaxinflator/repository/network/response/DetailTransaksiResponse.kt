package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DetailTransaksiResponse (
  @SerializedName("message")
  val message: String,
  @SerializedName("status")
  val status: Boolean,
  @SerializedName("data")
  val data: Transaksi
) : BaseResponse, Serializable {
  override fun status(): Boolean = status
  override fun message(): String = message

  data class Transaksi(
    @SerializedName("id")
    val id: Int,
    @SerializedName("header_cabang")
    val header_cabang: String,
    @SerializedName("nama_cabang")
    val nama_cabang: String,
    @SerializedName("kode")
    val kode: String,
    @SerializedName("subtotal")
    val subtotal: Int,
    @SerializedName("tanggal")
    val tanggal: String,
    @SerializedName("terms")
    val terms: String,
    @SerializedName("metode_bayar")
    val metode_bayar: String,
    @SerializedName("status_pembayaran")
    val status_pembayaran: Int,
    @SerializedName("detail")
    val detail: List<DetailTransaksi>
  )

  data class DetailTransaksi(
    @SerializedName("id")
    val id: Int,
    @SerializedName("id_item")
    val id_item: Int,
    @SerializedName("nama")
    val nama: String,
    @SerializedName("keterangan")
    val keterangan: String,
    @SerializedName("harga")
    val harga: Int,
    @SerializedName("qty")
    val qty: Int,
    @SerializedName("gagal")
    val gagal: Double,
    @SerializedName("subtotal")
    val subtotal: Int
  )
}