package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AbsensiResponse (
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("data")
    val data: List<Data>
) : BaseResponse, Serializable {
    override fun status(): Boolean = status
    override fun message(): String = message

    data class Data(
        @SerializedName("date_field")
        val dateField: String,
        @SerializedName("status_check_in")
        val statusCheckIn: String
    ) : Serializable {
        override fun equals(other: Any?): Boolean {
            if(other is Data){
                if(other.dateField == dateField){
                    return true
                }
            }

            return false
        }
        override fun hashCode(): Int = dateField.hashCode()
    }
}