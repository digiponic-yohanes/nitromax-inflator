package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class UserCabangResponse (
    @SerializedName("data")
    val data: UserCabang,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
) : BaseResponse, Serializable {
    override fun status(): Boolean = status
    override fun message(): String = message

    data class UserCabang(
        @SerializedName("id_cabang")
        val id_cabang: String,
        @SerializedName("id_user")
        val id_user: String,
        @SerializedName("nama_cabang")
        val nama_cabang: String,
        @SerializedName("alamat")
        val alamat: String,
        @SerializedName("telepon")
        val telepon: String,
        @SerializedName("email")
        val email: String,
        @SerializedName("pin")
        val pin: String,
        @SerializedName("password")
        val password: String,
        @SerializedName("logo")
        val logo: String
    )
}