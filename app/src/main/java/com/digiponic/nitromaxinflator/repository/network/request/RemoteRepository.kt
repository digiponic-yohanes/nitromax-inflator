package com.digiponic.nitromaxinflator.repository.network.request

import com.digiponic.nitromaxinflator.repository.network.parameter.BluetoothStatusLog
import com.digiponic.nitromaxinflator.repository.network.parameter.PayTransactionEmergencyParams
import com.digiponic.nitromaxinflator.repository.network.parameter.PayTransactionParams
import com.digiponic.nitromaxinflator.repository.network.response.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface RemoteRepository {
    @POST("auth/new_login")
    @FormUrlEncoded
    suspend fun login(@Field("username") username: String, @Field("password") password: String, @Field("id_cabang") id_cabang: String): LoginResponse

    @GET("auth/updated_user/{id}")
    suspend fun getUserDataUpdated(@Path("id")id: String): LoginResponse

    @GET("cabang/detailbarangjasa/{id}")
    suspend fun getAllItem(@Path("id") id: String): ItemResponse

    @GET("cabang/detailbarangjasa/tipe/{id}/55")
    suspend fun getAllProduk(@Path("id") id: String): ItemResponse

    @GET("cabang/detailbarangjasa/tipe/{id}/56")
    suspend fun getAllJasa(@Path("id") id: String): ItemResponse

    @POST("cabang/cash_owner")
    @FormUrlEncoded
    suspend fun cashKaryawan(@Field("cabang") id_cabang: String, @Field("karyawan") karyawan: String): CashKaryawanResponse

    @POST("cabang/mac_mesin")
    @FormUrlEncoded
    suspend fun getCabangMac(@Field("mac_mesin") mac_mesin: String): CabangResponse

    @GET("transaksi/two_index/{id}/{karyawan}")
    suspend fun getTransaksi(@Path("id") id: String, @Path("karyawan") karyawan: String): TransaksiResponse

    @POST("cabang/app_history")
    @FormUrlEncoded
    suspend fun saveAppHistory(@Field("id_cabang") id_cabang: String, @Field("email") email: String, @Field("keterangan") keterangan: String): NoDataResponse

    @POST("cabang/new_engine_history")
    suspend fun saveEngineHistory(@Body params: BluetoothStatusLog): NoDataResponse

    @POST("transaksi")
    suspend fun newCheckout(@Body params: PayTransactionParams): DetailTransaksiResponse

    @FormUrlEncoded
    @POST("v2/transaksi/cashless")
    suspend fun checkoutCashless(@Field("kode_penjualan") kodePenjualan: String): DetailTransaksiResponse

    @POST("transaksi/new_emergency")
    suspend fun recoveryTransaksi(@Body params: PayTransactionEmergencyParams): NoDataResponse

    @Multipart
    @POST("absensi/checkin_karyawan")
    suspend fun checkInKaryawan(@PartMap params: MutableMap<String, RequestBody>, @Part foto: MultipartBody.Part): CheckInResponse

    @POST("absensi/checkout_karyawan/{id}")
    suspend fun checkOutKaryawan(@Path("id") id_absensi: String): NoDataResponse

    @FormUrlEncoded
    @POST("absensi/absensi")
    suspend fun getAbsensiKaryawan(@Field("email") email: String, @Field("bulan") bulan:Int, @Field("tahun") tahun: Int): AbsensiResponse

    @FormUrlEncoded
    @POST("absensi/absensi_year")
    suspend fun getAbsensiKaryawanYear(@Field("email") email: String, @Field("tahun") tahun: Int): AbsensiResponse

    @FormUrlEncoded
    @POST("cabang/json_decode")
    suspend fun sendCorruptJson(@Field("id_cabang") idCabang: String, @Field("json_code") json: String): NoDataResponse

    @FormUrlEncoded
    @POST("transaksi/nitrogen_karyawan_tiga")
    suspend fun rekapTransaksiKaryawanTiga(@Field("id_absensi") id_absensi: String): GroupRekapNitrogenResponse

    @FormUrlEncoded
    @POST("transaksi/check_single_json")
    suspend fun checkSingleJson(@Field("id_cabang") id_cabang: String, @Field("json_string") json_string: String): SingleJsonResponse
}