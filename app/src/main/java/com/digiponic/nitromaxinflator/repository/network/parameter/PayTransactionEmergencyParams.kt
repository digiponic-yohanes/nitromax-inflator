package com.digiponic.nitromaxinflator.repository.network.parameter

import com.google.gson.annotations.SerializedName

data class PayTransactionEmergencyParams(
    @SerializedName("penjualan")
    val penjualan: List<PayTransactionParams>
)