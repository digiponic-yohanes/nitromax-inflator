package com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class BTEngineHistory (
    @SerializedName("datetime")
    val datetime: String,
    @SerializedName("mode")
    val mode: String
) : Serializable