package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class TransaksiResponse (
    @SerializedName("data")
    val data: List<DetailTransaksiResponse.Transaksi>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
) : BaseResponse, Serializable {
    override fun status(): Boolean = status
    override fun message(): String = message
}