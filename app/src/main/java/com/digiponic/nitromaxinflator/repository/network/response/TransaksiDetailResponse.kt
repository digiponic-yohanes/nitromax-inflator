package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class TransaksiDetailResponse (
    @SerializedName("data")
    val data: List<TransaksiDetail>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
) : BaseResponse, Serializable {
    override fun status(): Boolean = status
    override fun message(): String = message

    data class TransaksiDetail (
        @SerializedName("id")
        val id: String,
        @SerializedName("id_penjualan")
        val id_penjualan: String,
        @SerializedName("kode_penjualan")
        val kode_penjualan: String,
        @SerializedName("id_item")
        val id_item: String,
        @SerializedName("id_cabang")
        val id_cabang: String,
        @SerializedName("nama")
        val nama: String,
        @SerializedName("harga")
        val harga: String,
        @SerializedName("qty")
        val qty: String,
        @SerializedName("subtotal")
        val subtotal: String,
        @SerializedName("kategori")
        val kategori: String
    ) : Serializable
}