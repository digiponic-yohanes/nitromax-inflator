package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName

data class NoDataResponse(@SerializedName("status")
                          val status: Boolean,
                          @SerializedName("message")
                          val message: String): BaseResponse {
    override fun status(): Boolean = status
    override fun message(): String = message
}