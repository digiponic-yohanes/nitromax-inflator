package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class TransaksiDetailedResponse (
    @SerializedName("data")
    val data: MutableList<Transaksi>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
) : BaseResponse, Serializable {
    override fun status(): Boolean = status
    override fun message(): String = message

    data class Transaksi (
        @SerializedName("id")
        val id: String,
        @SerializedName("id_cabang")
        val id_cabang: String,
        @SerializedName("nama_cabang")
        val nama_cabang: String,
        @SerializedName("kode")
        val kode: String,
        @SerializedName("tanggal")
        val tanggal: String,
        @SerializedName("id_jenis_penjualan")
        val id_jenis_penjualan: String,
        @SerializedName("id_jenis_kendaraan")
        val id_jenis_kendaraan: String,
        @SerializedName("subtotal")
        val subtotal: String,
        @SerializedName("diskon_tipe")
        val diskon_tipe: String,
        @SerializedName("diskon_nominal")
        val diskon_nominal: String,
        @SerializedName("total")
        val total: String,
        @SerializedName("metode_pembayaran")
        val metode_pembayaran: String,
        @SerializedName("id_merchant")
        val id_merchant: String,
        @SerializedName("nomor_kartu")
        val nomor_kartu: String,
        @SerializedName("kode_trace")
        val kode_trace: String,
        @SerializedName("bayar")
        val bayar: String,
        @SerializedName("kembalian")
        val kemnbalian: String,
        @SerializedName("status_pembayaran")
        val status_pembayaran: String,
        @SerializedName("id_shift")
        val id_shift: String,
        @SerializedName("detail")
        val detail: List<TransaksiDetailResponse.TransaksiDetail>
    ) : Serializable
}
