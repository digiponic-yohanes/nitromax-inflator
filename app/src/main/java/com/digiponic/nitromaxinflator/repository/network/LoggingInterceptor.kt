package com.digiponic.nitromaxinflator.repository.network

import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import java.io.IOException
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets.UTF_8
import java.util.TreeSet
import java.util.concurrent.TimeUnit
import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.internal.http.promisesBody
import okhttp3.internal.platform.Platform
import okio.Buffer
import okio.GzipSource
import java.io.EOFException

/**
 * An OkHttp interceptor which logs request and response information. Can be applied as an
 * [application interceptor][OkHttpClient.interceptors] or as a [OkHttpClient.networkInterceptors].
 *
 * The format of the logs created by this class should not be considered stable and may
 * change slightly between releases. If you need a stable logging format, use your own interceptor.
 */
class LoggingInterceptor @JvmOverloads constructor(
    private val logger: Logger = Logger.DEFAULT
) : Interceptor {

    @Volatile private var headersToRedact = emptySet<String>()

    interface Logger {
        fun log(message: String)

        companion object {
            /** A [Logger] defaults output appropriate for the current platform. */
            @JvmField
            val DEFAULT: Logger = object : Logger {
                override fun log(message: String) {
                    Platform.get().log(message)
                }
            }
        }
    }

    fun redactHeader(name: String) {
        val newHeadersToRedact = TreeSet(String.CASE_INSENSITIVE_ORDER)
        newHeadersToRedact += headersToRedact
        newHeadersToRedact += name
        headersToRedact = newHeadersToRedact
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var log = ""
        val request = chain.request()

        val requestBody = request.body

        val connection = chain.connection()
        var requestStartMessage =
            ("--> ${request.method} ${request.url}${if (connection != null) " " + connection.protocol() else ""}")
        if (requestBody != null) {
            requestStartMessage += " (${requestBody.contentLength()}-byte body)"
        }
        log += requestStartMessage
        logger.log(requestStartMessage)

        val _headers = request.headers

        if (requestBody != null) {
            // Request body headers are only present when installed as a network interceptor. When not
            // already present, force them to be included (if available) so their values are known.
            requestBody.contentType()?.let {
                if (_headers["Content-Type"] == null) {
                    log += "Content-Type: $it"
                    logger.log("Content-Type: $it")
                }
            }
            if (requestBody.contentLength() != -1L) {
                if (_headers["Content-Length"] == null) {
                    log += "Content-Length: ${requestBody.contentLength()}"
                    logger.log("Content-Length: ${requestBody.contentLength()}")
                }
            }
        }

        for (i in 0 until _headers.size) {
            logHeader(_headers, i)
        }

        if (requestBody == null) {
            log += "--> END ${request.method}"
            logger.log("--> END ${request.method}")
        } else if (bodyHasUnknownEncoding(request.headers)) {
            log += "--> END ${request.method} (encoded body omitted)"
            logger.log("--> END ${request.method} (encoded body omitted)")
        } else if (requestBody.isDuplex()) {
            log += "--> END ${request.method} (duplex request body omitted)"
            logger.log("--> END ${request.method} (duplex request body omitted)")
        } else if (requestBody.isOneShot()) {
            log += "--> END ${request.method} (one-shot body omitted)"
            logger.log("--> END ${request.method} (one-shot body omitted)")
        } else {
            val buffer = Buffer()
            requestBody.writeTo(buffer)

            val contentType = requestBody.contentType()
            val charset: Charset = contentType?.charset(UTF_8) ?: UTF_8

            logger.log("")
            if (buffer.isProbablyUtf8()) {
                log += buffer.readString(charset)
                log += "--> END ${request.method} (${requestBody.contentLength()}-byte body)"
                logger.log(buffer.readString(charset))
                logger.log("--> END ${request.method} (${requestBody.contentLength()}-byte body)")
            } else {
                log += "--> END ${request.method} (binary ${requestBody.contentLength()}-byte body omitted)"
                logger.log(
                    "--> END ${request.method} (binary ${requestBody.contentLength()}-byte body omitted)")
            }
        }

        val startNs = System.nanoTime()
        val response: Response
        try {
            response = chain.proceed(request)
        } catch (e: Exception) {
            log += "<-- HTTP FAILED: $e"
            logger.log("<-- HTTP FAILED: $e")
            throw e
        }

        val tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs)

        val responseBody = response.body!!
        val contentLength = responseBody.contentLength()
        log += "<-- ${response.code}${if (response.message.isEmpty()) "" else ' ' + response.message} ${response.request.url} (${tookMs}ms)"
        logger.log(
            "<-- ${response.code}${if (response.message.isEmpty()) "" else ' ' + response.message} ${response.request.url} (${tookMs}ms)")

        val headers = response.headers
        for (i in 0 until headers.size) {
            logHeader(headers, i)
        }

        if (!response.promisesBody()) {
            log += "<-- END HTTP"
            logger.log("<-- END HTTP")
        } else if (bodyHasUnknownEncoding(response.headers)) {
            log += "<-- END HTTP (encoded body omitted)"
            logger.log("<-- END HTTP (encoded body omitted)")
        } else {
            val source = responseBody.source()
            source.request(Long.MAX_VALUE) // Buffer the entire body.
            var buffer = source.buffer

            var gzippedLength: Long? = null
            if ("gzip".equals(headers["Content-Encoding"], ignoreCase = true)) {
                gzippedLength = buffer.size
                GzipSource(buffer.clone()).use { gzippedResponseBody ->
                    buffer = Buffer()
                    buffer.writeAll(gzippedResponseBody)
                }
            }

            val contentType = responseBody.contentType()
            val charset: Charset = contentType?.charset(UTF_8) ?: UTF_8

            if (!buffer.isProbablyUtf8()) {
                log += "\n"
                log += "<-- END HTTP (binary ${buffer.size}-byte body omitted)"
                logger.log("")
                logger.log("<-- END HTTP (binary ${buffer.size}-byte body omitted)")
                return response
            }

            if (contentLength != 0L) {
                log += "\n"
                log += buffer.clone().readString(charset)
                logger.log("")
                logger.log(buffer.clone().readString(charset))
            }

            if (gzippedLength != null) {
                log += "<-- END HTTP (${buffer.size}-byte, $gzippedLength-gzipped-byte body)"
                logger.log("<-- END HTTP (${buffer.size}-byte, $gzippedLength-gzipped-byte body)")
            } else {
                log += "<-- END HTTP (${buffer.size}-byte body)"
                logger.log("<-- END HTTP (${buffer.size}-byte body)")
            }
        }
        Firebase.analytics.logEvent(FirebaseAnalytics.Param.SOURCE) {
            param("data", log)
        }

        return response
    }

    private fun logHeader(headers: Headers, i: Int) {
        val value = if (headers.name(i) in headersToRedact) "██" else headers.value(i)
        logger.log(headers.name(i) + ": " + value)
    }

    private fun bodyHasUnknownEncoding(headers: Headers): Boolean {
        val contentEncoding = headers["Content-Encoding"] ?: return false
        return !contentEncoding.equals("identity", ignoreCase = true) &&
                !contentEncoding.equals("gzip", ignoreCase = true)
    }

    private fun Buffer.isProbablyUtf8(): Boolean {
        try {
            val prefix = Buffer()
            val byteCount = size.coerceAtMost(64)
            copyTo(prefix, 0, byteCount)
            for (i in 0 until 16) {
                if (prefix.exhausted()) {
                    break
                }
                val codePoint = prefix.readUtf8CodePoint()
                if (Character.isISOControl(codePoint) && !Character.isWhitespace(codePoint)) {
                    return false
                }
            }
            return true
        } catch (_: EOFException) {
            return false // Truncated UTF-8 sequence.
        }
    }
}