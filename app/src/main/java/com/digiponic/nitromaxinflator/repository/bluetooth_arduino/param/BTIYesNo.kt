package com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param

import com.google.gson.annotations.SerializedName

data class BTIYesNo(
    @SerializedName("mode")
    val mode: String = "answerYesNo",
    @SerializedName("value")
    val value: Boolean
)
