package com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param

import com.google.gson.annotations.SerializedName

data class BTIRequestService(
    @SerializedName("mode")
    val mode: String = "transaction",
    @SerializedName("mode_transaksi")
    val modeTransaksi: Int,
    @SerializedName("jumlah_ban")
    val jumlahBan: Int,
    @SerializedName("tekanan")
    val tekanan: Int
)
