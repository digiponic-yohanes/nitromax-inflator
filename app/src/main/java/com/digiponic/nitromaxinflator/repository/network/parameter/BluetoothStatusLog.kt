package com.digiponic.nitromaxinflator.repository.network.parameter

import com.google.gson.annotations.SerializedName

data class BluetoothStatusLog(
  @SerializedName("tanggal_start_off")
  val datetime_start: String,
  @SerializedName("tanggal_end_off")
  val datetime_end: String,
  @SerializedName("id_cabang")
  val id_cabang: String,
  @SerializedName("email")
  val email: String
)
