package com.digiponic.nitromaxinflator.repository.network.response

class ErrorResponseException(message: String?) : Exception(message)