package com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param

import com.google.gson.annotations.SerializedName

data class BTReply(@SerializedName("reply") val value: String = "ok")
