package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class RekapNitrogenResponse(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("data")
    val data: List<RekapNitrogen>
) : BaseResponse, Serializable {
    override fun status(): Boolean = status
    override fun message(): String = message

    data class RekapNitrogen(
        @SerializedName("keterangan")
        val keterangan: String,
        @SerializedName("jumlah_cash")
        val jumlah_cash: Int,
        @SerializedName("jumlah_cashless")
        val jumlah_cashless: Int,
        @SerializedName("total_cash")
        val total_cash: Int,
        @SerializedName("total_cashless")
        val total_cashless: Int
    ) : Serializable
}
