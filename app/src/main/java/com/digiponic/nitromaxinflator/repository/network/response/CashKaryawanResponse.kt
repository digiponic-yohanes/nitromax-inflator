package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CashKaryawanResponse (
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("data")
    val data: CashKaryawan
) : BaseResponse, Serializable {
    override fun status(): Boolean = status
    override fun message(): String = message

    data class CashKaryawan(
        @SerializedName("yesterday")
        val yesterday: Int,
        @SerializedName("today")
        val today: Int
    )
}