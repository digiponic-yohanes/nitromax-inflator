package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName

data class TransaksiSuccessResponse(@SerializedName("status")
                          val status: Boolean,
                                    @SerializedName("message")
                          val message: String,
                                    @SerializedName("kode_penjualan")
                          val kode_penjualan: String): BaseResponse {
    override fun status(): Boolean = status
    override fun message(): String = message
    fun kode_penjualan(): String = kode_penjualan

}