package com.digiponic.nitromaxinflator.repository.network.response

import com.google.gson.annotations.SerializedName
import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import java.io.Serializable

data class LoginResponse(
  @SerializedName("data")
  val data: User,
  @SerializedName("message")
  val message: String,
  @SerializedName("status")
  val status: Boolean
) : BaseResponse, Serializable {
  override fun status(): Boolean = status
  override fun message(): String = message

  data class User(
    @SerializedName("email")
    val email: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("id_cabang")
    var idCabang: String,
    @SerializedName("nama_cabang")
    var namaCabang: String,
    @SerializedName("skala_gagal")
    var skalaGagal: Double,
    @SerializedName("skala_sama")
    var skalaSama: Double,
    @SerializedName("id_cms_privileges")
    val idCmsPrivileges: String,
    @SerializedName("id_owner")
    val idOwner: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("photo")
    val photo: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("terms")
    val terms: String,
    @SerializedName("is_using_qris")
    val is_using_qris: Int,
    @SerializedName("is_auto_print_struk")
    val is_auto_print_struk: Int,
    @SerializedName("header_cabang")
    var header_cabang: String,
    @SerializedName("limit_pressure_inflation_motor")
    val limit_pressure_inflation_motor: Int,
    @SerializedName("limit_pressure_inflation_mobil")
    val limit_pressure_inflation_mobil: Int,
    @SerializedName("has_tambal_ban_khusus")
    val has_tambal_ban_khusus: Int,
    @SerializedName("has_previous_login")
    val has_previous_login: Boolean
  ) : Serializable
}