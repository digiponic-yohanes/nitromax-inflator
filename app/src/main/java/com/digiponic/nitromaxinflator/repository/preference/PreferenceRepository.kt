package com.digiponic.nitromaxinflator.repository.preference

import android.content.SharedPreferences
import com.digiponic.nitromaxinflator.repository.network.response.LoginResponse
import com.google.gson.Gson

class PreferenceRepository(private val preference: SharedPreferences,
                           private val gson: Gson) {
    companion object {
        const val PREFERENCE = "PREFERENCE"
        const val USER = "USER"
        const val CORRUPTED_JSON = "CORRUPTED_JSON"
    }

    fun  saveUser(user: LoginResponse.User){
        preference.edit().putString(USER, gson.toJson(user)).apply()
    }

    fun addAbsensiId(id: String){
        preference.edit().putString("id_absensi", id).apply()
    }

    fun getUser(): LoginResponse.User?{
        return if(preference.contains(USER)){
            gson.fromJson(preference.getString(USER, ""), LoginResponse.User::class.java)
        }else{
            null
        }
    }

    fun getAbsensiId(): String? {
        return if (preference.contains("id_absensi")) {
            preference.getString("id_absensi", null)
        } else {
            null
        }
    }

    fun removeUser(){
        preference.edit().remove(USER).apply()
        preference.edit().remove("id_absensi").apply()
    }

    fun saveCorruptedJson(corruptedJson: String){
        var json = preference.getString(CORRUPTED_JSON, "") ?: ""
        json += corruptedJson
        preference.edit().putString(CORRUPTED_JSON, json).apply()
    }
}