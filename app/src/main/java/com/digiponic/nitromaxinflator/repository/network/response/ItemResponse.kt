package com.digiponic.nitromaxinflator.repository.network.response

import com.google.gson.annotations.SerializedName
import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import java.io.Serializable

data class ItemResponse(
    @SerializedName("data")
    val data: MutableList<Item>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
) : BaseResponse, Serializable {
    override fun status(): Boolean = status
    override fun message(): String = message

    data class Item(
        @SerializedName("deskripsi")
        val deskripsi: String,
        @SerializedName("gambar")
        val gambar: String,
        @SerializedName("harga_jual")
        val hargaJual: String,
        @SerializedName("id")
        val id: Int,
        @SerializedName("id_cabang")
        val idCabang: String,
        @SerializedName("id_kategori")
        val idKategori: String,
        @SerializedName("id_satuan")
        val idSatuan: String,
        @SerializedName("id_tipe")
        val idTipe: String,
        @SerializedName("keterangan")
        val keterangan: String,
        @SerializedName("kode")
        val kode: String,
        @SerializedName("qty")
        var qty: Int,
        @SerializedName("qty_minimal")
        val qtyMinimal: Int,
        @SerializedName("maks_pembelian")
        val maksPembelian: Int = 0
    ) : Serializable {

        override fun hashCode(): Int = id
        override fun equals(other: Any?): Boolean {
            return if(other == null){
                false
            }else {
                if(other is Item){
                    id == other.id
                }else {
                    false
                }
            }
        }
    }
}