package com.digiponic.nitromaxinflator.repository.network.parameter

import com.google.gson.annotations.SerializedName

data class PayTransactionParams(
    @SerializedName("id_mode")
    val idMode: Int,
    @SerializedName("cabang")
    val cabang: String,
    @SerializedName("jenis_kendaraan")
    val jenisKendaraan: String,
    @SerializedName("metode_pembayaran")
    val metodePembayaran: Int,
    @SerializedName("bayar")
    val bayar: Int,
    @SerializedName("user")
    val user: String,
    @SerializedName("tanggal")
    val tanggal: String? = null,
    @SerializedName("json_string")
    val json_string: String? = null,
    @SerializedName("detail")
    val detail: List<PayTransactionDetail>){
    data class PayTransactionDetail(
        @SerializedName("id_item")
        var idItem: String,
        @SerializedName("nama_item")
        var namaItem: String = "",
        @SerializedName("harga")
        var harga: Int = 0,
        @SerializedName("qty")
        var qty: Int,
        @SerializedName("gagal")
        var gagal: Int,
        @SerializedName("keterangan")
        var keterangan: String
    )

}