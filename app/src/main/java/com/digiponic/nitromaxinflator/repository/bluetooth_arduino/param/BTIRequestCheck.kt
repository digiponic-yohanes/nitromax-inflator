package com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param

import com.google.gson.annotations.SerializedName

data class BTIRequestCheckTekanan(
    @SerializedName("mode")
    val mode: String = "transaction",
    @SerializedName("mode_transaksi")
    val modeTransaksi: Int = 9,
    @SerializedName("tekanan")
    val tekanan: Int = 33,
    @SerializedName("jumlah_ban")
    val jumlah_ban: Int = 1
)
