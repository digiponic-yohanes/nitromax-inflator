package com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param

import com.google.gson.annotations.SerializedName

data class BTIUpdatePressure(
    @SerializedName("mode")
    val mode: String = "transaction",
    @SerializedName("tekanan")
    val tekanan: Int
)
