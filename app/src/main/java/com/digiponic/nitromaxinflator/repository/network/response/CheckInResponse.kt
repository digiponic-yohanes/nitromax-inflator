package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName

data class CheckInResponse(@SerializedName("id_absensi")
                           val id_absensi: String,
                          @SerializedName("status")
                          val status: Boolean,
                           @SerializedName("message")
                          val message: String): BaseResponse {
    override fun status(): Boolean = status
    override fun message(): String = message
    fun id_absensi(): String = id_absensi
}