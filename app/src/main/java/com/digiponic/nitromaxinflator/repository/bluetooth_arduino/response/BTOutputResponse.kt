package com.digiponic.nitromaxinflator.repository.bluetooth_arduino.response


import com.google.gson.annotations.SerializedName
import com.mcnmr.utilities.extension.isNotNull
import java.io.Serializable

data class BTOutputResponse(
  @SerializedName("detail")
  val detail: Detail,
  @SerializedName("harga")
  val harga: Int,
  @SerializedName("id_mode")
  val idMode: Int,
  @SerializedName("jenis_kendaraan")
  val jenisKendaraan: Int,
  @SerializedName("no_transaksi")
  val noTransaksi: Int,
  @SerializedName("tgl_transaksi")
  val tglTransaksi: String
) : Serializable {
  data class Detail(
    @SerializedName("jumlah_ban")
    val jumlahBan: Int,
    @SerializedName("jumlah_error")
    val jumlahError: Int,
    @SerializedName("jumlah_lubang")
    val jumlahLubang: Int,
    @SerializedName("mode_transaksi")
    val modeTransaksi: Int,
    @SerializedName("tekanan_akhir")
    val tekanan: String?,
    @SerializedName("tekanan_awal")
    val tekananAwal: String?
  ) : Serializable

  fun pressure(idModeTransaksi: Int, harga: Int, toleransi: Double): List<Pressure> {
    val list = mutableListOf<Pressure>()
    if (detail.isNotNull()) {
      if(detail.modeTransaksi == idModeTransaksi){
        val tekanan = detail.tekanan!!.split(",")
        val tekananAwal = detail.tekananAwal!!.split(",")

        tekanan.forEachIndexed { index, s ->
          if(s != "" && tekananAwal[index] != ""){
            val isTts = tekananAwal[index].contains("tts")
            val checkTekananAwal = if(isTts) 0 else tekananAwal[index].replace("(tts)", "").toInt()
            val cost: Int = if(s.toInt() <= checkTekananAwal) (harga * toleransi).toInt() else harga
            list.add(
              Pressure(
                tekanan = s.toInt(),
                tekananAwal = checkTekananAwal,
                harga = cost,
                isGagal = s.toInt() <= checkTekananAwal,
                isTts = isTts
              )
            )
          }
        }
      }
    }
    return list
  }

  data class Pressure(
    val tekanan: Int,
    val tekananAwal: Int,
    val harga: Int,
    val isGagal: Boolean,
    val isTts: Boolean = false
  )
}