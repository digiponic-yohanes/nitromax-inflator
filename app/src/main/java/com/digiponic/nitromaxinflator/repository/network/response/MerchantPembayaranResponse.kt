package com.digiponic.nitromaxinflator.repository.network.response

import com.google.gson.annotations.SerializedName
import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import java.io.Serializable

data class MerchantPembayaranResponse(
    @SerializedName("data")
    val data: List<Data>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
) : BaseResponse, Serializable {

    override fun status(): Boolean = status
    override fun message(): String = message

    data class Data(
        @SerializedName("gambar")
        val gambar: Any,
        @SerializedName("id")
        val id: String,
        @SerializedName("id_tipe")
        val idTipe: String,
        @SerializedName("keterangan")
        val keterangan: String,
        @SerializedName("kode")
        val kode: String
    ) : Serializable
}