package com.digiponic.nitromaxinflator.repository.network.response

import com.digiponic.nitromaxinflator.ancestors.BaseResponse
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DetailItemResponse (
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("data")
    val data: ItemResponse.Item
) : BaseResponse, Serializable {
    override fun status(): Boolean = status
    override fun message(): String = message
}