package com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param

import com.google.gson.annotations.SerializedName

data class BTIHole(
    @SerializedName("mode")
    val mode: String = "answerNumeric",
    @SerializedName("value")
    val jumlah_lubang: Int
)
