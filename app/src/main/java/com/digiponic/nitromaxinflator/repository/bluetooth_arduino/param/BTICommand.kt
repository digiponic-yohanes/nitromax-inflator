package com.digiponic.nitromaxinflator.repository.bluetooth_arduino.param

import com.google.gson.annotations.SerializedName

data class BTICommand(
    @SerializedName("mode")
    val mode: String = "command",
    @SerializedName("value")
    val value: String
)
