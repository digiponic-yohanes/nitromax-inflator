package com.digiponic.nitromaxinflator.sqlite

import com.digiponic.nitromaxinflator.ancestors.BaseActivity
import com.digiponic.nitromaxinflator.repository.network.parameter.PayTransactionParams
import com.digiponic.nitromaxinflator.sqlite.data.BluetoothName
import com.digiponic.nitromaxinflator.sqlite.data.Logtext
import com.digiponic.nitromaxinflator.sqlite.data.Penjualan
import com.digiponic.nitromaxinflator.sqlite.data.PenjualanDetail
import com.mcnmr.utilities.extension.isNotEmptyOrNotNull
import com.mcnmr.utilities.extension.isNotNull
import java.text.SimpleDateFormat
import java.util.*

class DatabaseHelper(activity: BaseActivity) {

  private val database = NitromaxDb.getInstance(activity)
  private val bluetoothNameDao = database.bluetoothNameDao()
  private val logtextDao = database.logtextDao()
  val penjualanDao = database.penjualanDao()
  private val penjualanDetailDao = database.penjualanDetailDao()

  fun writeLogToFile(logtext: String, type: String) {
    val values = Logtext(
      date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault()).format(Date()),
      text = "$type : $logtext"
    )
    logtextDao.insert(values)
  }

  fun checkBluetoothNameExist(): Boolean {
    return bluetoothNameDao.getBluetoothName().isNotNull()
  }

  fun insertNewBluetoothName(btName: String) {
    bluetoothNameDao.insert(BluetoothName(1, btName))
  }

  fun writePendingTransaction(skalaGagal: Double, params: PayTransactionParams): Boolean {
    val check = penjualanDao.getAll()
    return if (check.isEmpty()) {
      val id = 1
      for (det in params.detail) {
        val detail = PenjualanDetail(idPenjualan = id, idCabang = params.cabang, nama = det.namaItem, idItem = det.idItem, harga = det.harga, qty = det.qty, qtyGagal = det.gagal, skalaGagal = skalaGagal, keterangan = det.keterangan)
        penjualanDetailDao.insert(detail)
      }
      penjualanDao.insert(Penjualan(id = id, idCabang = params.cabang, tanggal = params.tanggal!!, jenisKendaraan = params.jenisKendaraan, metodePembayaran = params.metodePembayaran, namaOperator = params.user, jsonString = params.json_string!!, subtotal = params.bayar))
      true
    } else {
      false
    }
  }

  fun checkPendingTransaction() = penjualanDao.getAll().isEmpty()

  fun deletePendingTransaction() {
    penjualanDetailDao.deleteAll()
    penjualanDao.deleteAll()
  }

  fun assignPendingTransaction(): PayTransactionParams {
    val transaction = penjualanDao.getAll()
    val transDetail = penjualanDetailDao.getByIdPenjualan(transaction[0].id)

    val detail: MutableList<PayTransactionParams.PayTransactionDetail> = mutableListOf()
    for (ptd in transDetail) {
      detail.add(PayTransactionParams.PayTransactionDetail(idItem = ptd.idItem, namaItem = ptd.nama, harga = ptd.harga, qty = ptd.qty, gagal = ptd.qtyGagal, keterangan = ptd.keterangan))
    }
    return PayTransactionParams(
      idMode = 0,
      cabang = transaction[0].idCabang,
      jenisKendaraan = transaction[0].jenisKendaraan,
      metodePembayaran = transaction[0].metodePembayaran,
      bayar = transaction[0].subtotal,
      user = transaction[0].namaOperator,
      tanggal = transaction[0].tanggal,
      json_string = if (transaction[0].jsonString.isNotEmptyOrNotNull()) transaction[0].jsonString else "",
      detail = detail
    )
  }
}