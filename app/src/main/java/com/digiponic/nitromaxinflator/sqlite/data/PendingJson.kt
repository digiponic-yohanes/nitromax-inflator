package com.digiponic.nitromaxinflator.sqlite.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_pending_json")
data class PendingJson(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val string: String
)
