package com.digiponic.nitromaxinflator.sqlite.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_bluetooth_name")
data class BluetoothName(
  @PrimaryKey(autoGenerate = true) val id: Int,
  val bluetooth_name: String
)
