package com.digiponic.nitromaxinflator.sqlite.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_penjualan")
data class Penjualan(
    @PrimaryKey(autoGenerate = false) val id: Int,
    val idCabang: String,
    val tanggal: String,
    val jenisKendaraan: String,
    val subtotal: Int,
    val metodePembayaran: Int,
    val namaOperator: String,
    val jsonString: String
)
