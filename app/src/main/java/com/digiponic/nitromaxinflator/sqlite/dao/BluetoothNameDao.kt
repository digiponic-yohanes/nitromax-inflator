package com.digiponic.nitromaxinflator.sqlite.dao

import androidx.room.*
import com.digiponic.nitromaxinflator.sqlite.data.BluetoothName

@Dao
interface BluetoothNameDao {
  @Query("SELECT * FROM table_bluetooth_name")
  fun getBluetoothName(): BluetoothName

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insert(bluetoothName: BluetoothName)

  @Update
  fun update(bluetoothName: BluetoothName)
}