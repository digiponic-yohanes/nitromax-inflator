package com.digiponic.nitromaxinflator.sqlite

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.digiponic.nitromaxinflator.sqlite.dao.*
import com.digiponic.nitromaxinflator.sqlite.data.*

@Database(
    entities = [
        BluetoothName::class,
        Logtext::class,
        PendingJson::class,
        Penjualan::class,
        PenjualanDetail::class
    ],
    version = 3
)

abstract class NitromaxDb : RoomDatabase() {

    abstract fun bluetoothNameDao(): BluetoothNameDao
    abstract fun logtextDao(): LogtextDao
    abstract fun pendingJsonDao(): PendingJsonDao
    abstract fun penjualanDao(): PenjualanDao
    abstract fun penjualanDetailDao(): PenjualanDetailDao

    companion object {
        private var instance: NitromaxDb? = null

        @Synchronized
        fun getInstance(context: Context): NitromaxDb {
            if(instance == null)
                instance = Room.databaseBuilder(context.applicationContext, NitromaxDb::class.java,
                    "nitromax_database")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build()

            return instance!!
        }
    }

}