package com.digiponic.nitromaxinflator.sqlite.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_penjualan_detail")
data class PenjualanDetail(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val idPenjualan: Int,
    val idItem: String,
    val idCabang: String,
    val nama: String,
    val harga: Int,
    val qty: Int,
    val qtyGagal: Int,
    val skalaGagal: Double,
    val keterangan: String
)
