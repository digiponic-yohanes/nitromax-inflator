package com.digiponic.nitromaxinflator.sqlite.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.digiponic.nitromaxinflator.sqlite.data.PenjualanDetail

@Dao
interface PenjualanDetailDao {
    @Query("SELECT * FROM table_penjualan_detail")
    fun getAll(): List<PenjualanDetail>

    @Query("SELECT * FROM table_penjualan_detail WHERE idPenjualan = :idPenjualan")
    fun getByIdPenjualan(idPenjualan: Int): List<PenjualanDetail>

    @Query("DELETE FROM table_penjualan_detail")
    fun deleteAll()

    @Insert(onConflict = REPLACE)
    fun insert(penjualanDetail: PenjualanDetail)
}