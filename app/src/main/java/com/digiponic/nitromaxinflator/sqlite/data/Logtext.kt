package com.digiponic.nitromaxinflator.sqlite.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_log")
data class Logtext(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val date: String,
    val text: String
)
