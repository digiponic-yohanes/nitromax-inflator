package com.digiponic.nitromaxinflator.sqlite.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.digiponic.nitromaxinflator.sqlite.data.Logtext

@Dao
interface LogtextDao {
    @Query("SELECT * FROM table_log ORDER BY date DESC")
    fun getAll(): List<Logtext>

    @Query("SELECT text FROM table_log WHERE date = :date")
    fun getByDate(date: String): String

    @Insert(onConflict = REPLACE)
    fun insert(logtext: Logtext)
}