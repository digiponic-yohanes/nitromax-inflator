package com.digiponic.nitromaxinflator.sqlite.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.digiponic.nitromaxinflator.sqlite.data.PendingJson

@Dao
interface PendingJsonDao {
    @Query("SELECT * FROM table_pending_json")
    fun getAll(): List<PendingJson>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(pendingJson: PendingJson)
}