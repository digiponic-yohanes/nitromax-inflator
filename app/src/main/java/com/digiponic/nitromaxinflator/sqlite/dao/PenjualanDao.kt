package com.digiponic.nitromaxinflator.sqlite.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.digiponic.nitromaxinflator.sqlite.data.Penjualan

@Dao
interface PenjualanDao {
    @Query("SELECT * FROM table_penjualan")
    fun getAll(): List<Penjualan>

    @Query("DELETE FROM table_penjualan")
    fun deleteAll()

    @Insert(onConflict = REPLACE)
    fun insert(penjualan: Penjualan)
}