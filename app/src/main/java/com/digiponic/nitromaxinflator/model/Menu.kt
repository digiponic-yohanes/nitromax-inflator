package com.digiponic.nitromaxinflator.model

sealed class Menu(val id: Int, val name: String) {
    companion object {
        fun generate(): List<Menu> = arrayListOf(Semua(), Jasa(), Produk(),
            Absensi(), ConnectBluetoothMikro(), ConnectBluetoothPrinter(), Recovery(), Logtext(), Logout())
    }

    class Semua: Menu(id = 0, name = "Semua")
    class Jasa: Menu(id = 1, name = "Jasa")
    class Produk: Menu(id = 2, name = "Produk")
    class Absensi: Menu(id = 4, name = "Absensi")
    class Logout: Menu(id = 5, name = "Logout")
    class ConnectBluetoothMikro: Menu(id = 6, name = "Hubungkan Bluetooth")
    class ConnectBluetoothPrinter: Menu(id = 7, name = "Test Printer")
    class Logtext: Menu(id = 8, name = "Log")
    class Recovery: Menu(id = 9, name = "Recovery Transaksi")
}