package com.digiponic.nitromaxinflator.injection.module

import android.content.SharedPreferences
import com.digiponic.nitromaxinflator.repository.preference.PreferenceRepository
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PreferenceModule {
    @Provides
    @Singleton
    fun giveSharedPreferences(preference: SharedPreferences, gson: Gson): PreferenceRepository {
        return PreferenceRepository(
            preference = preference,
            gson = gson
        )
    }
}