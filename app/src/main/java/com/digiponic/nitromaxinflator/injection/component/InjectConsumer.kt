package com.digiponic.nitromaxinflator.injection.component

import com.digiponic.nitromaxinflator.MainApplication
import com.digiponic.nitromaxinflator.injection.module.ContextModule

import com.digiponic.nitromaxinflator.injection.module.PreferenceModule
import com.digiponic.nitromaxinflator.injection.module.RetrofitModule
import com.digiponic.nitromaxinflator.ui.absensi.AbsensiVM
import com.digiponic.nitromaxinflator.ui.check_pressure.CheckPressureVM
import com.digiponic.nitromaxinflator.ui.check_tire.CheckTireVM
import com.digiponic.nitromaxinflator.ui.checkout_success.CheckoutSuccessVM
import com.digiponic.nitromaxinflator.ui.dashboard.DashboardVM
import com.digiponic.nitromaxinflator.ui.login.LoginVM
import com.digiponic.nitromaxinflator.ui.logtext.LogVM
import com.digiponic.nitromaxinflator.ui.payment.PaymentVM
import com.digiponic.nitromaxinflator.ui.selfie.SelfieVM
import com.digiponic.nitromaxinflator.ui.splashscreen.SplashScreenVM
import com.digiponic.nitromaxinflator.ui.transaksi.TransaksiVM
import javax.inject.Singleton

import dagger.Component

@Singleton
@Component(modules = [
    ContextModule::class,
    RetrofitModule::class,
    PreferenceModule::class
])
interface InjectConsumer {
    fun inject(vm: SplashScreenVM)
    fun inject(vm: LoginVM)
    fun inject(vm: SelfieVM)
    fun inject(vm: DashboardVM)
    fun inject(vm: TransaksiVM)
    fun inject(vm: PaymentVM)
    fun inject(vm: AbsensiVM)
    fun inject(vm: CheckTireVM)
    fun inject(vm: CheckoutSuccessVM)
    fun inject(vm: LogVM)
    fun inject(vm: CheckPressureVM)
    fun inject(vm: MainApplication)
}