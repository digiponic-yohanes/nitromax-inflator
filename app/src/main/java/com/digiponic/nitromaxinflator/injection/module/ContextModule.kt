package com.digiponic.nitromaxinflator.injection.module

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.digiponic.nitromaxinflator.repository.network.LoggingInterceptor
import com.digiponic.nitromaxinflator.repository.preference.PreferenceRepository
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

@Module
class ContextModule(
    @get:Provides
    val giveContext: Context
) {
    @get:Provides
    val giveClient: OkHttpClient = OkHttpClient.Builder()
        .readTimeout(60, TimeUnit.SECONDS)
        .connectTimeout(60, TimeUnit.SECONDS)
        .addInterceptor(LoggingInterceptor()).build()
    @get:Provides
    val giveGson: Gson = Gson()
    @get:Provides
    val givePreference: SharedPreferences =
        giveContext.getSharedPreferences(PreferenceRepository.PREFERENCE, MODE_PRIVATE)

}
